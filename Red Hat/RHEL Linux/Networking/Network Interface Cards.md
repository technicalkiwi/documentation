

## lshw

Extract detailed information on the hardware config
`lshw -class network`

Output
``` bash
[xxxx~]$ lshw -class network
  *-network:0
       description: Ethernet interface
       product: NetXtreme BCM5720 Gigabit Ethernet PCIe
       vendor: Broadcom Inc. and subsidiaries
       physical id: 0
       bus info: pci@0000:04:00.0
       logical name: eno1
       version: 00
       serial: f4:ee:08:51:f8:8e
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: bus_master cap_list rom ethernet physical tp 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
       configuration: xxxxxx
       resources: xxxxx
       
  *-network:0
       description: Ethernet interface
       product: Ethernet Controller X710 for 10GbE SFP+
       vendor: Intel Corporation
       physical id: 0
       bus info: pci@0000:65:00.0
       logical name: ens2f0
       version: 02
       serial: 6c:fe:54:64:49:f0
       size: 10Gbit/s
       capacity: 10Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: bus_master cap_list rom ethernet physical fibre 10000bx-fd autonegotiation
       configuration: xxxxxxx
       resources: xxxx

```


## Ethtool

See Interface information, Including link status

`ethtool <interface name>`

Output
``` bash
[xxxx~]$ ethtool ens2f1
Settings for ens2f1:
        Supported ports: [ FIBRE ]
        Supported link modes:   10000baseSR/Full
        Supported pause frame use: Symmetric Receive-only
        Supports auto-negotiation: Yes
        Supported FEC modes: Not reported
        Advertised link modes:  10000baseSR/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Advertised FEC modes: Not reported
        Speed: 10000Mb/s
        Duplex: Full
        Auto-negotiation: off
        Port: FIBRE
        PHYAD: 0
        Transceiver: internal
netlink error: Operation not permitted
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```