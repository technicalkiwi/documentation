# Compose File Syntax
Example Config File

``` yml
version: "3"
services:

  ghost:
    image: ghost:latest
    restart: always
    depends_on:
      - ghost-db
    ports:
      - 9002:2368
    environment:
      #url: https://$$$$$$$$$$$$$
      database_client: mysql
      database_connection_host: db
      database_connection_user: root
      database_connection_password: $$$$$$$
      database_connection_databse: ghost
    volumes:
      - /home/dockers/ghost/content:/var/lib/ghost/content

  ghost-db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: $$$$$$$
    volumes:
      - /home/dockers/ghost/ghost_mysql:/var/lib/mysql
```

 

**Version**: This is the Version of Docker-compose to use when parsing the file
E.g Version 3

**Services**: Container that defines the Services to run


### First Indent
**Service Name** Create a name for the service, Standard is to name it after the software to be run 
e.g Ghost:

### Second Indent  
**Image**: Defines the image to build the service on 
E.g image: ghost:latest

**Restart**: Defines the behavior for the container upon a power cycle 
E.g restart: always

**Depends_on**: - Defines the other Services this service depends on
This can contain an indented array, each object defined by a - 
E.g depends_on: - ghost-db 

**Ports**: Defines how to map the Hosts ports to the containers ports 
E.g Ports: 9002:2368 binds ports 9002 on the host to port 2368 on the container