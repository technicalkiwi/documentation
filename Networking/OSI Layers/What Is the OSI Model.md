### What is the OSI model?

The OSI model (Open System Interconnection Model) consists of seven different groups, or layers. Each layer builds on top of the other to properly package, address and send data in the packet's payload.

### Advantages of OSI Model

**The OSI model helps users and operators of computer networks:**

-   Determine the required hardware and software to build their network.
-   Understand and communicate the process followed by components communicating across a network. 
-   Perform troubleshooting, by identifying which network layer is causing an issue and focusing efforts on that layer.

**The OSI model helps network device manufacturers and networking software vendors:**

-   Create devices and software that can communicate with products from any other vendor, allowing open interoperability
-   Define which parts of the network their products should work with.
-   Communicate to users at which network layers their product operates – for example, only at the application layer, or across the stack.



 IP networks use the two following addressing methods:

1.  **[Media access control (MAC) address](https://www.techtarget.com/searchnetworking/definition/MAC-address).** This address uses a 16-character hexadecimal address unique to the device's network card. MAC addresses function at Layer 2 and are used to communicate with other devices within the same Layer 2 LAN.
2.  **[IP address](https://www.techtarget.com/whatis/definition/IP-address).** This is a 32-bit numbering scheme administrators can assign to each connected device. IP addressing happens at Layer 3 of the OSI model.