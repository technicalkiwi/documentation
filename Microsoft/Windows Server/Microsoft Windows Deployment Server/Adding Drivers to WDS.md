First step is to add the driver package into the WDS system

-   Open WDS
-   Expand the List on the right hand side
-   Select the drivers folder
-   Right click on the top drivers folder
![](./resources/WDS1.png)
![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddav702ef215f973a7f9a56ec16050772d24.png?version=1&modificationDate=1620692582469&cacheVersion=1&api=v2&width=335&height=313)

-   Select the option to add driver package
-   A new window will pop up
-   Select the option to select all driver packages from a folder
-   Then browse to the location of the drivers
-   They are usually stored under C:\Installs\Drivers\xxxxx

![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddav6a346bdf913eb4ec8329e19baaffdbc5.png?version=1&modificationDate=1620692584269&cacheVersion=1&api=v2&width=406&height=336)
![](./resources/WDS2.png)
-   Once you have selected the correct pathway click next
![](./resources/WDS3.png)
![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddavdaf74ef026ad14500e8cf23553551205.png?version=1&modificationDate=1620692585529&cacheVersion=1&api=v2&width=496&height=458)

-   The window should then start discovering the different drivers within the package
-   This will take a few minutes
-   Once complete it should show a list of the drivers
![](./resources/WDS4.png)
![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddavb1fd93c93f990969e92a11a65dc1a157.png?version=1&modificationDate=1620692587135&cacheVersion=1&api=v2&width=418&height=399)

-   Click next
-   The next page is summary

  ![](./resources/WDS5.png)
![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddavd37519017dbd861568544fe7af92124e.png?version=1&modificationDate=1620692588406&cacheVersion=1&api=v2&width=392&height=381)

-   Select next
-   It should now start installing the drivers
-   When it does some may have failed
-   We can ignore them and move on  
    ![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddavde9860ff030ea328a34f34a51bd8e9e5.png?version=1&modificationDate=1620692589632&cacheVersion=1&api=v2&width=520&height=497)  
      ![](./resources/WDS6.png)
    
-   Next select whether to add them to an existing driver folder or create a new folder  
 ![](./resources/WDS7.png)
     ![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddavd544de29dde24be32c4dd6384f8b646d.png?version=1&modificationDate=1620692591289&cacheVersion=1&api=v2&width=469&height=400)
-   Once you have selected where the drivers are going click next

![](https://technicalkiwi.atlassian.net/wiki/download/thumbnails/1116438530/worddav1d7509c180dbc8040382f6203ee91216.png?version=1&modificationDate=1620692592969&cacheVersion=1&api=v2&width=400&height=389)
![](./resources/WDS8.png)
-   You should now be finished
-   The new Driver package folder will appear under the drivers section in the WDS main page
