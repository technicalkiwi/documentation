Login to the [Microsoft 365 Admin Center](https://admin.microsoft.com/AdminPortal/Home#/homepage)

On the Navigation Panel, Select reports  
Then select Usage  
[![Reports_Center.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Reports_Center.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Reports_Center.png)

CLick on the Select a report Drop down  
[![Reports_Center2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Reports_Center2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Reports_Center2.png)

Select Onedrive  
Select usage  
[![onedrive_usage1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/onedrive_usage1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/onedrive_usage1.png)

Select Time Frame for report  
Export Report  
[![onedrive_usage2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/onedrive_usage2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/onedrive_usage2.png)

It will download the report into a spreadsheet  
[![onedrive_usage3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/onedrive_usage3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/onedrive_usage3.png)

Open in Excel and browse data
[![onedrive_usage4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/onedrive_usage4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/onedrive_usage4.png)