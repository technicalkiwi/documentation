# Creating a ZFS Pool

## Create a Pool

The first zpool we’ll look at is a RAID 0. This works by striping your data across multiple disks. When a file is read from or written to the storage pool, all the disks will work together to present a portion of the data. This offers you a speed boost for your read and write speeds, but it doesn’t do anything for redundancy

`sudo zpool create mypool /dev/sdb /dev/sdc`


## Add a Disk to Existing Pool

If you want to add another hard disk to the pool, take a look at this command where we add hard disk /dev/sdd to our previously created mypool storage pool:

`sudo zpool add mypool /dev/sdd`

## Raid Z
RAID-Z is very similar to RAID 5, but improves upon it with better speed and avoiding some of the common errors associated with RAID 5.

RAID-Z will give you speed plus redundancy by using block level striping and distributed parity. There are three types of RAID-Z available, depending on how much parity you want.

* raidz1 (or just raidz) – single parity
* raidz2 – double parity
* raidz3 – triple parity

`sudo zpool create mypool raidz /dev/sdb /dev/sdc /dev/sdd`


## Remove ZFS Pool

We can destroy our zpool at any time with the following command

`sudo zpool destroy mypool`