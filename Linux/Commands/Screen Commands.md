## Starting Linux Screen

To start a screen session, simply type `screen` in your console:

`screen`
This will open a screen session, create a new window, and start a shell in that window.

Now that you have opened a screen session, you can get a list of commands by typing:
`Ctrl+a` `?`

### Starting Named Session

Named sessions are useful when you run multiple screen sessions. To create a named session, run the screen command with the following arguments:
`screen -S session_name`

> It’s always a good idea to choose a descriptive session name.


## Working with Linux Screen Windows

When you start a new screen session, it creates a single window with a shell in it.

You can have multiple windows inside a Screen session.

To create a new window with shell type `Ctrl+a` `c`, the first available number from the range `0...9` will be assigned to it.

Below are some most common commands for managing Linux Screen Windows:

-   `Ctrl+a` `c` Create a new window (with shell)
    
-   `Ctrl+a` `"` List all window
    
-   `Ctrl+a` `0` Switch to window 0 (by number )
    
-   `Ctrl+a` `A` Rename the current window
    
-   `Ctrl+a` `S` Split current region horizontally into two regions
    
-   `Ctrl+a` `|` Split current region vertically into two regions
    
-   `Ctrl+a` `tab` Switch the input focus to the next region
    
-   `Ctrl+a` `Ctrl+a` Toggle between the current and previous region
    
-   `Ctrl+a` `Q` Close all regions but the current one
    
-   `Ctrl+a` `X` Close the current region
    

### Detach from Linux Screen Session

You can detach from the screen session at any time by typing:

`Ctrl+a` `d`

The program running in the screen session will continue to run after you detach from the session.

### Reattach to a Linux Screen

To resume your screen session use the following command:

`screen -r`

In case you have multiple screen sessions running on your machine, you will need to append the screen session ID after the `r` switch.

To find the session ID list the current running screen sessions with:

``` bash
screen -ls
There are screens on:
    10835.pts-0.linuxize-desktop   (Detached)
    10366.pts-0.linuxize-desktop   (Detached)
2 Sockets in /run/screens/S-linuxize.
```


If you want to restore screen 10835.pts-0, then type the following command:

`1screen -r 10835`

## Basic Linux Screen Usage 

Below are the most basic steps for getting started with screen:

1.  On the command prompt, type `screen`.
    
2.  Run the desired program.
    
3.  Use the key sequence `Ctrl-a` + `Ctrl-d` to detach from the screen session.
    
4.  Reattach to the screen session by typing `screen -r`.



The Linux screen command is extremely useful for the cases when you need to start a long-running process on a remote machine.

Even if your connection drops and the SSH session is terminated, with the screen command you can be sure that the process will keep running in the background and the “lost” terminal session can be resumed.

This note shows how install and how to use the Linux screen command to run a process in the background.

Cool Tip: How to Scroll Up while running the screen command! Read more →

Linux `screen` Command
Install the screen using one of the following commands, depending on your Linux distribution:

` sudo apt-get install screen`
- or -
` sudo yum install screen`
Start a screen session:

` screen`
Press the space or return button to close the license agreement and continue to the screen shell, that looks just like a regular terminal window.

Am I Inside a “Screen”? An interface inside a screen session is exactly as the command prompt. To determine whether you are inside the screen or not, execute the echo `STY command. It returns the name of the screen you’re in. If it is null – you are inside the “real” `terminal.

To start a named screen session, run:

`screen -S <session_name>`
To execute a command in the background but don’t attach to the screen session (useful for system startup scripts), run:

` screen -dm <command>`
- or -
` screen -S <session_name> -dm <command>`
Detach from the screen (disconnect the screen from the terminal and put it into the background): Ctrl + A then D.

To terminate the current screen session (not put it into the background but close), press Ctrl + D or type:

` exit`
List the currently running screen sessions:

` screen -ls`
Reattach to the screen (resume the detached screen session):

` screen -r`
To reattached to the the specific screen session, run:

` screen -r <session_name>`
Kill the specific screen session:

` screen -X -S <session_name> quit`
To kill all the screen sessions, run:

` pkill screen`