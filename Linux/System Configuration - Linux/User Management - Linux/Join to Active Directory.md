# SSSD And REALMD
SSSD and REALMD. Literally so easy, it's unbelievable. Linux machines appear in AD and you can apply GPOs to tweak who can log in to the console, SSH, etc. To start, use your package manager to install REALMD then do the following:


`realm discover ad.mydomain.local --verbose`

This will discover the domain and tell you what other packages need to be installed to join the domain. Once you've installed them, you just do the following:


`realm join ad.mydomain.local --user="administrator" --verbose`

This will join the Linux machine to the domain and create a computer account in the Computers container in AD. Then to enable GPO based access control, so things like "Allow Log on Locally" become enabled, you add the following to the /etc/sssd/sssd.conf under the [domain/ad.mydomain.local] section:

<pre>
access_provider = ad
ad_gpo_access_control = enforcing
</pre>

And then do a systemctl restart sssd.service to restart the SSSD service and pick up the changes to the config file. If you want to specify the OU the computer account goes in, you can specify `--computer-ou="OU=Computers,DC=ad,DC=mydomain,DC=local"` to the join command. You can also specify the OS name and version that appear on the account by adding `--os-name="Ubuntu Linux 20.04 LTS"` and `--os-version="20.04.1"` to the join command.

If you intend to use Webmin or Cockpit with this machine and want to be able to login with your AD credentials, you will need to add the following to sssd.conf under the [domain/ad.mydomain.local] section:


`enumerate = true`

This works around a bug in SUDO that causes the command to return false when Cockpit or Webmin query to see if you have permission to run it or not. Cockpit actually crashes but Webmin states it failed to login.


`ad_gpo_remote_interactive = +webmin, +cockpit`

Replace +webmin and +cockpit with whatever the PAM modules in /etc/pam.d for Webmin or Cockpit and this will lock Webmin and Cockpit to the "Allow log on through Remote Desktop Services" privilege in Group Policy.

Again once the change is made, restart SSSD and then open up visudo and add the following rules:


`%Domain\ Admins        ALL=(ALL:ALL)        ALL`

This results in members of the Domain Admins group in AD being given SUDO access which will be needed by Webmin and Cockpit. Once all is done, you should be good to go to log in to SSH and the console of the machine with an AD account.

If you have any issues logging in, be sure to enable failure audit logging on your domain controller to rule out any account problems. In addition, if you don't require the use of Webmin or Cockpit, do not enable enumeration in SSSD, it adds extra overhead to your domain controllers which if the machines are heavily used, can induce a denial of service. Plus if you have a particularly noisy audit policy enabled, your Security event log is going to be a fucking beef cake in five minutes.

EDIT: Oh and don't forget to do pam_auth_update --enable mkhomedir so that a home directory for your users is enabled when they login.