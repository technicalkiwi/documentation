# Fixing Office 365 Installation

## Issue Overview

The Most Common Cause of Office Activation issues is this little bastard right here

![image-20200722140303841](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722140303841.png)



This Is the Network Connectivity Status Indicator (NCSI) 
Microsoft in their infinite wisdom have decided to make this the source of truth regarding internet connectivity for the Office suite.

![image-20200722141638262](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722141638262.png)



This causes issues when attempting to activate a new installation of office or verify an existing subscription.
Office becomes unlicensed and removes a lot of the functionality.

![image-20200722141613880](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722141613880.png)



Luckily we have found some work arounds.



## For Head Office Computers

#### Fixing NSCI

* Open CMD Prompt
* Run `ipconfig /release`
* Wait a minute or so
* Run `ipconfig /renew`
* Open a browser and browse to an external web page,  e.g spotlight.co.nz
* Watch the NSCI switch from error state to working

![image-20200722142703612](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722142703612.png)



#### Activate Office

* Open any Office program
* login with user account
  ![image-20200722141908287](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722141908287.png)
* Check Office subscription is verified 
  ![image-20200722142512875](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722142512875.png)





## For Store Computers

#### Fix NSCI Error

For a Store computer with a static IP the steps are pretty straight forward

* Set the Primary DNS entry to use Google's DNS : 8.8.8.8
* Wait 30 Seconds
* Open a browser and browse to an external web page,  e.g spotlight.co.nz
* Watch the NSCI switch from error state to working

![image-20200722142703612](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722142703612.png)



#### Activate Office

* Open any Office program
* login with user account
  ![image-20200722141908287](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722141908287.png)
* Check Office subscription is verified 
  ![image-20200722142512875](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200722142512875.png)