# NCSI Issues

The active probe process consists of the following steps:

- Windows 10 or later versions:
  1. NCSI sends a DNS request to resolve the address of the `www.msftconnecttest.com` FQDN.
  2. If NCSI receives a valid response from a DNS server, NCSI sends a plain HTTP GET request to `http://www.msftconnecttest.com`/connecttest.txt.
  3. If NCSI successfully downloads the text file, it makes sure that the file contains Microsoft Connect Test.
  4. NCSI sends another DNS request to resolve the address of the `dns.msftncsi.com` FQDN which should return 131.107.255.255 
     - If any of these requests fails, the network alert appears in the Task Bar (as described in Symptoms). If you hover over the icon, you see a message such as "No connectivity" or "Limited Internet access" (depending on which requests failed).
     - If all of these requests succeed, the Task Bar shows the usual network icon. If you hover over the icon, you see a message such as "Internet access."



### Registry Change

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator]
@=""
"NoActiveProbe"=dword:00000000
"DisablePassivePolling"=dword:00000000
```

- ```
  HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\NlaSvc\Parameters\Internet\EnableActiveProbing
  ```

  - Key Type: DWORD
  - Value: Decimal 0 (False)
    
    

**The below 2 Entries don't exist by default and must be created**

- ```
  HKLM\Software\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator\NoActiveProbe
  ```

  - Key Type: DWORD
  - Value: Decimal 1 (True)

- ```
  HKLM\Software\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator\DisablePassivePolling
  ```

  - Key Type: DWORD
  - Value: Decimal 1 (True)





### Adapter Changes

- Open device manager → select your NIC → open the Advanced tab
- Locate and select IPv4 Checksum Offload in the Properties list
  - `Disable-NetAdapterChecksumOffload -Name "*" -TcpIPv4`
- Disable IPV6





### WinHTTP

Sometimes, we can access the http://www.msftncsi.com/ncsi.txt file in IE, but the NCSI still fails. It is because the NCSI traffic is not sent via IE but via WinHTTP component and use proxy specially. A proxy server which requires user authentication won't allow it access Internet. Basically, NCSI must perform extra steps in an environment that has proxy servers. Web Proxy Automatic Discovery (WPAD) proxy detection is recommended. If WPAD is not used, configure WinHTTP proxy settings to help NCSI:

`netsh winhttp set proxy command`



### Group Policy

To use Group Policy to disable NCSI active probes, configure the following GPO:

Computer Configuration\Administrative Templates\System\Internet Communication Management\Internet Communication settings\Turn off Windows Network Connectivity Status Indicator active tests

* Value: Enabled



To use Group Policy to disable NCSI passive probes, configure the following GPO:

Computer Configuration\Administrative Templates\Network\Network Connectivity Status Indicator\Specify passive polling.

* Value: Enabled

