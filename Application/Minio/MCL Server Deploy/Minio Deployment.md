
# Install Minio Servers
### Minio Prerequisite Setup


Set Hostname
`sudo hostnamectl set-hostname MCL-PRD-MIN01`

> [!Minio Disk Setup]
> MinIO _requires_ that data be on a seperate disk to the operating system

Create Directory for MinIO to store its data
`sudo mkdir /minio`

Label Data Disk
 ``` bash
mkfs.xfs /dev/sdb -L DISK1
```

Add Disks to mount points in `/etc/fstab`
``` bash
  # <file system>  <mount point>  <type>  <options>         <dump>  <pass>
  LABEL=DISK1      /minio     xfs     defaults,noatime  0       2
```

Mount the disk
`sudo mount -a`

**Create minio-user group and user**
``` bash
sudo groupadd -r minio-user
sudo useradd -m -r -g minio-user minio-user
```

**Give ownership of the data directorya to the MinIO user and group**
```bash
sudo chown -R minio-user:minio-user /minio
```


**Create DNS Records for each node**
* min01.connectme365.com  
* min02.connectme365.com 
* min03.connectme365.com 

> [!DNS Records]
> MinIO _requires_ using expansion notation `{x...y}` to denote a sequential series of MinIO hosts when creating a server pool. MinIO therefore _requires_ using sequentially-numbered hostnames to represent each [`minio server`](https://min.io/docs/minio/linux/reference/minio-server/minio-server.html#command-minio.server "minio.server") process in the deployment.
> 
> Create the necessary DNS hostname mappings _prior_ to starting this procedure.
> You can specify the entire range of hostnames using the expansion notation `minio{1...4}.example.com`.

**Time Synchronization**

Multi-node systems must maintain synchronized time and date to maintain stable internode operations and interactions. Make sure all nodes sync to the same time server regularly.

## Minio Setup
**Download mino Deb from source to /tmp**
Make sure to get the latest release from mino
``` bash
cd /tmp
wget https://dl.min.io/server/minio/release/linux-amd64/minio_20230907020502.0.0_amd64.deb 
```


**dpkg deb of minio**
``` bash
sudo dpkg -i minio_*.deb
```

**Install Minio Client**
``` bash
cd /tmp
sudo wget https://dl.min.io/client/mc/release/linux-amd64/mc
sudo chmod +x mc
sudo mv mc /usr/local/bin/mc
```


**Create and open MinIO’s environment file**
`sudo nano /etc/default/minio`

``` bash
MINIO_VOLUMES="https://min0{1...3}.connectme365.com:9000/minio/disk{1...4}/minio"
MINIO_OPTS="--console-address :9001"
MINIO_ROOT_USER=minioadmin
MINIO_ROOT_PASSWORD=Bmc2wyk1iVJQMZycKFBzdM99sFC53P5bANnT8nWwtJBhRu9U6
MINIO_SERVER_URL="https://minio.connectme365.com:9000"

```

**Create exception in firewll**
`sudo ufw allow 9000:9001/tcp`


**Create or Move Certs for HTTPS**
MinIO enables (TLS) 1.2+ automatically upon detecting a valid x.509 certificate (`.crt`) and private key (`.key`) in the MinIO `${HOME}/.minio/certs` directory.

Make directory for certs
``` bash
sudo mkdir /home/minio-user/.minio
sudo mkdir /home/minio-user/.minio/certs
```

Place TLS certificates into /home/minio-user/.minio/certs
``` bash
cp xxx/public.crt  /home/minio-user/.minio/certs
cp xxx/privkey.key  /home/minio-user/.minio/certs
```

Own cert directory as minio-user
``` bash
sudo chown -R minio-user:minio-user /home/minio-user/.minio 
```


> [!Custom Cert location]
> Alternatively, specify a custom certificate directory using `--certs-dir` argument
> Modify the `MINIO_OPTS` variable in `/etc/defaults/minio` to set this option.
> `MINIO_OPTS="--console-address :9001 --certs-dir /opt/minio/certs"`


**Start Service** on all servers
`sudo systemctl start minio`

Check if the webportal is available at the DNS address specified previously

Stop Minio service
`sudo systemctl stop minio`

# Setup Vault Server

## Vault Prerequisites
Set Hostname of host
sudo hostnamectl set-hostname MCL-PRD-VLT01


Set Ip of Host
``` yaml
network:
  ethernets:
    ens160:
      addresses:
      - 10.3.30.93/24
#      gateway4: 10.3.30.254
      routes:
        - to: default
          via: 10.3.30.254
      nameservers:
        addresses:
        - 10.3.30.1
        - 10.3.31.1
        search:
        - cernonz.net.nz

```

Create Vault User
``` bash
sudo groupadd -r vault
sudo useradd -m -r -g vault vault
```

Set Vault user as owner of vault directory
```bash
sudo chown -R vault:vault /opt/vault
```
## Install Vault
Add PGP package to allow installation of signing key
`sudo apt update && sudo apt install gpg`

Download and add PGP key to new keyring
``` bash
cd /tmp
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg 
```

Verify Key
``` bash
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint 
```

Add Hashicorp repo
``` bash
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
```

Run apt update to check new packages
Then install vault

``` bash
sudo apt update && sudo apt upgrade -y
sudo apt install vault
```

Verify Installation of vault
`vault`

> [!NOTE] Vault Error
> If you get an error that the binary could not be found, then your `PATH` environment variable was not setup properly. Please go back and ensure that your `PATH` variable contains the directory where Vault was installed.

## Configure Vault
Copy TLS certs across to the server
``` bash
cp xxx/public.crt  /opt/vault/tls/
cp xxx/privkey.key  /opt/vault/tls/
```

Change public cert to .pem file
``` bash
cd /opt/vault/tls
cp public.crt public.pem
```

Create initial Configuration File
`nano /etc/vault.d/vault-server.hcl`

``` yml
disable_mlock = true
ui            = true
api_addr      = "https://vault.connectme365.com:8200"
  

listener "tcp" {
  address       = "vault.connectme365.com:8200"
  tls_cert_file = "/opt/vault/tls/public.pem"
  tls_key_file  = "/opt/vault/tls/private.key"
}
  

storage "file" {
  path = "/opt/vault/data"
}
```

Export Vault Address and token
``` bash
export HTTP_PROXY="http://vault.connectme365.com"
export HTTPS_PROXY="https://vault.connectme365.com"
export VAULT_ADDRESS="https://vault.connectme365.com"
```


Start Vault
``` bash
vault server -config /etc/vault.d/vault-server.hcl
```

Initialize Vault
``` bash
vault operator init 
```

Take note of the unseal keys and Initial Root token
> [!NOTE] Tip
> If you use the web UI to Initialize the vault you can download the keys and token as a JSON file

Unseal the Vault and Login
```bash
vault operator unseal -address="https://vault.connectme365.com:8200"
# Enter the Unseal Key
vault login -address="https://vault.connectme365.com:8200"
# Enter the Root token
```

Create new Key store
``` bash
vault secrets enable -address="https://vault.connectme365.com:8200" -path=kv kv
```

Create new Approle
``` bash
vault auth enable -address="https://vault.connectme365.com:8200" approle
```

Create a new kes-policy.hcl in /opt/vault/config/kes-policy.hcl
``` hcl
path "kv/*" {
      capabilities = [ "create", "read", "list, "delete" ]
}
```

Write kes-policy to vault
```bash
vault policy write -address="https://vault.connectme365.com:8200" kes-policy /opt/vault/config/kes-policy.hcl
```

Write kes-role config
```bash
vault write -address="https://vault.connectme365.com:8200"    auth/approle/role/kes-role token_num_uses=0 secret_id_num_uses=0 period=5m

vault write -address="https://vault.connectme365.com:8200"    auth/approle/role/kes-role policies=kes-policy
```

Export and take note of Kes Role ID
``` bash
vault read -address="https://vault.connectme365.com:8200"       auth/approle/role/kes-role/role-id
```
<pre>
Key        Value
---        -----
role_id    $ROLEID$
</pre>

Export and take note of Kes Secret ID
``` bash
vault write -address="https://vault.connectme365.com:8200"   -f auth/approle/role/kes-role/secret-id
```
<pre>
Key                   Value
---                   -----
secret_id             $SecretID$
secret_id_accessor    $ACCESSOR$
secret_id_num_uses    0
secret_id_ttl         0s
</pre>


# Setup Loadbalancer
## Load balancer Prerequisites
Update apt cache and Install Nginx
``` bash
sudo apt update && sudo apt upgrade -y
sudo apt install nginx
```

Create cert directory under nginx directory and set nginx as owner
```bash
sudo mkdir /etc/nginx/certs
sudo chown -R www-data:www-data /etc/nginx/*
```

Copy TLS certs across to Nginx server
``` bash
cp xxx/public.crt  /etc/nginx/certs/
cp xxx/privkey.key /etc/nginx/certs/
```

Delete default ngnix config
```bash
sudo rm /etc/nginx/sites-available/default
```

Create new nginx config
``` nginx
upstream minio_s3 {
   least_conn;
   server min01.connectme365.com:9000;
   server min02.connectme365.com:9000;
   server min03.connectme365.com:9000;
}

upstream minio_console {
   least_conn;
   server min01.connectme365.com:9001;
   server min02.connectme365.com:9001;
   server min03.connectme365.com:9001;
}

server {
   listen       80;
   listen  [::]:80;
   server_name  minio.connectme365.com;

   # Allow special characters in headers
   ignore_invalid_headers off;
   # Allow any size file to be uploaded.
   # Set to a value such as 1000m; to restrict file size to a specific value
   client_max_body_size 0;
   # Disable buffering
   proxy_buffering off;
   proxy_request_buffering off;

   location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;

      proxy_connect_timeout 300;
      # Default is HTTP/1, keepalive is only enabled in HTTP/1.1
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      chunked_transfer_encoding off;

      proxy_pass http://minio_s3; # This uses the upstream directive definition to load balance
   }
}

server {
   listen       443 ssl;
   listen  [::]:443;
   server_name  minio.connectme365.com;
   ssl_certificate /etc/nginx/certs/public.crt;
   ssl_certificate_key /etc/nginx/certs/private.key;
   
   # Allow special characters in headers
   ignore_invalid_headers off;
   # Allow any size file to be uploaded.
   # Set to a value such as 1000m; to restrict file size to a specific value
   client_max_body_size 0;
   # Disable buffering
   proxy_buffering off;
   proxy_request_buffering off;

   location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;

      proxy_connect_timeout 300;
      # Default is HTTP/1, keepalive is only enabled in HTTP/1.1
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      chunked_transfer_encoding off;

      proxy_pass https://minio_s3; # This uses the upstream directive definition to load balance
   }
}


server {
   listen       9000 ssl;
   listen  [::]:9000;
   server_name  minio.connectme365.com;
   ssl_certificate /etc/nginx/certs/public.crt;
   ssl_certificate_key /etc/nginx/certs/private.key;

   # Allow special characters in headers
   ignore_invalid_headers off;
   # Allow any size file to be uploaded.
   # Set to a value such as 1000m; to restrict file size to a specific value
   client_max_body_size 0;
   # Disable buffering
   proxy_buffering off;
   proxy_request_buffering off;

   location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;

      proxy_connect_timeout 300;
      # Default is HTTP/1, keepalive is only enabled in HTTP/1.1
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      chunked_transfer_encoding off;

      proxy_pass https://minio_s3; # This uses the upstream directive definition to load balance
   }
}

# Console address for Minio
server {

   listen       443 ssl;
   listen  [::]:433;
   server_name  console.connectme365.com;
   ssl_certificate /etc/nginx/certs/public.crt;
   ssl_certificate_key /etc/nginx/certs/private.key;

   # Allow special characters in headers
   ignore_invalid_headers off;
   # Allow any size file to be uploaded.
   # Set to a value such as 1000m; to restrict file size to a specific value
   client_max_body_size 0;
   # Disable buffering
   proxy_buffering off;
   proxy_request_buffering off;

   location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-NginX-Proxy true;

      # This is necessary to pass the correct IP to be hashed
      real_ip_header X-Real-IP;

      proxy_connect_timeout 300;

      # To support websocket
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";

      chunked_transfer_encoding off;

      proxy_pass https://minio_console/; # This uses the upstream directive definition to load balance
   }
}

# Redirects request to 443 with SSL
server {
    listen 80;
    server_name console.connectme365.com;
    return 301 https://console.connectme365.com$request_uri;
}
```

Link the config file to the sites-enabled file
```bash
sudo ln -s /etc/nginx/sites-available/minio.connectme365.com /etc/nginx/sites-enabled/
```

Test the nginx configs
``` bash
sudo nginx -t
```

If it passes all checks reload the nginx daemon
``` bash
sudo systemctl reload nginx
```

Last, but not least, configure MinIO

In `/etc/default/mino` add the following config

``` config 
MINIO_SERVER_URL="https://minio.connectme365.com"
MINIO_BROWSER_REDIRECT_URL="https://console.connectme365.com"
```

## Install KES
``` bash
cd /tmp
wget https://github.com/minio/kes/releases/download/v0.22.1/kes-linux-amd64
```

Chmod the KES binary and move it to a `PATH` executable location
``` bash
chmod +x kes-linux-amd64
mv kes-linux-amd64 /usr/local/bin/kes

kes --version
```

Create Kes user
```bash 
sudo groupadd -r kes
sudo useradd -m -r -g kes kes
```

```bash
sudo chown -R kes:kes /opt/kes
```

Do not start the server, We need to get Minio configured first

Copy across the TLS certificate for KES to secure communications between KES and the Vault deployment
``` bash
cp xxx/public.crt  /opt/kes/certs/
cp xxx/privkey.key /opt/kes/certs/
```

Create a KES configuration file with the following contents `/opt/kes/config/kes-config.yaml`
``` yml
address: 0.0.0.0:7373
admin:
  identity: disabled

tls:
  key:  /opt/kes/certs/kes-server.key
  cert: /opt/kes/certs/kes-server.cert

policy:
  minio:
    allow:
    - /v1/key/create/*
    - /v1/key/generate/* # e.g. '/minio-'
    - /v1/key/decrypt/*
    - /v1/key/list/*
    identities:
    - $MIN01
    - $MIN02
    - $MIN03
keystore:
  vault:
    endpoint: https://vault.connectme365.com:8200
    engine: "kv/" # Replace with the path to the K/V Engine
    version: "v1" # Specify v1 or v2 depending on the version of the K/V Engine
    approle:
      id: "$APPROLEID"     # Hashicorp Vault AppRole ID
      secret: "$SECRETID$" # Hashicorp Vault AppRole Secret ID
      retry: 15s
    status:
      ping: 10s
```

Fetch the needed information using the following commands

``` bash
kes identity of /opt/minio/certs/minio-kes.cert
Identity:  $MINIO IDENTITY HASH$


vault read -address="https://vault.connectme365.com:8200" auth/approle/role/kes-role/role-id
Key        Value
---        -----
role_id    $APP ROLE ID$



vault write -f auth/approle/role/kes-role/secret-id
Key                   Value
---                   -----
secret_id             $KES SECRET ID$
secret_id_accessor    $ACCESSORID
secret_id_num_uses    0
secret_id_ttl         0s

```

Last, but not least, configure MinIO

In `/etc/default/mino` add the following config

``` config 
# /etc/default/mino
MINIO_KMS_KES_ENDPOINT=https://kes.connectme365.com:7373
MINIO_KMS_KES_CERT_FILE=/home/minio-user/.minio/certs/public.crt
MINIO_KMS_KES_KEY_FILE=/home/minio-user/.minio/certs/private.key
MINIO_KMS_KES_CAPATH=/opt/kes/certs/kes-server.cert
MINIO_KMS_KES_KEY_NAME=minio-backend-default-key
```

Open a new Screen session to start the service
``` bash
screen -S kes
```

Start Kes using the kes-config.yaml file
```bash
sudo setcap cap_ipc_lock=+ep $(readlink -f $(which kes))

sudo kes server --auth=off --config=/opt/kes/config/kes-config.yaml
```

Disconnect from the screen session with Ctrl+a then d

# Test Minio
Start Minio
Open the console webpage  [MinIO Console (connectme365.com)](https://console.connectme365.com/login)
Login 

### Testing
Create a new bucket
Create a new encryption key for the bucket
Create a new user
Create a new Access Key

If all succeeds then make each process into a service 


# Creating Services

> [!NOTE] IMPORTANT
> Make sure that any processes of the same service are completely stopped before bringing via the service

## Minio Service
Minio automatically creates its own service
Make sure its enabled at startup
``` bash
sudo systemctl enable minio
```

## Vault Service
Create a new service file
``` bash
sudo nano /etc/systemd/system/vault.service
```

Create the service

``` shell
[Unit]
Description="HashiCorp Vault - A tool for managing secrets"
Documentation=https://www.vaultproject.io/docs/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/vault.d/vault-server.hcl
StartLimitIntervalSec=60
StartLimitBurst=3

[Service]
Type=notify
Environment="VAULT_ADDR=https://vault.connectme265.com:8200"
EnvironmentFile=/etc/vault.d/vault.env
User=vault
Group=vault
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=yes
PrivateDevices=yes
SecureBits=keep-caps
AmbientCapabilities=CAP_IPC_LOCK
CapabilityBoundingSet=CAP_SYSLOG CAP_IPC_LOCK
NoNewPrivileges=yes
ExecStart=/usr/bin/vault server -config=/etc/vault.d/vault-server.hcl
ExecReload=/bin/kill --signal HUP $MAINPID
KillMode=process
KillSignal=SIGINT
Restart=on-failure
RestartSec=5
TimeoutStopSec=30
LimitNOFILE=65536
LimitMEMLOCK=infinity

[Install]
WantedBy=multi-user.target
```

Start and Enable the service
``` bash
sudo systemctl enable vault
sudo systemctl start vault
```

## KES Service

Create a new service file
``` bash
sudo nano /etc/systemd/system/kes.service
```

Create the service

``` shell
[Unit]
Description="KES"
Documentation=https://github.com/minio/kes/wiki
Wants=network-online.target
After=network-online.target
AssertFileIsExecutable=/usr/local/bin/kes

[Service]
WorkingDirectory=/opt/kes/
AmbientCapabilities=CAP_IPC_LOCK
User=root
Group=root
ProtectProc=invisible
ExecStart=/usr/local/bin/kes server --auth=off --config=/opt/kes/config/kes-config.yaml

# Let systemd restart this service always
Restart=always
# Specifies the maximum file descriptor number that can be opened by this process
LimitNOFILE=65536
# Specifies the maximum number of threads this process can create
TasksMax=infinity
# Disable timeout logic and wait until process is stopped
TimeoutStopSec=infinity
SendSIGKILL=no
[Install]
WantedBy=multi-user.target
```

Start and Enable the service
``` bash
sudo systemctl enable kes
sudo systemctl start kes
```