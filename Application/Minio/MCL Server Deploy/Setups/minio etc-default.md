``` bash

# Set the hosts and volumes MinIO uses at startup
# The command uses MinIO expansion notation {x...y} to denote a
# sequential series.
#
# The following example covers four MinIO hosts
# with 4 drives each at the specified hostname and drive locations.
# The command includes the port that each MinIO server listens on
# (default 9000)

MINIO_VOLUMES="https://minio0{1...3}.connectme365.com:9000/minio/disk{1...4}/minio"
#MINIO_VOLUMES="/minio"

# Set all MinIO server options
#
# The following explicitly sets the MinIO Console listen address to
# port 9001 on all network interfaces. The default behavior is dynamic
# port selection.

MINIO_OPTS="--console-address :9001"

# Set the root username. This user has unrestricted permissions to
# perform S3 and administrative API operations on any resource in the
# deployment.
#
# Defer to your organizations requirements for superadmin user name.

MINIO_ROOT_USER=minioadmin

# Set the root password
#
# Use a long, random, unique string that meets your organizations
# requirements for passwords.

MINIO_ROOT_PASSWORD=Bmc2wyk1iVJQMZycKFBzdM99sFC53P5bANnT8nWwtJBhRu9U6

# Set to the URL of the load balancer for the MinIO deployment
# This value *must* match across all MinIO servers. If you do
# not have a load balancer, set this value to to any *one* of the
# MinIO hosts in the deployment as a temporary measure.
MINIO_SERVER_URL="https://minio.connectme365.com:9000"

# Add these environment variables to the existing environment file

MINIO_KMS_KES_ENDPOINT=https://minio01.connectme365.com:7373
MINIO_KMS_KES_CERT_FILE=/opt/minio/certs/minio-kes.cert
MINIO_KMS_KES_KEY_FILE=/opt/minio/certs/minio-kes.key

# Allows validation of the KES Server Certificate (Self-Signed or Third-Party CA)
# Change this path to the location of the KES CA Path
MINIO_KMS_KES_CAPATH=/opt/kes/certs/kes-server.cert

# Sets the default KMS key for the backend and SSE-KMS/SSE-S3 Operations)
MINIO_KMS_KES_KEY_NAME=minio-backend-default-key

```