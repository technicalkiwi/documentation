## Installing Active Directory Domain Services.

From Server Manager select add roles and features  
[![Install_ADDS1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS1.png)

Select Role Based Installation  
[![Install_ADDS2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS2.png)

Select Server from server pool  
Select local server
[![Install_ADDS3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS3.png)

Check the box for Active Directory Domain Services  
[![Install_ADDS4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS4.png)

Confirm adding the features required for Active Directory Domain services  
[![Install_ADDS5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS5.png)

Select Next on the add features page  
[![Install_ADDS6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS6.png)

Review the changes being made
[![Install_ADDS7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS7.png)

Confirm the Installation
[![Install_ADDS8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS8.png)

Check Installation has succeeded on the the Server
[![Install_ADDS9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_ADDS9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_ADDS9.png)


## Configuring Active Directoy Domain Services
From Server Manager Click on the Flag  
Select Promote this server to a domain controller
[![configure_ADDS1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_ADDS1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_ADDS1.png)