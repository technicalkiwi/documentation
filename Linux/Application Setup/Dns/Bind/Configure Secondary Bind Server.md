[Configure BIND as a slave DNS server](http://www.microhowto.info/howto/configure_bind_as_a_slave_dns_server.html)

## Configuration on Primary Server

#### Ensure Notifications are Enabled
There are two ways to control when zone transfers take place:
-   by polling at regular intervals, or
-   by having the master notify the slave when the zone has changed.

The latter method is preferred as it is both quicker and more efficient.

This can be done for individual zones:
``` shell
zone "technicalkiwi.com" {
  type master;
  file "/var/lib/bind/db.technicalkiwi.com";
  notify yes;
  // ...
};
```

or as the default for all zones:
``` shell
options {
  notify yes;
  // ...
};
```

#### Specify Servers to Notify
The master needs to know which nameservers to notify. By default it notifies the ones that have `NS` records, which for most purposes is sufficient. Nameservers that do not have `NS` records can be notified by adding an `also-notify` statement. As previously this can be done either for an individual zone:
``` shell
zone "tehcnicalkiwi.com" {
  type master;
  notify yes;
  also-notify { 10.10.10.1; };
  file "/var/lib/bind/db.technicalkiwi.com";
};
```

or as the default for all zones:
``` shell
options {
  notify yes;
  also-notify { 10.10.10.1; };
  // ...
};
```

#### Ensure that the primary server allows zone transfers

By default BIND allows zone transfers from anywhere. The servers that are allowed to perform transfers are specified in an `allow-transfer` statement. As with notifications this can be done either for an individual zone:
```shell
zone "technicalkiwi.com" {
  type master;
  notify yes;
  allow-transfer { 10.10.10.1; };
  file "/var/lib/bind/db.technicalkiwi.com";
};
```

as the default for all zones:
```shell
options {
  notify yes;
  allow-transfer { 10.10.10.1; };
  // ...
};
```

If you are content for zone transfers to be unrestricted then you can make this explicit using an address of `any`:
``` shell
options {
  notify yes;
  allow-transfer { any; };
  // ...
};
```


> [!NOTE] Note
> If the configuration has been changed in any way then it should be reloaded after the change




## Configuration on Secondary Server

The `named` configuration file must include a zone declaration for each zone to be served. Here is a suitable declaration for the zone `technicalkiwi.com` on ns1:
Setting the `type` to `slave` specifies that the zone data is obtained from another nameserver.

``` shell
zone "technicalkiwi.com" {
  type slave;
  masters { 10.10.10.21; };
  file "/var/lib/bind/db.technicalkiwi.com";
};
```

> [!NOTE] Notes
> Slave zone files should be placed in `/var/lib/bind` (not `/etc/bind`) so that `named` has permission to write to them. 
> The Masters section must contain an IP address


## Testing 

#### Check the Secondary is serving the Zone
You can test whether `ns1` is operational by using the `dig` command to request the statement of authority record for the zone in question:
The `+norecurs` flag at the end of the command instructs `dig` to perform a non-recursive query.
`dig @203.0.113.1 -t SOA technicalkiwi.com +norecurs`

Inspect the flags that are displayed near the start of the response:
``` shell
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 42311
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 2
```

The `aa` flag is the significant one. If  the seconday is operating as a slave then the `aa` flag will be present, meaning that the answer is authoritative.


#### Check Notifications are working

Make a change to the zone file on the primary server, and increment the serial number
Reload the configuration on the primary, this should trigger the primary to notify its secondary server a new config is availble,  the secondary should then request a zone transfer.

You can determine whether this has happened by re-requesting the SOA record:
`dig @203.0.113.1 -t SOA example.com +norecurs`

If the serial number has changed to the new increment then a zone transfer has occurred
``` shell
;; ANSWER SECTION:
example.com.            86400   IN      SOA     example.com. hostmaster.example.com. 42 28800 7200 604800 86400
```
