
# Install Percona

Add the Percona Repositories
Install percona-xtradb-cluster
``` bash
sudo yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm
sudo percona-release setup pxc-80
sudo yum install percona-xtradb-cluster
```


Post installation start the mysql service and find the temporary IP
``` bash
sudo service mysql start
sudo grep 'temporary password' /var/log/mysqld.log
```

Use the temporary password to log into the server:

``` bash
mysql -u root -p
```


Run an `ALTER USER` statement to change the temporary password, and exit the client,
``` mysql 
ALTER USER 'root'@'localhost' IDENTIFIED BY 'rootPass';
exit
```

Stop the mysql service
``` bash
sudo service mysql stop
```



# Configure Percona Cluster

Configure the `/etc/my.conf` file

Common Config
``` conf
wsrep_provider=/usr/lib64/galera4/libgalera_smm.so
wsrep_cluster_name={{ cluster name }}
# Leave wsrep_cluster_address blank if bootstrapping cluster
#wsrep_cluster_address=gcomm://{{ node1 ip }},{{ node2 ip }},{{ node3 ip }}
```

Node1 Config
``` conf
wsrep_node_name={{ node hostname}}
wsrep_node_address={{ node Ip address}}
pxc_strict_mode=ENFORCING
```

Node2 & 3 config
``` conf
wsrep_node_name={{ node hostname}}
wsrep_node_address={{ node Ip address}}
```


#### Certificate Config Example

``` bash
# Template my.cnf for PXC
# Edit to your requirements.
[client]
socket=/var/lib/mysql/mysql.sock
[mysqld]
server-id=1
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
# Binary log expiration period is 604800 seconds, which equals 7 days
binlog_expire_logs_seconds=604800
######## wsrep ###############
# Path to Galera library
wsrep_provider=/usr/lib64/galera4/libgalera_smm.so
# Cluster connection URL contains IPs of nodes
#If no IP is found, this implies that a new cluster needs to be created,
#in order to do that you need to bootstrap this node
wsrep_cluster_address=gcomm://{{ node1 ip }},{{ node2 ip }},{{ node3 ip }}
# In order for Galera to work correctly binlog format should be ROW
binlog_format=ROW
# Slave thread to use
wsrep_slave_threads=8
wsrep_log_conflicts
# This changes how InnoDB autoincrement locks are managed and is a requirement for Galera
innodb_autoinc_lock_mode=2
# Node IP address
#wsrep_node_address={{ node ip address }}
# Cluster name
wsrep_cluster_name=pxc-cluster
#If wsrep_node_name is not specified,  then system hostname will be used
wsrep_node_name={{ node hostname }}
#pxc_strict_mode allowed values: DISABLED,PERMISSIVE,ENFORCING,MASTER
pxc_strict_mode=ENFORCING
# SST method
wsrep_sst_method=xtrabackup-v2


```


# Bootstrap the Cluster

## Bootstrap First Node

After you [configure all PXC nodes](https://docs.percona.com/percona-xtradb-cluster/8.0/configure-nodes.html#configure), initialize the cluster by bootstrapping the first node. The initial node must contain all the data that you want to be replicated to other nodes.

Bootstrapping implies starting the first node without any known cluster addresses: if the `wsrep_cluster_address` variable is empty, Percona XtraDB Cluster assumes that this is the first node and initializes the cluster.


``` bash
systemctl start mysql@bootstrap.service
```


> [!NOTE] Stopping mysql@bootstrap
> A service started with `mysql@bootstrap` must be stopped using the same command. For example, the `systemctl stop mysql` command does not stop an instance started with the `mysql@bootstrap` command.


Test that the cluster has been initialized
``` bash
show status like 'wsrep%';
```

Expected output
``` mysql
+----------------------------+--------------------------------------+
| Variable_name              | Value                                |
+----------------------------+--------------------------------------+
| wsrep_local_state_uuid     | c2883338-834d-11e2-0800-03c9c68e41ec |
| ...                        | ...                                  |
| wsrep_local_state          | 4                                    |
| wsrep_local_state_comment  | Synced                               |
| ...                        | ...                                  |
| wsrep_cluster_size         | 1                                    |
| wsrep_cluster_status       | Primary                              |
| wsrep_connected            | ON                                   |
| ...                        | ...                                  |
| wsrep_ready                | ON                                   |
+----------------------------+--------------------------------------+
40 rows in set (0.01 sec)
```


## Add Other Nodes to Cluster
New nodes that are [properly configured](https://docs.percona.com/percona-xtradb-cluster/8.0/configure-nodes.html#configure) are provisioned automatically. When you start a node with the address of at least one other running node in the [`wsrep_cluster_address`](https://docs.percona.com/percona-xtradb-cluster/8.0/wsrep-system-index.html#wsrep_cluster_address) variable, this node automatically joins and synchronizes with the cluster.

Configure `/etc/my.cnf`
Add in the cluster IP's
``` conf
wsrep_cluster_name={{ cluster name }}
wsrep_cluster_address=gcomm://{{ node1 ip }},{{ node2 ip }},{{ node3 ip }}
```

Start mysql service
``` bash
systemctl start mysql
```


Check status of the node
``` bash
show status like 'wsrep%';
```

Expected output
``` mysql
+----------------------------------+----------------------------------------+
| Variable_name                    | Value                                  |
+----------------------------------+----------------------------------------+
| wsrep_local_state_uuid           | a08247c1-5807-11ea-b285-e3a50c8efb41   |
| ...                              | ...                                    |
| wsrep_local_state                | 4                                      |
| wsrep_local_state_comment        | Synced                                 |
| ...                              |                                        |
| wsrep_cluster_size               | 2                                      |
| wsrep_cluster_status             | Primary                                |
| wsrep_connected                  | ON                                     |
| ...                              | ...                                    |
| wsrep_provider_capabilities      | :MULTI_MASTER:CERTIFICATION: ...       |
| wsrep_provider_name              | Galera                                 |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>      |
| wsrep_provider_version           | 4.3(r752664d)                          |
| wsrep_ready                      | ON                                     |
| ...                              | ...                                    | 
+----------------------------------+----------------------------------------+
75 rows in set (0.00 sec)
```

The output of `SHOW STATUS` shows that the new node has been successfully added to the cluster. The cluster size is now 2 nodes, it is the primary component, and it is fully connected and ready to receive write-set replication.

If the state of the second node is `Synced` as in the previous example, then the node received full [SST](https://docs.percona.com/percona-xtradb-cluster/8.0/glossary.html#sst) is synchronized with the cluster, and you can proceed to add the next node.


> [!NOTE] State is Joiner
> If the state of the node is `Joiner`, it means that SST hasn’t finished. Do not add new nodes until all others are in `Synced` state


Repeat for all other nodes
`wsrep_cluster_size` should match the number of nodes added

e.g 3 node cluster
``` mysql
+----------------------------+--------------------------------------+
| Variable_name              | Value                                |
+----------------------------+--------------------------------------+
| wsrep_local_state_uuid     | c2883338-834d-11e2-0800-03c9c68e41ec |
| ...                        | ...                                  |
| wsrep_local_state          | 4                                    |
| wsrep_local_state_comment  | Synced                               |
| ...                        | ...                                  |
| wsrep_cluster_size         | 3                                    |
| wsrep_cluster_status       | Primary                              |
| wsrep_connected            | ON                                   |
| ...                        | ...                                  |
| wsrep_ready                | ON                                   |
+----------------------------+--------------------------------------+
40 rows in set (0.01 sec)
```


## Configure Status Check for Load Balancers