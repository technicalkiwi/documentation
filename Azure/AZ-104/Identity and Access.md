# Manage Microsoft EntraID Identities
## Administer Identity



### Federation
Identity Governance

Access package
* Create access packages and assign catalogs to the policy
* Can also assign roles the access package


Identity Protection
* Risk based access controls
* Links to conditional access policies
* Can see risky users / signins

