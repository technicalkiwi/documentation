### Local backup

This option was configured in the `.env` file as a backup option by default.

```
# Setting backup driver to choose local backup via Wings
APP_BACKUP_DRIVER=wings
```

Now, you might wonder where your backup will be stored. For this, you need to write down the path inside Wings’ `config.yml`:

```
system:
  backup_directory: /path/to/backup/storage
```

### S3 backup

AWS S3 can also be used to create a backup. For implementing a backup on S3, change the given configuration in the `.env` file:

```
APP_BACKUP_DRIVER=s3

# Info to actually use s3
AWS_DEFAULT_REGION=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_BACKUPS_BUCKET=
AWS_ENDPOINT=
```

Pterodactyl provides setup options for the reverse proxy, reCAPTCHA, and 2FA.