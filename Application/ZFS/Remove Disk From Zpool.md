# Remove Disk From Existing Zpool

### Find the Disk to Remove
To remove devices from a pool, use the zpool remove command. This command supports removing hot spares, cache, log, and top level virtual data devices. You can remove devices by referring to their identifiers

Find the Identifier of the disk with `zpool status $poolname`

`sudo zpool status zpool`

It will output the pool and the disks
``` bash
        NAME        STATE     READ WRITE CKSUM
        zpool       DEGRADED     0     0     0
          sdc       ONLINE       0     0     0
          sdd       ONLINE       0     0     0
          sde       DEGRADED     0     0     0  too many errors
          sdf       ONLINE       0     0     0
```

### Remove Disk From Pool
Note the name of the disk you want to remove 
Remove the disk with

`sudo zpool remove $poolname $diskname`

E.g `sudo zpool remove $zpool $sde`

### Check Removal Status
Check on the status of the removal with `zpool status $poolname`

It will have a section for removal that will display the current status of removing the disk as well as display the new disk configuration

```bash
  scan: scrub in progress since Mon Jun 13 05:59:21 2022
        1.20T scanned at 1.25G/s, 464G issued at 483M/s, 5.58T total
        0B repaired, 8.12% done, 0 days 03:05:28 to go
remove: Removal of vdev 2 copied 43.1G in 0h7m, completed on Mon Jun 13 05:56:09 2022
    637K memory used for removed device mappings

    config:

        NAME          STATE     READ WRITE CKSUM
        zpool         ONLINE       0     0     0
          sdc         ONLINE       0     0     0
          sdd         ONLINE       0     0     0
          sdf         ONLINE       0     0     0
```


**You can cancel a top-level device removal operation by using the command zpool remove –s.**

