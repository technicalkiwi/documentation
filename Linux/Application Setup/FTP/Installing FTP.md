## Setting up FTP
Overview

## Installation Instructions
We will be installing this on Ubuntu, for other distributions replace "apt" with your chosen package manager.

Update apt repository

`sudo apt update`

Install Vsftpd.

`sudo apt install vsftpd -y`

Once the install has finished, start the daemon and enable it at start up.

`sudo systemctl start vsftpd`

`sudo systemctl enable vsftpd`

Verify the vsftpd service is running.

`sudo systemctl status vsftpd`

The output should look similar to the below.
![Ftp Status](../resources/ftp_status.png)