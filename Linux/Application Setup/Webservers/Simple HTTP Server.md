# Simple HTTP Server
The simple HTTP server module in python is a quick and dirty method of publishing a directory to your local network. 

Why would we need this? Often we need to move the contents of a directory from one machine to another, or continuously use the same files on multiple machines. While this can be completed in many different ways there are a few **advantages** this method has over the others.  

 - HTTP is a ubiquitous protocol and therefore there is a very high likelihood the machine you are trying to receive the files on has access to it. This is not always true for things like USB drives which are often locked out of systems with sensitive information.

- You can publish a directory and access it from many systems at once, which can save you from having to move a USB drive or other method of file transfer from machine to machine.

- Aside from the initial python install (only necessary on windows and the more obscure Linux systems) an HTTP server can be spun up within seconds.

- You can easily use curl and wget to automate file transfers.

There are obviously **disadvantages** to this method.

- The HTTP server is not meant for production as there are almost no security checks when publishing files, which means for anything needing a secure copy environment an HTTP server is unsuitable. 

- When moving large files from a single machine to another I would still recommend using removable media like USB drives especially for large files

- For anything beyond your immediate LAN such as a separate network within your organization or even WAN access this is not an option due to issue no.1 as port forwarding such an insecure connection would be highly unadvisable.  

 

## Setup.

The setup for the python2’s SimpleHTTPServer or the python3’s http.server is straight forward enough.

1. Confirm your system has a working copy of Python. 
   - In both Linux and Windows this can be checked by typing python -Vor python3 -V 
   - If not you can install python3 using sudo apt install python3.8 on Linux 
or download the latest version from python.org/downloads on Windows
2. From here we need to navigate to the directory you want your HTTP server to publish
   - Using the cd command works in both Linux and Windows
3. Once we are in the correct directory we can start our HTTP server,
   - If running python3 we use the command python3 -m http.server
   - If running python2 we use the command python -m SimpleHTTPServer

## Useful additions.

We can add a number to the end of the string to denote the port we wish to use, by default it will use port 8000. Eg: `python3 -m http.server 7800` will serve your current directory with HTTP on port 7800.


If we want to run this task in the background we can easily add an ampersand & to the end of the string Eg. `python -m SimpleHTTPServer 7800` & Which will run as a background task.

You can check on the progress of these tasks by running the jobs command, and bring it back to the front with the foreground fg command.

![Back Ground](../resources/simple_http_additions.png)