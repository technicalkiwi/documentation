
## Create new users

Log into F5 Privileged remote access
[PDC PRA Webtop (lnzra.nz)](https://pra-pdc.lnzra.nz/vdesk/webtop.eui?webtop=/Common/Specialists&webtop_type=webtop_full)

Select Splunk

Login to Splunk


Select Settings > Users
![[create_user2.png]]

Find a user with similar permissions to the new users
> [!NOTE] User Permissions
> Most Users should only have the 'user' permission set

On the selected user select Edit > Clone
![[create_user3.png]]

Fill in User Information
![[create_user4.png]]

> [!NOTE] Force Change Password
> Ensure the must change password option is selected

Save

Send user login details
