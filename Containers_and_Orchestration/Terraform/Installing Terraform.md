# Installing Terraform
## Installing on Linux

Check required packages are installed
`sudo apt-get update && sudo apt-get install -y gnupg software-properties-common`

Download Hashicorp GPG Key and add it to the system

``` bash
wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
   ```
 
Add the Hashicorp repositories 

``` bash
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
```
 

Install Terraform

`sudo apt-get update && sudo apt-get install terraform`

**Enable Tab Completion**
Check bashrc file is there
`touch ~/.bashrc`

Then install the autocomplete package.
`terraform -install-autocomplete`
 

## Installing on Windows

Download the Binary from Downloads | Terraform by HashiCorp
Extract the executable and move it into a folder

Add the executable location into the PATH