
Lotto NZ has a dedicated management domain. This domain is used for the management of infrastructure devices, such as:

- Servers
- Storage arrays
- Network devices
    - Switches
    - Firewalls
    - NTP appliances
- Monitoring servers

