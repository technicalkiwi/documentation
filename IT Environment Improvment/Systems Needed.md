# Work Projects

##### Linux Patch Management

* The foreman + Katello ( Free - Self managed)
* [Uyuni (uyuni-project.org)](https://www.uyuni-project.org/) (Built for Suse)
* Automox ( $3 Per Device per month)
* [orcharhino - orcharhino](https://orcharhino.com/en/)



##### IP Address Management

* [PHPIpam](https://phpipam.net/)
* [Netbox](https://github.com/netbox-community/netbox)



##### Version Control System

* Git Lab
* Git Hub
* Gitea (Self Hosted)
* Azure Repos



##### Documentation Repository

* Confluence
* Bookstack
* Sharepoint
* Git Repo



##### Automation

* Ansible
* Terraform
* Microsoft Deployment Tool (MDT)
* Powershell / Bash Scripts



##### Status/Outage Page

* [System Status Dashboard](http://www.system-status-dashboard.com/home)
* [Staytus](https://github.com/adamcooke/staytus)
* [Cachet](https://cachethq.io/)
* [Cstate](https://github.com/cstate/cstate)



