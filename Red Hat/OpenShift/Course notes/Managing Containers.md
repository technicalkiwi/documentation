
## CRI-O Container Engine Commands

To manage containers with the crictl command, you must first identify the node that is hosting your containers.
`kubectl get pods -o wide`

Connect to that node in debug mode
`oc debug node/$nodename`

Run desired command, e.g
`crictl ps --name postgresql`

### Commands

`crictl pods`
 Lists all pods on a node.
 
 `crictl image`
 Lists all images on a node.
 
 `crictl inspect`
 Retrieve the status of one or more containers.
 
 `crictl exec`
 Run a command in a running container.
 
`crictl logs`
 Retrieve the logs of a container.
 
 `crictl ps`
 List running containers on a node





## Container Information
Get Container id
`crictl ps --name $contianername -o json | jq .containers[0].id`

After identifying the container ID, you can use the crictl inspect command and the container ID to retrieve the PID of the running container
`crictl inspect -o json 27943ae4f3024 | jq .info.pid`

After determining the PID of a running container, you can use the lsns -p PID command to list the system namespaces of a container
`lsns -p $PID`

You can also use the PID of a running container with the nsenter command to enter a specific namespace of a running container
`nsenter -t $PID -p -r ps -ef`
The -t option specifies the target PID for the nsenter command. 
 The -p option directs the nsenter command to enter the process or pid namespace.
 The -r option sets the top-level directory of the process namespace as the root directory, thus
 enabling commands to execute in the context of the namespace

## Image Management
### Skopeo 

Skopeo is another tool to inspect and manage remote container images. With Skopeo, you can copy and sync container images from different container registries and repositories
You also can use Skopeo to inspect the configuration and contents of a container image, and to list the available tags for a container image. Unlike other container image tools, Skopeo can execute without a privileged account, such as root. Skopeo does not require a running daemon to execute various operations.


`skopeo inspect`
View low-level information for an image name, such as environment variables and available tags
You can include the --config flag to view the configuration, metadata, and history of a container repository

`skopeo copy`
Copy an image from one location or repository to another
Use the `skopeo copy $transport://SOURCE-IMAGE $transport://DESTINATION-IMAGE`
e.g `skopeo copy docker://quay.io/skopeo/stable:latest \ docker://registry.example.com/skopeo:latest`

`skopeo delete`
Delete a container image from a repository
e.g `skopeo delete docker://registry.example.com/skopeo:latest`


`skopeo sync`
Synchronize one or more images from one location to another. Use this command to copy all container images from a source to a destination
e.g `skopeo sync --src docker --dest docker \ registry.access.redhat.com/ubi8/httpd-24 registry.example.com/httpd-2`

`skopeo list-tags`
List tags on an image




### OC IMAGE
The OpenShift command-line interface provides the oc image command. You can use this command to inspect, configure, and retrieve information about container images.

`oc image info`
inspects and retrieves information about a container image.

 `oc image append`
 Use this command to add layers to container images, and then push the container image to a
 registry

`oc image extract`
 You can use this command to extract or copy files from a container image to a local disk. Use
 this command to access the contents of a container image without first running the image as a
 container. A running container engine is not required

 `oc image mirror`
 Copy or mirror container images from one container registry or repository to another. For
 example, you can use this command to mirror container images between public and private
 registries. You can also use this command to copy a container image from a registry to a disk.
 The command mirrors the HTTP structure of a container registry to a directory on a disk. The
 directory on the disk can then be served as a container registry


## Copy Files To/From Container


`oc cp $sourcefilepath $destfilepath`
Copy file to or from the container
e.g from container to local host`oc cp apache-app-kc82c:/var/www/html/index.html /tmp/index.bak`
e.g from localhost to container `oc cp /tmp/index.html apache-app-kc82c:/var/www/html/`

`oc rsync $sourcefilepath $destfilepath`
when using the oc CLI, file and directory synchronization is available by using the oc rsync command
e.g `oc rsync apache-app-kc82c:/var/www/ /tmp/web_files`

## Remote Access
You can use the port-forwarding function to expose connectivity to the pod for investigation. With this function, an administrator can connect on the new port and inspect the problematic application
`oc port-forward RESOURCE EXTERNAL_PORT:CONTAINER_PORT`
e.g `oc port-forward nginx-app-cc78k 8080:80`


## Container Management

### Edit Container Manifest

`oc edit pod/mysql-server`

### Connect to Running Container
Administrators use CLI tools to connect to a container via a shell for forensic inspections. With this approach, you can connect to, inspect, and run any available commands within the specified container.

`oc rsh POD_NAME`
e.g `oc rsh nginx-app-cc78k`

The oc rsh command does not accept the -n namespace option. Therefore, you must change to the namespace of the pod before you execute the oc rsh command. If you need to connect to a specific container in a pod, then use the -c container_name option to specify the container name. If you omit this option, then the command connects to the first container in the pod

### Execute Commands in a Container
Passing commands to execute within a container from the CLI is another method for troubleshooting a running container. Use this method to send a command to run within the container, or to connect to the container, when further investigation is necessary.

`oc exec POD | TYPE/NAME [-c container_name] -- COMMAND`
e.g `oc exec -it mariadb-lc78h -- ls /`

> [!NOTE] -it Flag
> It is common to add the -it flags to the kubectl exec or oc exec commands. These flags instruct the command to send STDIN to the container and STDOUT/STDERR back to the terminal. The format of the command output is impacted by the inclusion of the -it flags.


### Show Container Events and Logs
Retrieving the cluster logs provides the chronological details of the container actions. Administrators inspect this log output for information and issues that occur in the running container.
Use the -c container_name to specify a container in the pod. If you omit this option, then the command targets the first container in the pod

`oc logs POD_NAME`
e.g `oc logs BIND9-app-rw43j`

