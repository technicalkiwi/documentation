Go to the [Office Admin Portal](https://admin.microsoft.com/AdminPortal/Home#/homepage)
Open the Users Sub Menu  
Select Active Users  
[![Office_new_user1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user1.png)

Select Add a user  
[![Office_new_user2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user2.png)

Enter User Information  
Set Username to use "briscoegroup.co.nz" Domain  
Select Address to send the user information to  
[![Office_new_user3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user3.png)

Asign the needed license to the user
Select Next
[![Office_new_user4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user4.png)

Do not Add any roles to the user  
[![Office_new_user5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user5.png)

Review the Setup  
If matches the users needs select finish adding  
[![Office_new_user6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Office_new_user6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Office_new_user6.png)