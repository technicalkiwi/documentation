# Troubleshooting Unbound
If you want to check the status of the Unbound DNS, run the following command:

`unbound-control status`
Sample output:
<pre>
version: 1.9.4
verbosity: 0
threads: 2
modules: 3 [ subnet validator iterator ]
uptime: 65 seconds
options: reuseport control(ssl)
unbound (pid 3407) is running...
</pre>

If you want to back up a DNS Cache to a text file, run the following command:

`unbound-control dump_cache > cache.txt`

You can verify the cache.txt file with the following command:

`cat cache.txt`
Sample output:
<pre>
START_RRSET_CACHE
END_RRSET_CACHE
START_MSG_CACHE
END_MSG_CACHE
EOF
</pre
>
In some cases, your DNS server cannot reply to your query. In this case, you can flush the DNS cache using the following command:

unbound-control flush ubuntu.com