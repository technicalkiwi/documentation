![[load_balancing1.png]]


### Public Load Balancing
Maps Public Ip addresses and ports of incoming traffic to Vm's private ip address
Apply load balancing rules to distribute traffic across vm's and services

![[load_balancing2.png]]


### Internal Load Balancing
Directs traffic only to resources inside a virtual network
Frontend IP addresses and vnets are never directly exposed
Enables load balancing within a virtual environment


![[load_balancing3.png]]


### Load Balancer rules
Maps a frontend ip to backend pool
Rules can be combined with NAT rules
NAT rules are explicitly attached to a vm or NIC to complete the path to the target


### Creating Backend Pool
