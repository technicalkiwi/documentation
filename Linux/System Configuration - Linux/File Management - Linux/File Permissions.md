# File Permissions in Linux
## Checking File Current Permissions
 

## Changing File Permissions
To Change File permissions in Linux you use the chmod command

To Add Permissions you would would use “+” 
chmod +rwx filename

To Remove Permissions you add you use a “-” 
chmod -rwx filename

## Change Permissions using Character Code
To change directory permissions in Linux, use the following:

- `chmod +rwx filename` to add permissions.
- `chmod -rwx directoryname` to remove permissions.
- `chmod +x filename` to allow executable permissions.
- `chmod -wx filename` to take out write and executable permissions.

“r” is for read permissions 
“w” is for write permissions 
 “x” is for execute permissions 

## Change Permissions using Numerical Code
You may need to know how to change permissions in numeric code in Linux, so to do this you use numbers instead of “r”, “w”, or “x”.

- 0 = No Permission
- 1 = Execute
- 2 = Write
- 4 = Read

You add up the numbers depending on the level of permission you want to give.

Permission numbers are:

- 0 = ---
- 1 = --x
- 2 = -w-
- 3 = -wx
- 4 = r--
- 5 = r-x
- 6 = rw-
- 7 = rwx

For example:

`chmod 777 foldername` will give read, write, and execute permissions for everyone.

`chmod 700 foldername` will give read, write, and execute permissions for the user only.

`chmod 327 foldername` will give write and execute (3) permission for the user, w (2) for the group, and read, write, and execute for the users.

## Changing File Ownership
These commands will give ownership to someone, but all sub files and directories still belong to the original owner.

`chown username filename`

`chown username foldername`

## Changing Ownership Recursively
You can add -R after the command to make it run recursively, which transfers ownership of all sub directories to the new owner.
`chown username foldername -R`