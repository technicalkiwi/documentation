# Vault Status

### Terminal
Connect to Vault server
`ssh mcladmin@10.3.30.193`

Run vault status
`vault status -address="https://vault.connectme365.com:8200"`

<pre>
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.14.2
Build Date      2023-08-24T13:19:12Z
Storage Type    file
Cluster Name    vault-cluster-e445a4c2
Cluster ID      xxxxxx
HA Enabled      false
</pre>

### API Endpoint
Perform a get request on the seal-status API endpoint

`curl https://vault.connectme365.com:8200/v1/sys/seal-status`

```json
{
    "type": "shamir",
    "initialized": true,
    "sealed": false,
    "t": 1,
    "n": 1,
    "progress": 0,
    "nonce": "",
    "version": "1.14.2",
    "build_date": "2023-08-24T13:19:12Z",
    "migration": false,
    "cluster_name": "vault-cluster-e445a4c2",
    "cluster_id": "xxxxxxxxx",
    "recovery_seal": false,
    "storage_type": "file"
}
```


# Vault Unseal
### Terminal

Connect to Vault server
`ssh mcladmin@10.3.30.193`

Unseal the Vault
`sudo vault unseal`

Enter Unseal Key from Bitwarden
Https://bitwarden.cernonz.com

Login to Vault
`vault login -address="https://vault.connectme365.com:8200"`

Connect to WebGui to check vault access
https://vault.conncetme365.com


### Gui
Connect to vault server webpage
https://vault.conncetme365.com

Enter Unseal Key from Bitwarden
Https://bitwarden.cernonz.com

Login with token from Bitwarden
