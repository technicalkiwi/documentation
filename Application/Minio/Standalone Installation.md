## System Configuration

### Disk Configuration

2 hard disks attached to vm  
1 for OS configured using LVM  
1 labeled as MINIO, mounted at /mnt/minio as xfs

  
Create Directory for MinIO to store its data  
`sudo mkdir /mnt/minio`

Label Data Disk
`mkfs.xfs /dev/sdb -L MINIO`

Add Disks to mount points in `/etc/fstab`
``` shell
  # <file system>  <mount point>  <type>  <options>         <dump>  <pass>
  LABEL=MINIO     /mnt/minio     xfs     defaults,noatime  0       2
```

Mount the disk  
`sudo mount -a`

### Host Files
If you do not have a dns server servicing the minio cluster you will need to setup the hostfile
Host file configured on each Minio host with each server in the cluster and the ip address

``` shell
# /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4

192.168.1.101   minio1.{{ domain }}
192.168.1.102   minio2.{{ domain }}
192.168.1.103   minio3.{{ domain }}
192.168.1.104   minio4.{{ domain }}
```

## Application Configuration

**Create minio-user group and user**

`sudo groupadd -r minio-user`
`sudo useradd -m -r -g minio-user minio-user`

**Give ownership of the data directorya to the MinIO user and group**

`sudo chown -R minio-user:minio-user /mnt/minio`

Download Minio Rpm

Install Minio RPM

Create Minio config file in /etc/default/minio  
Change MINIO_VOLUMES to match cluster configuration  
Set MINIO_ROOT_PASSWORD to a secure string  
Set MINIO_SERVER_URL to the console address or the load balancer address

Example

``` shell
# /etc/default/minio
# Set the hosts and volumes MinIO uses at startup
# The command uses MinIO expansion notation {x...y} to denote a
# sequential series.
#
# The following example covers four MinIO hosts
# with 4 drives each at the specified hostname and drive locations.
# The command includes the port that each MinIO server listens on
# (default 9000)

MINIO_VOLUMES="http://minio{1...4}.{{ domain }}:9000/mnt/minio"

# Set all MinIO server options
#
# The following explicitly sets the MinIO Console listen address to
# port 9001 on all network interfaces. The default behavior is dynamic
# port selection.

MINIO_OPTS="--address :9000 --console-address :9001"

# Set the root username. This user has unrestricted permissions to
# perform S3 and administrative API operations on any resource in the
# deployment.
#
# Defer to your organizations requirements for superadmin user name.

MINIO_ROOT_USER=root

# Set the root password
#
# Use a long, random, unique string that meets your organizations
# requirements for passwords.

MINIO_ROOT_PASSWORD=#SECUREPASSWORDHERE

# Set to the URL of the load balancer for the MinIO deployment
# This value *must* match across all MinIO servers. If you do
# not have a load balancer, set this value to to any *one* of the
# MinIO hosts in the deployment as a temporary measure.
MINIO_SERVER_URL="http://minio.{{ domain }}:9000"
```

Once all servers configured

Enable and start Minio Service