
## Persistent Storage
Containers have ephemeral storage by default. The lifetime of this ephemeral storage does not
 extend beyond the life of the individual pod, and this ephemeral storage cannot be shared across
 pods. When a container is deleted, all the files and data inside it are also deleted. To preserve the
 files, containers use persistent storage volumes.
 
 Because OpenShift Container Platform uses the Kubernetes persistent volume (PV) framework,
 cluster administrators can provision persistent storage for a cluster. Developers can use persistent
 volume claims (PVCs) to request PV resources without specific knowledge of the underlying
 storage infrastructure.
 
 Two ways exist to provision storage for the cluster: static and dynamic. Static provisioning requires
 the cluster administrator to create persistent volumes manually. Dynamic provisioning uses
 storage classes to create the persistent volumes on demand


![[Pasted image 20240522153304.png]]

### Storage Types

##### configMap
 The configMap volume externalizes the application configuration data. This use of
 the configMap resource ensures that the application configuration is portable across
 environments and can be version-controlled.
#####  emptyDir
 An emptyDir volume provides a per-pod directory for scratch data. The directory is usually
 empty after provisioning. emptyDir volumes are often required for ephemeral storage.

##### hostPath
 A hostPath volume mounts a file or directory from the host node into your pod. To use a
 hostPath volume, the cluster administrator must configure pods to run as privileged. This
 configuration grants access to other pods in the same node.
 
 ##### iSCSI
 Internet Small Computer System Interface (iSCSI) is an IP-based standard that provides
 block-level access to storage devices. With iSCSI volumes, Kubernetes workloads can
 consume persistent storage from iSCSI targets.
 
##### local
 You can use Local persistent volumes to access local storage devices, such as a disk or
 partition, by using the standard PVC interface. Local volumes are subject to the availability of
 the underlying node, and are not suitable for all applications.
 
##### NFS
 An NFS (Network File System) volume can be accessed from multiple pods at the same
 time, and thus provides shared data between pods. The NFS volume type is commonly used
 because of its ability to share data safely.

### Volume Access Mode
Persistent volume providers vary in capabilities. A volume uses access modes to specify the modes
 that it supports

| Access mode   | Abbreviation | Description                                    |
| ------------- | ------------ | ---------------------------------------------- |
| ReadWriteOnce | RWO          | A single node mounts the volume as read/write. |
| ReadOnlyMany  | ROX          | Many nodes mount the volume as read-only.      |
| ReadWriteMany | RWX          | Many nodes mount the volume as read/write      |
#### Access mode Support
Developers must select a volume type that supports the required access level by the application

| Volume type          | RWO | ROX | RWX |
| -------------------- | --- | --- | --- |
| configMap            | Y   | N   | N   |
| emptyDir             | Y   | N   | N   |
| emptyDir             | Y   | N   | N   |
| iSCSI             \| | Y   | Y   | N   |
| local                | Y   | N   | N   |
| NFS                  | Y   | Y   | Y   |

### Volume Modes
 Kubernetes supports two volume modes for persistent volumes: Filesystem and Block. 
 If the volume mode is not defined for a volume, then Kubernetes assigns the default volume mode: Filesystem

OpenShift Container Platform can provision raw block volumes. These volumes do not have a file
system, and can provide performance benefits for applications that either write to the disk directly
or that implement their own storage service. Raw block volumes are provisioned by specifying
`volumeMode: Block` in the PV and PVC specification


### Persistent Volume Claim
A persistent volume claim (PVC) resource represents a request from an application for storage. A
 PVC specifies the minimal storage characteristics, such as capacity and access mode. A PVC does
 not specify a storage technology, such as iSCSI or NFS.
 
The lifecycle of a PVC is not tied to a pod, but to a namespace. Multiple pods from the same
 namespace but with potentially different workload controllers can connect to the same PVC. You
 can also sequentially connect storage to and detach storage from different application pods, to
 initialize, convert, migrate, or back up data.

![[pvc.png]]

#### Creating a PVC
A PVC belongs to a specific project. To create a PVC, you must specify the access mode and
 size, among other options. A PVC cannot be shared between projects

To add a volume to an application deployment, use the `oc set volumes` command
``` shell
oc set volumes deployment/example-application \
--add \ 
--name example-pv-storage \ 
--type persistentVolumeClaim \ 
--claim-mode rwo \ 
--claim-size 15Gi \
--mount-path /var/lib/example-app \
--claim-name example-pv-claim
```

Command details one bullet point per line
* Specify the name of the deployment that requires the PVC resource.
* Setting the add option to true adds volumes and volume mounts for containers.
* The name option specifies a volume name. If not specified, a name is autogenerated.
* The supported types, for the add operation, include emptyDir, hostPath, secret, configMap, and persistentVolumeClaim.
* The claim-mode option defaults to ReadWriteOnce. The valid values are ReadWriteOnce (RWO), ReadWriteMany (RWX), and ReadOnlyMany (ROX).
* Create a claim with the given size in bytes, if specified along with the persistent volume type. The size must use SI notation, for example, 15, 15 G, or 15 Gi.
* The mount-path option specifies the mount path inside the container.
* The claim-name option provides the name for the PVC, and is required for the persistentVolumeClaim type.



## Storage Class
Storage classes are a way to describe types of storage for the cluster and to provision dynamic
 storage on demand.

Outside the application function, the developer must also consider the impact of the reclaim
 policy on storage requirements. A reclaim policy determines what happens to the data on a PVC
 after the PVC is deleted

### Dedicated attributes 
#### Parameters
 Parameters can configure file types, change storage types, enable encryption, enable
 replication, and so on. Each provisioner has different parameter options. Accepted parameters
 depend on the storage provisioner. For example, the io1 value for the type parameter, and
 the iopsPerGB parameter, are specific to EBS. When a parameter is omitted, the storage
 provisioner uses the default value.
 
#### Provisioners
 The provisioner attribute identifies the source of the storage medium plug-in. Provisioners
 with names that begin with a kubernetes.io value are available by default in a Kubernetes
 cluster.
#### ReclaimPolicy
 The default reclaim policy, Delete, automatically reclaims the storage volume when the PVC
 is deleted. Reclaiming storage in this way can reduce the storage costs. The Retain reclaim
 policy does not delete the storage volume, so that data is not lost if the wrong PVC is deleted.
 This reclaim policy can result in higher storage costs if space is not manually reclaimed.
#### VolumeBindingMode
 The volumeBindingMode attribute determines how volume attachments are handled for a
 requesting PVC. Using the default Immediate volume binding mode creates a PV to match
 the PVC when the PVC is created. This setting does not wait for the pod to use the PVC, and
 thus can be inefficient. The Immediate binding mode can also cause problems for storage
 back ends that are topology-constrained or are not globally accessible from all nodes in the
 cluster. PVs are also bound without the knowledge of a pod's scheduling requirements, which
 might result in unschedulable pods.
 
 By using the WaitForFirstConsumer mode, the volume is created after the pod that
 uses the PVC is in use. With this mode, Kubernetes creates PVs that conform to the pod's
 scheduling constraints, such as resource requirements and selectors.
#### AllowVolumeExpansion
 When set to a true value, the storage class specifies that the underlying storage volume
 can be expanded if more storage is required. Users can resize the volume by editing the
 corresponding PVC object. This feature can be used only to grow a volume, not to shrink it


## Stateful sets

### Application Clustering
Clustering applications, such as MySQL and Cassandra, typically require persistent storage to
maintain the integrity of the data and files that the application uses. 
 
 When many applications require persistent storage at the same time, multi-disk provisioning might not be possible due to the limited amount of available resources.
 
 Shared storage solves this problem by allocating the same resources from a single device to
 multiple services.


### Storage Services

 File storage solutions provide the directory structure that is found in many environments. Using
 file storage is ideal when applications generate or consume reasonable volumes of organized data.
 Applications that use file-based implementations are prevalent, easy to manage, and provide an
 affordable storage solution.


### Stateful Sets

 A stateful application is characterized by acting according to past states or transactions, which
 affect the current state and future ones of the application. Using a stateful application simplifies
 recovery from failures by starting from a certain point in time.
 
 A stateful set is the representation of a set of pods with consistent identities. These identities are
 defined as a network with a single stable DNS, hostname, and storage from as many volume claims as the stateful set specifies. A stateful set guarantees that a given network identity maps to the same storage identity.

Stateful set pods do not share a persistent volume. Instead, stateful set pods each have their own unique persistent volumes. Pods are created without a replica set, and each replica records its own transactions. Each replica has its own identifier, which is maintained in any rescheduling. You must configure application-level clustering so that stateful set pods have the same data.

Stateful sets are the best option for applications, such as databases, that require consistent
identities and non-shared persistent storage