[How to Install KVM on Ubuntu 22.04 Server (wpdiaries.com)](https://www.wpdiaries.com/kvm-on-ubuntu/)


Install KVM packages
``` bash
sudo apt install -y qemu-kvm virt-manager libvirt-daemon-system virtinst libvirt-clients bridge-utils
```