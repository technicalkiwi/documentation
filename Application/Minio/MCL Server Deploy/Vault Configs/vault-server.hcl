disable_mlock = true
ui            = true
api_addr      = "https://vault.connectme365.com:8200"


listener "tcp" {
  address       = "vault.connectme365.com:8200"
  tls_cert_file = "/opt/vault/tls/public.pem"
  tls_key_file  = "/opt/vault/tls/private.key"
}


storage "file" {
  path = "/opt/vault/data"
}
