Link: [Guide to the Secure Configuration of Red Hat Enterprise Linux 8](https://static.open-scap.org/ssg-guides/ssg-rhel8-guide-pci-dss.html)


## Cryptographic policies
Configure Cryptographic policies

``` bash
sudo update-crypto-policies --set DEFAULT
```

Ensure the following are using the policy and have not been configured to ignore
* ssh
* Keberos
* Libreswan
* Open SSL


## Intrusion Detection
Ensure intrusion detection software is installed and configured

## Access Control
Configure PAM
* Lockout for failed attempts
* Limit password reuse
* Password quality requirements
* Set password hashing algorithm
* last login notifications
* account expiry
* auditd configured to record events
* audit logs owned by root and set to 640 or less

## Bootloader
* Ensure grub is owned by root

## File Ownership
* Ensure sensitive files have correct ownership

## NTP
* Ensure NTP enabled and configured 

## SSH
* Client alive max count to 0
* client alive interval 900
* 
 
 