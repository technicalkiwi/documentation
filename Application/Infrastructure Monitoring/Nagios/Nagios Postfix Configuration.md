set Hostname to $DomainName

`sudo hostnamectl set-hostname briscoe.nz`

  
Install Postfix and Mailutils

`sudo apt install postfix mailutils`

Select Internet site

![](blob:https://technicalkiwi.atlassian.net/4db5b8bb-045d-4777-8a59-228c277af996#media-blob-url=true&id=5cbc63ea-ae2e-4fee-aaa0-643fe7358bdb&collection=contentId-1116471304&contextId=1116471304&mimeType=image%2Fpng&name=image-20210511-004248.png&size=31312&height=449&width=690&alt=)
![Nagios Postfix](./resources/nagios_postfix.png)
  
  

Set Mail Name to $DomainName

  
Edit Postfix configuration file **/etc/postfix/main.cfg** and make following changes.

**Postfix Config**

<pre>
myhostname = nagios.technicalkiwi.com
mydomain = technicalkiwi.com
myorigin = $myhostname
myorigin = $myhostname
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
mydestination = # $myhostname, technicalkiwi.com, tk-lnx-nagios01, localhost.localdomain, localhost
relayhost = 192.168.10.80 #(If Relaying through another mail server)
mynetworks = 127.0.0.0/8 
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = loopback-only
inet_protocols = all
home_mailbox = Maildir
</pre>