# Points of Improvement



## Active Directory

* Remove massive nesting in Distribution Groups
* Set Security Groups by User Role
* Sync User Job Title with Payroll
* User Onboarding/Termination Automation



## File Server

* Access levels by Role not user
* Folder Naming convention
* Lock down Photocopy Top Level



## Access Forms

* Standardize Forms
* Make Fillable PDF Version - Ties into automation
* Make sure everyone is using the most current version of the form



## Documentation

* Remove Redundant Docs
* Update needed docs to be relevant to current setup
* Identify which process's are missing docs
* Setup Lifecycle for Documentationw
* Ensure Future Documentation is created/updated



## Status/Outage Notification

* Status Page for first point of call
* Toast Notifications
* Teams Notifications



## Systems That would be handy

* IPAM Software
* Version Control System (git)
* Configuration Automation Tool
* Deployment Automation - PDQ
* Documentation Repository - Confluence



## Internal Solutions Needed

Internal Certificate Authority - XCA



## Virtual Machines

* Check OS lifecycle for VM's
* Linux Systems patching cycle