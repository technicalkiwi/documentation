``` yaml
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.${dockername}.entrypoints=http"
      - "traefik.http.routers.${dockername}.rule=Host(`${domain}.technicalkiwi.com`)"
      - "traefik.http.middlewares.${dockername}-https-redirect.redirectscheme.scheme=https"
      - "traefik.http.routers.${dockername}.middlewares=${dockername}-https-redirect"
      - "traefik.http.routers.${dockername}-secure.entrypoints=https"
      - "traefik.http.routers.${dockername}-secure.tls=true"
      - "traefik.http.routers.${dockername}-secure.rule=Host(`${domain}.technicalkiwi.com`)"
      - "traefik.http.routers.${dockername}-secure.service=${dockername}"
      - "traefik.http.services.${dockername}.loadbalancer.server.port=${serverport}"
      - "traefik.docker.network=proxy-network"
```


ENV File

``` env
dockername=snipeit
serverport=80
domain=assets
```