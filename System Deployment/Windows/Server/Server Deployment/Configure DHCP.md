### Prerequisites for DHCP
Open Server Manager  
Click on the Flag and select Complete DHCP Configuration  
[![configure_dhcp1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp1.png)

Click next past Description Page  
[![configure_dhcp2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp2.png)

Use your BGR account for Authorization  
Commit The Changes   
[![configure_dhcp3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp3.png)

Review The Summary and Check it was successfull  
[![configure_dhcp4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp4.png)

### Configuring DHCP
Open the DHCP Console  
[![configure_dhcp5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp5.png)

Open the Server Name   
Right Clikc on IPc4 and Select new scope  
[![configure_dhcp6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp6.png)
Click Next past the new Scope Wizard  
[![configure_dhcp7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp7.png)

Give the Scope a descriptive name  
[![configure_dhcp8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp8.png)
Set the Ip Range to be between Store_IP.105 and Store_IP.109  
Change the Length to 26  
[![configure_dhcp9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp9.png)

Do not add any Exclusions or Delay
[![configure_dhcp10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp10.png)

Set Lease Duration to 1 Day
[![configure_dhcp11.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp11.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp11.png)

Select Yes to configure these options now  
[![configure_dhcp12.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp12.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp12.png)

Set the Router to the Stores General gateway, Store_IP.65  then select Add
[![configure_dhcp13.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp13.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp13.png)

The new router should now appear in the list  
[![configure_dhcp14.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp14.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp14.png)

The Domain Services should auto populate, move the Store Sever to be at the top  
[![configure_dhcp15.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp15.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp15.png)

Do not Configure a WINS Server  
[![configure_dhcp16.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp16.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp16.png)

Activate the Scope  
[![configure_dhcp17.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp17.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp17.png)

There should now be a scope available under IPv4  
[![configure_dhcp18.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp18.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp18.png)

### Motorola Configuration

Right Click on IPv4 and Select Define Vendor Classes  
[![configure_dhcp_moto1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto1.png)

Select Add  
[![configure_dhcp_moto2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto2.png)

Fill in the required Fields  
Display name and Description: MotorolaAP6532  
Under the ASCII collum Add the following on Invidual Lines
* Motorola
* AP.AP653
* 2  
[![configure_dhcp_moto3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto3.png)

It Should now be available in the DHCPO Vendor Classes list  
[![configure_dhcp_moto4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto4.png)

Right Click on IPv4 Again and selct Set Predefined Options  
[![configure_dhcp_moto5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto5.png)

Select MotorolaAP6532 From the Dropdown List  
Click on Add  
[![configure_dhcp_moto6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto6.png)

Fill in the needed information  
Name: 192.168.10.29  
DataType: String  
Code: 191  
Description: 192.168.10.29   
[![configure_dhcp_moto7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto7.png)

Add a Value to the string  
Pool=192.168.10.29;level=2  
[![configure_dhcp_moto8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto8.png)

Right Click on Server Options, Select Configure Options  
[![configure_dhcp_moto9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto9.png)

Go to the Advanced Tab  
Select MotorolaAP6532 From the Vendor Class Dropdown  
Tick the Option for 192.168.10.29 
Select Apply  
[![configure_dhcp_moto10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/configure_dhcp_moto10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/configure_dhcp_moto10.png)