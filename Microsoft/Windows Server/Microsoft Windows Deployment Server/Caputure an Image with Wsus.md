## Preparing the OS for Capture

 Remove and old Snapshots from Previous Builds
Clean drive
Change Environment Build Value
![](./resources/WDS9.png)
![](blob:https://technicalkiwi.atlassian.net/c294186e-2636-444b-b09e-639baed65944#media-blob-url=true&id=c8379951-8ce9-47e9-a07e-7a94a2157e53&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=ENV-SOE.png&size=4933&height=90&width=440&alt=)

Create snapshot
Run Sysprep Utility (%WINDIR%\\system32\\sysprep\\sysprep.exe)
Select the options for

-   Audit mode
-   Generalized
-   Reboot

![](./resources/WDS10.png)

Click ok and let it run until its rebooted

Login back in as the admin

Run Scandisk on c:\
![](./resources/WDS11.png)
![](blob:https://technicalkiwi.atlassian.net/577b5eb4-b721-4027-8343-60cd248c5c9c#media-blob-url=true&id=5a82804d-954f-4a3a-9723-0a75a3433d19&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=scan-disk.png&size=59449&height=317&width=434&alt=)

 Clean logs
Create another snapshot
Run Sysprep Again (%WINDIR%\\system32\\sysprep\\sysprep.exe)
Select the options for

-   OOBE
-   Generalized
-   Shutdown

![](./resources/WDS12.png)
![](blob:https://technicalkiwi.atlassian.net/294db41d-b37a-4867-995b-2f8c9e1948ef#media-blob-url=true&id=3dba5885-9044-4aaa-a74d-d917689112d3&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=sysprep-OOBE.png&size=10374&height=191&width=255&alt=)

Let it run and wait for it to shutdown
Take a snapshot

## Capturing the OS

Check VM Nic is connected to Head office network

Force next boot into bios

Network boot to WDS

Select Option for Capture
![](./resources/WDS13.png)
![](blob:https://technicalkiwi.atlassian.net/085d9e1a-d70f-48b7-8625-bde3a06e0921#media-blob-url=true&id=0fcc1abc-b11f-4327-9437-1a63a1a1fe6c&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=image-20210517-044053.png&size=19284&height=250&width=844&alt=)

Click through the welcome screen

Select the volume to capture ( Should detect the drive containing windows  
Name the Image  
Give the Image a Description
![](./resources/WDS14.png)
![](blob:https://technicalkiwi.atlassian.net/31db9eec-b3c9-4fb7-b0cc-edc87591921b#media-blob-url=true&id=ab1f7acc-20cf-4666-ac27-35a939d82209&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=image-20210517-045413.png&size=22957&height=213&width=442&alt=)

Capture the image and target location is the secondary drive.

![](blob:https://technicalkiwi.atlassian.net/fac0d12f-8faa-4038-ba97-31327af3f6ff#media-blob-url=true&id=bacfea67-b746-4d63-a765-47dfb10b09d6&collection=contentId-1095663703&contextId=1095663703&mimeType=image%2Fpng&name=image-20210517-045504.png&size=32194&height=326&width=428&alt=)
![](./resources/WDS15.png)

Capture should begin