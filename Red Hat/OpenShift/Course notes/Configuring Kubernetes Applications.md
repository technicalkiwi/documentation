 When an application is run in Kubernetes with a pre-existing image, the application uses
 the default configuration. This action is valid for testing purposes. However, for production
 environments, you might need to customize your applications before deploying them

With Kubernetes, you can use manifests in JSON and YAML formats to specify the intended
 configuration for each application. You can define the name of the application, labels, the image
 source, storage, environment variables, and more

Example deployment file
``` yaml
 apiVersion: apps/v1 
kind: Deployment 
metadata: 
  name: hello-deployment
 spec: 
  replicas: 1
  selector:
    matchLabels:
      app: hello-deployment
  template:
    metadata:
      labels:
        app: hello-deployment
    spec: 
      containers:
      - env: 
        - name: ENV_VARIABLE_1
          valueFrom:
            secretKeyRef:
              key: hello
              name: world
        image: quay.io/hello-image:latest
```

* apiversion: API version of the resource.
* kind: Deployment resource type.
* metadata: In this section, you specify the metadata of your application, such as the name.
* spec: You can define the general configuration of the resource that is applied to the deployment, such as the number of replicas (pods), the selector label, and the template data.
* spec/spec: In this section, you specify the configuration for your application, such as the image name,the container name, ports, environment variables, and more.
* env: You can define the environment variables to configure your application needs


### Configuration Map

Example configuration map:
``` yaml
apiVersion: v1
 kind: ConfigMap 
metadata:
  name: example-configmap
  namespace: my-app
 data: 
  example.property.1: hello
  example.property.2: world
  example.property.file: |
    property.1=value-1
    property.2=value-2
    property.3=value-3
 binaryData: 
  bar: L3Jvb3QvMTAw
```

* kind: ConfigMap resource type.
* data: Contains the configuration data
* binaryData: Points to an encoded file in base64 that contains non-UTF-8 data, for example, a binary Java keystore file. Place a key followed by the encoded file.



### Secret
Example secret file
``` yaml
 apiVersion: v1
 kind: Secret
 metadata:
  name: example-secret
  namespace: my-app
 type: Opaque 
data: 
  username: bXl1c2VyCg==
  password: bXlQQDU1Cg==
 stringData: 
  hostname: myapp.mydomain.com
  secret.properties: |
    property1=valueA
    property2=valueB
```

* type: Specifies the type of secret
* data: Specifies the encoded string and data
* stringData: Specifies the decoded string and data.


## Persistent Volume

Example persistent volume file
``` yaml
 apiVersion: v1
 kind: PersistentVolume 
metadata:
  name: block-pv 
spec:
  capacity:
    storage: 10Gi 
  accessModes:
    - ReadWriteOnce 
  volumeMode: Block 
  persistentVolumeReclaimPolicy: Retain 
  fc: 
    targetWWNs: ["50060e801049cfd1"]
    lun: 0
    readOnly: false
```

* kind: PersistentVolume is the resource type for PVs
* metadata/name: Provide a name for the PV, which subsequent claims use to access the PV
* spec/storage: Specify the amount of storage that is allocated to this volume
* readwriteonce: The storage device must support the access mode that the PV specifies
* volumemode: The volumeMode attribute is optional for Filesystem volumes, but is required for Block volumes.
* persistentVolumeReclaimPolicy:  The persistentVolumeReclaimPolicy determines how the cluster handles the PV when the PVC is deleted. Valid options are Retain or Delete
* fc: The remaining attributes are specific to the storage type. In this example, the fc object specifies the Fiber Channel storage type attributes


## Storage Class

``` yaml
apiVersion: storage.k8s.io/v1 
kind: StorageClass 
metadata:
  name: io1-gold-storage 
  annotations: 
    storageclass.kubernetes.io/is-default-class: 'false'
    description:'Provides RWO and RWOP Filesystem & Block volumes'
    ...
 parameters: 
  type: io1
  iopsPerGB: "10"
    ...
 provisioner: kubernetes.io/aws-ebs 
reclaimPolicy: Delete 
volumeBindingMode: Immediate 
allowVolumeExpansion: true

```

* apiversion: A required item that specifies the current API version.
* kind: A required item that specifies the API object type.
* metadata/name: A required item that specifies the name of the storage class.
* annotations: An optional item that specifies annotations for the storage class.
* pareameters: An optional item that specifies the required parameters for the specific provisioner; this object differs between plug-ins.
* provisioner: A required item that specifies the type of provisioner that is associated with this storage class.
* reclaimpolicy: An optional item that specifies the selected reclaim policy for the storage class
* volumeBindingMode: An optional item that specifies the selected volume binding mode for the storage class.
* allowvolumeExpansion: An optional item that specifies the volume expansion setting