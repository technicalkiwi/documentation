### Old Server
Open Print Management  
[![Printer_migration1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration1.png)

Under Action select Migrate Printers  
[![Printer_migration2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration2.png)

Select export printer queues and printer drivers to a file  
[![Printer_migration3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration3.png)

Select the local print server  
[![Printer_migration4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration4.png)

Review the list of printers that will be exported  
[![Printer_migration5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration5.png)

Select a location to save the file   
[![Printer_migration6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration6.png)

Export complete  
[![Printer_migration7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration7.png)

Copy the file accross to the new Server  
[![Printer_migration8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration8.png)


## New Server
Open Print Management  
[![Printer_migration9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration9.png)

Select Action and then select Migrate printers
[![Printer_migration10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration10.png)

Select Import printer Queues and printer drivers from file  
[![Printer_migration11.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration11.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration11.png)

Select the file you copied accross  
[![Printer_migration12.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration12.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration12.png)

Review the printers to be installed  
[![Printer_migration13.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration13.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration13.png)

Select this print server   
[![Printer_migration14.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration14.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration14.png)

Select Overwrite existing printers  
Select List All Printers  
[![Printer_migration15.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration15.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration15.png)

The import will begin 
[![Printer_migration16.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration16.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration16.png)

 Import complete check the errors for any major failure  
[![Printer_migration17.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration17.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration17.png)

You should now be able to see the migrated printers
[![Printer_migration18.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Printer_migration18.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Printer_migration18.png)