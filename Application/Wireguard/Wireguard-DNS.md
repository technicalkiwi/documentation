# Wireguard DNS Settings

On distros that use systemd-resolved, you might see an error message like the following if you try to start up a WireGuard interface with wg-quick 

resolvconf: command not found

### CHECK FOR SYSTEMD-RESOLVED
To check if your system is using systemd-resolved, inspect the contents of the /etc/resolv.conf file. If the file starts with a line like the following, it’s using systemd-resolved:

``` bash
$ cat /etc/resolv.conf
# This is /run/systemd/resolve/stub-resolv.conf managed by man:systemd-resolved(8).
...
```

### OVERRIDE DNS FOR SPECIFIC DOMAINS
#### Resolve FQDN Queries
If you use WireGuard to connect to an internal network, and that network includes a custom DNS resolver which resolves an internal domain name for hosts on that network, you can configure systemd-resolved to use that resolver for that particular domain name.

For example, if the resolver’s IP address is **10.10.10.1**, and the domain name is **tkiwi.local**, you might normally use the following DNS settting:

`DNS = 10.10.10.1, tkiwi.local`

With systemd-resolved, however, instead of using the DNS setting, add the following **PostUp** command to the **[Interface]** section of your WireGuard config file:

`PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i ~tkiwi.local`

Example
``` bash

# /etc/wireguard/wg0.conf
[Interface]
PrivateKey = peer_private_key_goes_here
Address = 10.10.10.2/24
PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i ~tkiwi.local

[Peer]
PublicKey = server_public_key_goes_here
AllowedIPs = 10.10.10.0/24
Endpoint = vpn.technicalkiwi.com:51820

```

When you start your WireGuard interface up, this command will direct systemd-resolved to use the DNS server at **10.10.10.1** to resolve queries for the **tkiwi.local** domain name 

#### Multiple Domains to Query
If you have multiple custom domains (like example.com, example.net, and example.org) that you want your internal resolver to resolve, specify them together in the same command, separated by whitespace:
`PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i ~example.com ~example.net ~example.org`

#### Resolve all Queries

If you also want to use tkiwi.local as a search domain (so that systemd-resolved will attempt to resolve queries for unqualified hostnames like *mail* or *chat* as if they were fully-qualified hostnames like *mail.tkiwi.local* or *chat.tkiwi.local*) remove the tilde (~) from the front of **tkiwi.local**:
`PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i tkiwi.local`


### OVERRIDE DNS GLOBALLY
If you use WireGuard to connect to the Internet — or just to a network with a recursive DNS resolver that you prefer to use over your default resolver — you can configure systemd-resolved to use that resolver globally.

For example, if you wanted to use the Quad9 resolver for all Internet DNS queries when your WireGuard interface is up, you’d normally use the following DNS setting:

DNS = 9.9.9.9, 149.112.112.112

With systemd-resolved, however, instead of using that DNS setting, add the following PostUp command to the [Interface] section of your WireGuard config file:

`PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i ~.`

When you start your WireGuard interface up, this command will direct systemd-resolved to use the DNS server at 10.10.10.1 to resolve queries for any domain name.

### AVOID DNS SETTINGS FROM DHCP
If you also use Network Manager, and you connect to networks that you don’t trust, you may also want to adjust Network Manager’s settings for those networks to prevent them from changing your DNS settings (which they may do via DHCP, the Dynamic Host Configuration Protocol).

For example, to modify Network Manager’s settings for your saved Rando Coffee Shop connection to ignore any DNS settings it may push to you over DHCP, run this command:

`nmcli modify 'Wifi SSID' ipv4.ignore-auto-dns yes`
Alternately, you can use the Network Manager UI for this: edit your Rando Coffee Shop connection, switch to its IPv4 Settings tab, and select the Automatic (DHCP) addresses only method:

Automatic (DHCP) addresses only
If you don’t allow DNS to be configured via DHCP, and you don’t configure any DNS servers through Network Manager, systemd-resolved will use the DNS resolvers configured in its base DNS setting from the /etc/systemd/resolved.conf file. There you can configure your preferred DNS servers; for example, to use Quad9:

``` bash

# /etc/systemd/resolved.conf
DNS=9.9.9.9 149.112.112.112 2620:fe::fe 2620:fe::9
```
This base DNS setting will be overridden by any per-connection customizations you make with the PostUp commands above (or any per-connections settings you configure via Network Manager).