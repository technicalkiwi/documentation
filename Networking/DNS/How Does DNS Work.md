The process of DNS resolution involves converting a hostname (such as [www.example.com](http://www.example.com/ "http://www.example.com")) into a computer-friendly IP address (such as 192.168.1.1). An IP address is given to each device on the Internet, and that address is necessary to find the appropriate Internet device - like a street address is used to find a particular home. When a user wants to load a webpage, a translation must occur between what a user types into their web browser ([example.com](http://example.com/ "http://example.com")) and the machine-friendly address necessary to locate the [example.com](http://example.com/ "http://example.com") webpage.

In order to understand the process behind the DNS resolution, it’s important to learn about the different hardware components a DNS query must pass between. For the web browser, the DNS lookup occurs “ behind the scenes” and requires no interaction from the user’s computer apart from the initial request.

There are 4 DNS servers involved in loading a webpage:

-   [DNS recursor](https://www.cloudflare.com/learning/dns/dns-server-types#recursive-resolver "https://www.cloudflare.com/learning/dns/dns-server-types#recursive-resolver") - The recursor can be thought of as a librarian who is asked to go find a particular book somewhere in a library. The DNS recursor is a server designed to receive queries from client machines through applications such as web browsers. Typically the recursor is then responsible for making additional requests in order to satisfy the client’s DNS query.
    

-   **Root nameserver** - The [root server](https://www.cloudflare.com/learning/dns/glossary/dns-root-server/ "https://www.cloudflare.com/learning/dns/glossary/dns-root-server/") is the first step in translating (resolving) human readable host names into IP addresses. It can be thought of like an index in a library that points to different racks of books - typically it serves as a reference to other more specific locations.
    

-   [TLD nameserver](https://www.cloudflare.com/learning/dns/dns-server-types#tld-nameserver "https://www.cloudflare.com/learning/dns/dns-server-types#tld-nameserver") - The top level domain server (TLD) can be thought of as a specific rack of books in a library. This nameserver is the next step in the search for a specific IP address, and it hosts the last portion of a hostname (In [example.com](http://example.com/ "http://example.com"), the TLD server is “com”).
    
-   [Authoritative nameserver](https://www.cloudflare.com/learning/dns/dns-server-types#authoritative-nameserver "https://www.cloudflare.com/learning/dns/dns-server-types#authoritative-nameserver") - This final nameserver can be thought of as a dictionary on a rack of books, in which a specific name can be translated into its definition. The authoritative nameserver is the last stop in the nameserver query. If the authoritative name server has access to the requested record, it will return the IP address for the requested hostname back to the DNS Recursor (the librarian) that made the initial request.
    

What's the difference between an authoritative DNS server and a recursive DNS resolver?

Both concepts refer to servers (groups of servers) that are integral to the DNS infrastructure, but each performs a different role and lives in different locations inside the pipeline of a DNS query. One way to think about the difference is the [recursive](https://www.cloudflare.com/learning/dns/what-is-recursive-dns/ "https://www.cloudflare.com/learning/dns/what-is-recursive-dns/") resolver is at the beginning of the DNS query and the authoritative nameserver is at the end.

**Recursive DNS resolver**

The recursive resolver is the computer that responds to a recursive request from a client and takes the time to track down the [DNS record](https://www.cloudflare.com/learning/dns/dns-records/ "https://www.cloudflare.com/learning/dns/dns-records/"). It does this by making a series of requests until it reaches the authoritative DNS nameserver for the requested record (or times out or returns an error if no record is found). Luckily, recursive DNS resolvers do not always need to make multiple requests in order to track down the records needed to respond to a client; [caching](https://www.cloudflare.com/learning/cdn/what-is-caching/ "https://www.cloudflare.com/learning/cdn/what-is-caching/") is a data persistence process that helps short-circuit the necessary requests by serving the requested resource record earlier in the DNS lookup.

![](blob:https://technicalkiwi.atlassian.net/56adb745-61fe-4d20-b753-25fb8256de1c#media-blob-url=true&id=51b3fbb0-dd19-41da-b63e-a4d76410cff1&collection=contentId-358907905&contextId=358907905&mimeType=image%2Fpng&name=dns-record-request-sequence-1.png&size=35859&height=215&width=755&alt=)
![DNS1](./resources/dns-record-request-sequence-1.png)

**Authoritative DNS server**

Put simply, an authoritative DNS server is a server that actually holds, and is responsible for, DNS resource records. This is the server at the bottom of the DNS lookup chain that will respond with the queried resource record, ultimately allowing the web browser making the request to reach the IP address needed to access a website or other web resources. An authoritative nameserver can satisfy queries from its own data without needing to query another source, as it is the final source of truth for certain DNS records.

![](blob:https://technicalkiwi.atlassian.net/118767ae-7be8-44e9-aab6-cbcf51bb11d2#media-blob-url=true&id=0bac4ee5-5861-4e00-ba9b-4e075482294a&collection=contentId-358907905&contextId=358907905&mimeType=image%2Fpng&name=dns-record-request-sequence-2.png&size=36687&height=230&width=802&alt=)
![DNS2](./resources/dns-record-request-sequence-2.png)

It’s worth mentioning that in instances where the query is for a subdomain such as [foo.example.com](http://foo.example.com/ "http://foo.example.com") or [blog.cloudflare.com](https://blog.cloudflare.com/ "https://blog.cloudflare.com/"), an additional nameserver will be added to the sequence after the authoritative nameserver, which is responsible for storing the subdomain’s [CNAME record](https://www.cloudflare.com/learning/dns/dns-records/dns-cname-record/ "https://www.cloudflare.com/learning/dns/dns-records/dns-cname-record/").

![DNS3](./resources/dns-record-request-sequence-3.png)
![](blob:https://technicalkiwi.atlassian.net/ee786d80-2798-4b37-948e-4158477260ec#media-blob-url=true&id=7dac332a-7689-428f-9c0e-1f2354ce25e6&collection=contentId-358907905&contextId=358907905&mimeType=image%2Fpng&name=dns-record-request-sequence-3.png&size=48515&height=385&width=770&alt=)

There is a key difference between many DNS services and the one that Cloudflare provides. Different DNS recursive resolvers such as Google DNS, OpenDNS, and providers like Comcast all maintain data center installations of DNS recursive resolvers. These resolvers allow for quick and easy queries through optimized clusters of DNS-optimized computer systems, but they are fundamentally different than the nameservers hosted by Cloudflare.

Cloudflare maintains infrastructure-level nameservers that are integral to the functioning of the Internet. One key example is the [f-root server network](https://blog.cloudflare.com/f-root/ "https://blog.cloudflare.com/f-root/") which Cloudflare is partially responsible for hosting. The F-root is one of the root level DNS nameserver infrastructure components responsible for the billions of Internet requests per day. Our [Anycast network](https://www.cloudflare.com/learning/cdn/glossary/anycast-network/ "https://www.cloudflare.com/learning/cdn/glossary/anycast-network/") puts us in a unique position to handle large volumes of DNS traffic without service interruption.