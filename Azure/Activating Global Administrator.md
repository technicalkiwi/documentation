Go to Azure Portal
Log in with Admin account

Search for PIM 
Select Microsoft Entra Privileged Identity Management

![[activate_ga1.png]]

Go Tasks > My Roles in the left-hand menu
![[activate_ga3.png]]

Go to Eligible assignments
Activate Global Administrator

![[activate_ga2.png]]
Set activation window ( Default is 8 hours)
Give a reason for the activation
![[activate_ga4.png]]


Once active it should then show up in the active assignments tab

![[activate_ga5.png]]