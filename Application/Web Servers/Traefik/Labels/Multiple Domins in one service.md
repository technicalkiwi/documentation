To have multiple domains point to the same container/service you need to alter the host rules to include all the domains 

**Method 1**
Seperate each Host rule for the  domain with a || 
The below will route the 2 domains to the same whoami container
e,g
``` yaml
"traefik.http.routers.whoami.rule=Host(`whoami.technicalkiwi.com`) || Host(`whoami.technicalkiwi.tk`)"
```


Make sure to have one for each entry point
e.g 
``` yaml
whoami:
  image: containous/whoami 
  container_name: whoami
  labels: 
    - 'traefik.enable=true' 
    - "traefik.http.routers.whoami.rule=Host(`whoami.technicalkiwi.com`) || Host(`whoami.technicalkiwi.tk`)" 
    - 'traefik.http.routers.whoami.entrypoints=web-secure' 
    - - "traefik.http.routers.whoami-secure.rule=Host(`whoami.technicalkiwi.com`) || Host(`whoami.technicalkiwi.tk`)" 
 
```


**Method 2**
Seperate each domain within the same host rule

e.g
``` yaml
"traefik.http.routers.whoami-secure.rule=Host(`whoami.technicalkiwi.com`,`whoami.technicalkiwi.tk`,`whoami.technicalkiwi.nz`)"
```

Make sure to have one for each entry point
``` yaml
whoami:
  image: containous/whoami 
  container_name: whoami
  labels: 
    - 'traefik.enable=true' 
    - "traefik.http.routers.whoami.rule=Host(`whoami.technicalkiwi.com`,`whoami.technicalkiwi.tk`,`whoami.technicalkiwi.nz`)"  
    - 'traefik.http.routers.whoami.entrypoints=web-secure' 
    - "traefik.http.routers.whoami-secure.rule=Host(`whoami.technicalkiwi.com`,`whoami.technicalkiwi.tk`,`whoami.technicalkiwi.nz`)" 
 
```


**Traefik Example**
It looks like this in Traefik
![Multi domains](mutli_domains1.png)