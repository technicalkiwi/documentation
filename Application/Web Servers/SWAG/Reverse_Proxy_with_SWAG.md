## Understanding the proxy conf structure
### Subdomain proxy conf
Here's the preset proxy conf for Heimdall as a subdomain (ie. https://heimdall.linuxserver-test.com):

``` yaml
# make sure that your dns has a cname set for heimdall

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    server_name heimdall.*;

    include /config/nginx/ssl.conf;

    client_max_body_size 0;

    # enable for ldap auth, fill in ldap details in ldap.conf
    #include /config/nginx/ldap.conf;

    # enable for Authelia
    #include /config/nginx/authelia-server.conf;

    location / {
        # enable the next two lines for http auth
        #auth_basic "Restricted";
        #auth_basic_user_file /config/nginx/.htpasswd;

        # enable the next two lines for ldap auth
        #auth_request /auth;
        #error_page 401 =200 /ldaplogin;

        # enable for Authelia
        #include /config/nginx/authelia-location.conf;

        include /config/nginx/proxy.conf;
        resolver 127.0.0.11 valid=30s;
        set $upstream_app heimdall;
        set $upstream_port 443;
        set $upstream_proto https;
        proxy_pass $upstream_proto://$upstream_app:$upstream_port;

    }
}
```

Let's dissect this conf to look at what each directive or block does.

<code>
server {
}
</code>
This is our server block. Whenever nginx gets a request from a client, it determines which server block should be processed based on the destination server name, port and other relevant info, and the matching server block determines how nginx handles and responds to the request.

<code>
   listen 443 ssl;
    listen [::]:443 ssl;
</code>

This means that only requests coming to port 443 will match this server block.

`server_name heimdall.*;`
Only destination addresses that match heimdall.* will match this server block.

`include /config/nginx/ssl.conf;`
This directive injects the contents of our ssl.conf file here, which contains all ssl related settings (cert location, ciphers used, etc.).

`client_max_body_size 0;`
Removes the size limitation on uploads (default 1MB).

<code>
 # enable for ldap auth, fill in ldap details in ldap.conf
# include /config/nginx/ldap.conf;
</code>
Commented out (disabled) by default. When enabled, it will inject the contents of ldap.conf, necessary settings for LDAP auth.

<code>
    # enable for Authelia
    # include /config/nginx/authelia-server.conf;
</code>
Commented out (disabled) by default. When enabled, it will inject the contents of authelia-server.conf, necessary settings for Authelia integration.

<code>
    location / {
        }
</code>
Location blocks are used for subfolders or paths. After a server block is matched, nginx will look at the subfolder or path requested to match one of the location blocks inside the selected server block. This particular block in our example is for / so it will match any subfolder or path at this address.

<code>
        # enable the next two lines for http auth
        # auth_basic "Restricted";
        # auth_basic_user_file /config/nginx/.htpasswd;
</code>

Commented out (disabled) by default. When enabled, it will use .htpasswd to perform user/pass authentication before allowing access.

<code>
        # enable the next two lines for ldap auth
        # auth_request /auth;
       # error_page 401 =200 /login;
</code>

Commented out (disabled) by default. When enabled, it will use LDAP authentication before allowing access.
<code>
        # enable for Authelia
        # include /config/nginx/authelia-location.conf;
</code>

Commented out (disabled) by default. When enabled, it will use Authelia authentication before allowing access.
 `include /config/nginx/proxy.conf;`

Injects the contents of proxy.conf, which contains various directives and headers that are common for proxied connections.
 `resolver 127.0.0.11 valid=30s;`

Tells nginx to use the docker dns to resolve the IP address when the container name is used as address in the next line.

<code>
        set $upstream_app heimdall;
        set $upstream_port 443;
        set $upstream_proto https;
        proxy_pass $upstream_proto://$upstream_app:$upstream_port;
</code>

> During start, nginx checks all dns hostnames used in proxy_pass statements and if any one of them is not accessible, it refuses to start.


We really don't want a stopped proxied container to prevent our webserver from starting up, so we use a trick.
If the proxy_pass statement contains a variable instead of a dns hostname, nginx doesn't check whether it's accessible or not during start. 

So here we are setting 3 variables, one named upstream_app with the value of heimdall, one named $upstream_port, with the value of the internal heimdall port 443, and one named $upstream_proto with the value set to https. 

We we use these variables as the address in the proxy_pass directive. That way, if the heimdall container is down for any reason, nginx can still start. When using a variable instead of hostname, we also have to set the resolver to docker dns in the previous line.

> If the proxied container is not in the same user defined bridge network as SWAG (could be on a remote host, could be using host networking or macvlan), we can change the value of $upstream_app to an IP address instead: set $upstream_app 192.168.1.10;