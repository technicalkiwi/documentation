Go to the [Azure Dashboard](https://aad.portal.azure.com/#@briscoegroup.co.nz/dashboard/private/0134ffce-00bf-4757-852e-56bbb34ab018)

Go to Azure Active Directory
[![AzureAD_Changing-Roles1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles1.png)

Go to Users  
Select the user you want to manage  
[![AzureAD_Changing-Roles2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles2.png)

Go to Assigned Roles  
[![AzureAD_Changing-Roles3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles3.png)

Select Add Assignment
[![AzureAD_Changing-Roles4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles4.png)

Add in the Roles you want the user to have  
Select Add  
[![AzureAD_Changing-Roles5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles5.png)

You should see a success notification pop up
[![AzureAD_Changing-Roles6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/AzureAD_Changing-Roles6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/AzureAD_Changing-Roles6.png)