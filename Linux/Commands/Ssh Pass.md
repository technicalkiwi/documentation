## Overview

**sshpass** is a utility designed for running **ssh** using the mode referred to as "keyboard-interactive" password authentication, but in non-interactive mode.

ssh uses direct TTY access to make sure that the password is indeed issued by an interactive keyboard user. Sshpass runs ssh in a dedicated tty, fooling it into thinking it is getting the password from an interactive user.

The command to run is specified after sshpass' own options. Typically it will be "ssh" with arguments, but it can just as well be any other command. The password prompt used by ssh is, however, currently hardcoded into sshpass.

## Syntax

**sshpass** [**-f**_**filename**|**-dnumber|-ppassword|-e] [options] command arguments**_

#### Options

If no option is given, sshpass reads the password from the standard input. The user may give at most one alternative source for the password:

-   _**-p**_ password is given on the command line.
    
-   **-f** password is the first line of the file _filename_.
    
-   **-d** _number_ is a file descriptor inherited by sshpass from the runner. The password is read from the open file descriptor.
    
-   **-e** The password is taken from the environment variable "SSHPASS
    
-   **-v** Verbose output
    

### Example

`sshpass -f ./pass.conf ssh bgrman@192.168.10.42`

This will read the password from the first line of the file and pass it through to the ssh command
![ssh Pass](./resources/sshpass.png)
![](blob:https://technicalkiwi.atlassian.net/49848da9-f291-4b67-8f44-4462a06c0b03#media-blob-url=true&id=55a8bd04-de42-4042-a750-e032e7526dd4&collection=contentId-365920265&contextId=365920265&mimeType=image%2Fpng&name=sshpass_use1.png&size=12664&height=162&width=709&alt=)

## Security Considerations

First and foremost, users of sshpass should realize that ssh's insistance on only getting the password interactively is not without reason. It is close to impossible to securely store the password, and users of sshpass should consider whether ssh's public key authentication provides the same end-user experience, while involving less hassle and being more secure.

The -p option should be considered the least secure of all of sshpass's options. All system users can see the password in the command line with a simple "ps" command. Sshpass makes a minimal attempt to hide the password, but such attempts are doomed to create race conditions without actually solving the problem. Users of sshpass are encouraged to use one of the other password passing techniques, which are all more secure.

In particular, people writing programs that are meant to communicate the password programatically are encouraged to use an anonymous pipe and pass the pipe's reading end to sshpass using the -d option.

## Return Values

As with any other program, sshpass returns 0 on success. In case of failure, the following return codes are used:

1.  Invalid command line argument
    
2.  Conflicting arguments given
    
3.  General runtime error
    
4.  Unrecognized response from ssh (parse error)
    
5.  Invalid/incorrect password
    
6.  Host public key is unknown. sshpass exits without confirming the new key.
    

## Further Reading

[https://linux.die.net/man/1/sshpass](https://linux.die.net/man/1/sshpass "https://linux.die.net/man/1/sshpass")  
[https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/](https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/ "https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/")