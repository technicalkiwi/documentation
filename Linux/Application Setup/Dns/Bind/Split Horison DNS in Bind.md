## Prerequisites 
* Have bind9 installed


## Setup

The most important feature of bind that enables this split-view or split horizon configuration is called "views".  Views in bind will let you host a set of configuration for some, and another set of configuration for others.

Views in bind is created using the view clause
``` shell
view "testview" {
};
```

There are two methods to add source addresses that are applicable to your view. The first method is using the statement called "_match-clients_". _match-clients_ requires a source address list as argument.
``` shell
view "testview" {
        `match-clients { 10.1.12.0``/24``; };`
};
```

The second method is to use _acl_ statements. Let's see an example _acl_ statement and how it can be used inside _views_ statement in bind.
``` shell
acl "testacl" { 10.1.12.0/24; };
view "testview" {
    match-clients {"testacl"; };
};
```
