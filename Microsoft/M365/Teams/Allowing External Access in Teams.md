# Allowing External Access in Teams

### Allow or block domains

Enable your organization to communicate with another Teams organization

Go to Org-wide settings > External access.  

Select Add A Domain

[![Teams_Allow_External1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_External1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_External1.png)


Add in the Domain you want to allow access  
Click Save  
[![Teams_Allow_External2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_External2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_External2.png)

Turn on the Users can communicate with other Skype for Business and Teams users setting  
Check the Domain shows as allowed
[![Teams_Allow_External3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_External3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_External3.png)

**Make sure the admin in the other Teams organization completes these same steps**