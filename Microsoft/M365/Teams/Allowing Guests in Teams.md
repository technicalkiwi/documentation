# Allowing Guests in Teams

### Teams Settings

Go to [Teams Admin Center](https://admin.teams.microsoft.com/dashboard)  
Go to org wide settings > Guest Access  
Allow Guest Access
[![Teams_Allow_Guests1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests1.png)

Once this is on, it will show extra settings  
Tweak to fit IT Policy  
[![Teams_Allow_Guests2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests2.png)


### Azure AD Settings

Go To the [Azure Active Directory Admin Center](https://aad.portal.azure.com/#@briscoegroup.onmicrosoft.com/dashboard/private/985d8376-ebbc-44ce-b972-3f6418e9401b)  
Select Azure Active Directory  
[![Teams_Allow_Guests3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests3.png)

Select Organizational relationships  
[![Teams_Allow_Guests4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests4.png)

Select Settings  
Tweak to fit IT Policy  
[![Teams_Allow_Guests5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests5.png)

### Office Settings

Go to the [Office Admin Portal](https://admin.microsoft.com/Adminportal/Home#/)  
Go to [Settings > Settings](https://admin.microsoft.com/Adminportal/Home#/SettingsMultiPivot)  
Select Service Tab  
Select Office 365 Groups  
[![Teams_Allow_Guests6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests6.png)  

Ensure Both options are Ticked  
This allows guests to be added into groups, and then access the group content  
[![Teams_Allow_Guests7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests7.png)

Go to the Security Tab  
Select Sharing  
[![Teams_Allow_Guests8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests8.png)

Ensure the option is ticked  
This allows new guests to be invited ( If off existing Guest will not be affected)
[![Teams_Allow_Guests9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests9.png)


### Share point Settings

Go to the [Share Point Admin Portal](https://briscoegroup-admin.sharepoint.com/_layouts/15/online/AdminHome.aspx)  
Go to Sites > Active Sites  
[![Teams_Allow_Guests10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests10.png)

Select the Site to manage  
Select Sharing  
[![Teams_Allow_Guests11.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests11.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests11.png)

Ensure Permissions are set to Anyone or New and Existing Guests  
[![Teams_Allow_Guests12.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests12.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests12.png)


### Application Settings

Within the Teams Application go to the team to manage  
Select the 3 dots to bring up the option menu  
Select Manage Team  
[![Teams_Allow_Guests13.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests13.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests13.png)

Go to Setting Tab  
Select Guest Permissions  
Tweak to match IT Policy  
[![Teams_Allow_Guests14.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Allow_Guests14.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Allow_Guests14.png)