Go to the [Azure AD Admin Portal](https://aad.portal.azure.com/)

Select Azure Active Directory  
[![AzureAD_Creating_Security_Group1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group1.png)

Select Groups  
[![AzureAD_Creating_Security_Group2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group2.png)

Select New Group  
[![AzureAD_Creating_Security_Group3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group3.png)

Set Group Type as Security  
Enter a Group Name  
Create a Group Description  
[![AzureAD_Creating_Security_Group4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group4.png)

Set a Group Owner to Global Admin  
[![AzureAD_Creating_Security_Group5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group5.png)

Under Members Heading click " No Member Selected"  
This will bring up the menu to add in Members  
Add in all required Members  
[![AzureAD_Creating_Security_Group6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group6.png)

Review the Changes to be made match expectations  
Select Create   
[![AzureAD_Creating_Security_Group7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group7.png)

You should see a notification about creating the group  
[![AzureAD_Creating_Security_Group8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group8.png)

Once Complete the Group should now be available under the groups tab
[![AzureAD_Creating_Security_Group9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/AzureAD_Creating_Security_Group9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/AzureAD_Creating_Security_Group9.png)