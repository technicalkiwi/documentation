## Opening and Creating Files 

To open an existing file or to create a new file, type `nano` followed by the file name:  
E.g `nano flag.txt`

**To open a file you must have read permissions to the file**

This opens a new editor window, and you can start editing the file.  
At the bottom of the window, there is a list of the most basic command shortcuts to use with the nano editor.

![](blob:https://technicalkiwi.atlassian.net/43c1b039-f636-48d7-acb4-df7ee4860400#media-blob-url=true&id=e477082e-9327-4bf9-a0a2-32db070d5448&collection=contentId-316211267&contextId=316211267&mimeType=image%2Fpng&name=image-20200626-010854.png&size=63801&height=481&width=816&alt=)
![nano](./resources/nano.png)

All commands are prefixed with either `^` or `M` character. The caret symbol (`^`) represents the `Ctrl` key. For example, the `^J` commands mean to press the `Ctrl` and `J` keys at the same time. The letter `M` represents the `Alt` key.

You can get a list of all commands by typing `Ctrl+g`.

## Editing Files

Unlike vi, nano is a modeless editor, which means that you can start typing and editing the text immediately after opening the file

To move the cursor to a specific line and character number, use the `Ctrl+_` command. The menu on the bottom of the screen will change. Enter the number(s) in the “Enter line number, column number:” field and hit `Enter`.

### Searching and replacing

To search for a text, press `Ctrl+w`, type in the search term, and press `Enter`. The cursor will move to the first match. To move to the next match, press `Alt+w`.

If you want to search and replace, press `Ctrl+\`. Enter the search term and the text to be replaced with. The editor will move to the first match and ask you whether to replace it. After hitting `Y` or `N` it will move to the next match. Pressing `A` will replace all matches.

### Copping, cutting, and pasting

To select text, move the cursor to the beginning of the text and press `Alt+a`. This will set a selection mark. Move the cursor to the end of the text you want to select using the arrow keys. The selected text will be highlighted. If you want to cancel the selection press `Ctrl+6`

Copy the selected text to the clipboard using the `Alt+6` command. `Ctrl+k` will cut the selected text.

If you want to cut whole lines, simply move the cursor to the line and press `Ctrl+k`. You can cut multiple lines by hitting `Ctrl+k` several times.

To paste the text move the cursor to where you want to put the text and press `Ctrl+u`.

### Saving and Exiting

To save the changes you’ve made to the file, press `Ctrl+o`. If the file doesn’t already exist, it will be created once you save it.

To exit nano press `Ctrl+x`. If there are unsaved changes, you’ll be asked whether you want to save the changes.

To save the file, you must have at write permissions to the file. If you are [creating a new file](https://linuxize.com/post/create-a-file-in-linux/ "https://linuxize.com/post/create-a-file-in-linux/"), you need to have write permission to the directory where the file is created  
  

## Customizing Nano (nanorc)

When nano is launched, it reads its configuration parameters from the system-wide configuration file `/etc/nanorc` and from the user-specific files `~/.config/nano/nanorc` and `~/.nanorc` if the files are present.

Options specified in the user files take precedence over the global options.

Visit the [nanorc](https://www.nano-editor.org/dist/latest/nanorc.5.html "https://www.nano-editor.org/dist/latest/nanorc.5.html") page for a complete list of all available option.

### Syntax Highlighting

Nano ships with syntax highlighting rules for most popular file types. On most Linux systems, the syntax files are stored in the `/usr/share/nano` directory and included by default in the `/etc/nanorc` configuration file.

`1include "/usr/share/nano/*.nanorc"`

The easiest option to enable highlighting for a new file type is to copy the file containing the syntax highlighting rules to the `/usr/share/nano` directory.