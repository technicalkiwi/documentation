
Create Directories on host
``` bash
mkdir -p /opt/kes/certs
mkdir -p /opt/kes/config
mkdir -p /opt/minio/certs
mkdir -p /opt/minio/config
mkdir -p ~/minio
```

## Install KES
``` bash
cd /tmp
wget https://github.com/minio/kes/releases/download/v0.22.1/kes-linux-amd64
```

Chmod the KES binary and move it to a `PATH` executable location
``` bash
chmod +x kes-linux-amd64
mv kes-linux-amd64 /usr/local/bin/kes

kes --version
```

Create Kes user
```bash 
sudo groupadd -r kes
sudo useradd -m -r -g kes kes
```

```bash
sudo chown -R kes:kes /opt/kes
```

Do not start the server, We need to get Minio configured first
## [Configure Minio Server](MinIO_Setup.md)


Create a TLS certificate for KES to secure communications between KES and the Vault deployment

``` bash
 kes identity new kes_server \
--key  /opt/kes/certs/kes-server.key  \
--cert /opt/kes/certs/kes-server.cert  \
--ip   "127.0.0.1"  \
--dns  localhost
```

Next, we’ll set a TLS certificate for MinIO to perform mTLS authentication to KES

``` bash
 kes identity new minio_server \
--key  /opt/minio/certs/minio-kes.key  \
--cert /opt/minio/certs/minio-kes.cert  \
--ip   "127.0.0.1"  \
--dns  localhost
```

Set Minio-User as owner of certs
``` bash
sudo chown -R minio-user:minio-user /minio
sudo chown -R minio-user:minio-user /opt/minio
```

Create a KES configuration file with the following contents `/opt/kes/config/kes-config.yaml`
``` yml
address: 0.0.0.0:7373
admin:
  identity: disabled
  
tls:
  key:  /opt/kes/certs/kes-server.key
  cert: /opt/kes/certs/kes-server.cert

policy:
  minio:
    allow:
    - /v1/key/create/*
    - /v1/key/generate/* # e.g. '/minio-'
    - /v1/key/decrypt/*
    identities:
    - $MINIO_IDENTITY HASH$
keystore:
  vault:
    endpoint: https://vault.connectme365.com:8200
    engine: "kv/" # Replace with the path to the K/V Engine
    version: "v2" # Specify v1 or v2 depending on the version of the K/V Engine
    approle:
      id: "$APP ROLE ID$"     # Hashicorp Vault AppRole ID
      secret: "KES SECRET ID" # Hashicorp Vault AppRole Secret ID
      retry: 15s
    status:
      ping: 10s

```

Fetch the needed infomation using the following commands

``` bash
kes identity of /opt/minio/certs/minio-kes.cert
Identity:  $MINIO IDENTITY HASH$


vault read -address="https://vault.connectme365.com:8200" auth/approle/role/kes-role/role-id
Key        Value
---        -----
role_id    $APP ROLE ID$



vault write -f auth/approle/role/kes-role/secret-id
Key                   Value
---                   -----
secret_id             $KES SECRET ID$
secret_id_accessor    16442cd9-8ba6-e730-042a-6ff756f2947f
secret_id_num_uses    0
secret_id_ttl         0s

```

Last, but not least, configure MinIO

In `/etc/default/mino` add the following config

``` config 
# /etc/default/mino
MINIO_KMS_KES_ENDPOINT=https://localhost:7373
MINIO_KMS_KES_CERT_FILE=/opt/minio/certs/minio-kes.cert
MINIO_KMS_KES_KEY_FILE=/opt/minio/certs/minio-kes.key
MINIO_KMS_KES_CAPATH=/opt/kes/certs/kes-server.cert
MINIO_KMS_KES_KEY_NAME=minio-backend-default-key
```

Open a new Screen session to start the service
``` bash
screen -S kes
```

Start Kes using the kes-config.yaml file
```bash
sudo setcap cap_ipc_lock=+ep $(readlink -f $(which kes))

sudo kes server --auth=off --config=/opt/kes/config/kes-config.yaml
```

Disconnect from the screen session with Ctrl+a then d