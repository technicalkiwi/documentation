
## Navigating with SFTP
### Remote Navigation
Print remote working Directory 
`pwd`

List the contents of the current directory on the remote machine:
`ls`

Change remote working directory
`cd $newlocation`

### Local Navigation
Print Local working Directory 
`lpwd`

List the contents of the current directory on the local machine:
`lls`

Change local working directory
`lcd $newlocation`



## Transferring Files with SFTP

### Transferring Remote Files to the Local System
 Download files from remote host
`get $remoteFile`

Copy the remote file to a different name
`get $remoteFile $localFile`

Copy a directory and all of its contents by specifying the recursive option
`get -r someDirectory`

Maintain the appropriate permissions and access times by using the `-P` or `-p` flag
`get -Pr someDirectory`


### Transferring Local Files to the Remote System
Upload files from local host
`put $localFile`

Upload an entire local directory
`put -r $localDirectory`


### Disk space Check

Check Disk Free Space
`df -h`
<pre>
Output 
Size     Used    Avail   (root)    %Capacity
19.9GB   1016MB   17.9GB   18.9GB           4%
</pre>