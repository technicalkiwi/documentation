
## Htpasswd
Connect to Open shift cluster

### Create new user

Retrieve the htpasswd file from the `htpass-secret` `Secret` object and save the file to your file system:

``` bash
oc get secret htpass-secret -ojsonpath={.data.htpasswd} -n openshift-config | base64 --decode > users.htpasswd
```


#### Add the new user account to the file
**Method 1**
``` bash
htpasswd -bB users.htpasswd <username> <password>
```

**Method 2

Have the user create an htpasswd file and send it to you
``` bash
htpasswd -c -B -b $FILENAME $USERNAME $PASSWORD
```

Manually open the users.htpasswd file from openshift
Insert users htpasswd string
Save file
``` bash
nano users.htpasswd
```

#### Upload to Openshift
Upload the updated users.htpasswd file to the openshift cluster
``` bash
oc create secret generic htpass-secret --from-file=htpasswd=users.htpasswd --dry-run=client -o yaml -n openshift-config | oc replace -f -
```

