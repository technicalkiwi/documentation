
## Openshift Overview
Red Hat OpenShift Container Platform (RHOCP) is a complete platform to run applications in clusters of servers. OpenShift is based on existing technologies such as containers and Kubernetes. Containers provide a way to package applications and their dependencies that can be executed on any system with a container runtime. 


#### Integrated developer workflow
* When running applications on Kubernetes, you need to build and store container images for the applications.
* OpenShift integrates a built-in container registry, CI/CD pipelines, and S2I, a  tool to build artifacts from source repositories to container images.
####  Observability
* To achieve the intended reliability, performance, and availability of applications, cluster administrators might need additional tools to prevent and solve issues.
 * OpenShift includes monitoring and logging services for both your applications and the cluster.
#### Server management
* Kubernetes requires an operating system to run on that must be installed, configured, and maintained. 
* OpenShift provides installation and update procedures for many scenarios.
* Hosts in a cluster use Red Hat Enterprise Linux CoreOS (RHEL CoreOS) as
  the underlying operating system. 
* OpenShift also brings unified tools and a graphical web console to manage all the different capabilities, and additional enhancements such as improved security measures


### Open Shift Components

#### Red Hat Enterprise Linux CoreOS
CoreOS is an immutable operating system that is optimized for running containerized applications

