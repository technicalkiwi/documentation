## Application Installation

### Nagios Core

Install Prerequisites

`1sudo apt-get install -y autoconf gcc libc6 make wget unzip apache2 php libapache2-mod-php7.4 libgd-dev`

Download and unzip the application

`1cd /tmp 2wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.5.tar.gz 3tar xzf nagioscore.tar.gz 4`

Compile the application

`1cd /tmp/nagioscore-nagios-4.4.5/ 2sudo ./configure --with-httpd-conf=/etc/apache2/sites-enabled 3sudo make all 4`

![](blob:https://technicalkiwi.atlassian.net/a91d8831-3b4f-4625-9ade-74e48e6e5c92#media-blob-url=true&id=256ee33b-9666-4c4c-9637-e71ea11b326a&collection=contentId-1116373014&contextId=1116373014&mimeType=image%2Fpng&name=image-20210511-003738.png&size=28710&height=392&width=573&alt=)

Create User account and group

`1sudo make install-groups-users 2sudo usermod -a -G nagios www-data 3`

Install Binaries

`1sudo make install`

Install Daemon and set to start on boot

`1sudo make install-daemoninit`

Install Command file

`1sudo make install-commandmode`

Install Sample Config file

`1sudo make install-config`

Install Apache2 Components

`1sudo make install-webconf 2sudo a2enmod rewrite 3sudo a2enmod cgi 4`

Configure firewall

`1sudo ufw allow Apache 2sudo ufw reload 3`

###

Create an admin account for the nagios webgui, this will prompt for a password.

`1sudo htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin`

Restart apache to apply configuration changes

`1sudo systemctl restart apache2.service`

Start Nagios Service

`1sudo systemctl start nagios.service`

Test Installation by going to $IP address/nagios. e.g [http://bgr-lnx-nagios01/nagios](http://bgr-lnx-nagios01/nagios "http://bgr-lnx-nagios01/nagios")

Login with username: nagiosadmin and the password configured earlier

**But Wait**

Currently you have only installed the Nagios Core engine. You'll notice some errors under the hosts and services along the lines of:

`1(No output on stdout) stderr: execvp(/usr/local/nagios/libexec/check_load, ...) failed. errno is 2: No such file or directory`

### Nagios Plugins

Install Prerequisistes

`1sudo apt-get install -y autoconf gcc libc6 libmcrypt-dev make libssl-dev wget bc gawk dc build-essential snmp libnet-snmp-perl gettext`

Download and extract source files

`1cd /tmp 2wget --no-check-certificate -O nagios-plugins.tar.gz https://github.com/nagios-plugins/nagios-plugins/archive/release-2.2.1.tar.gz 3tar zxf nagios-plugins.tar.gz 4`

Compile and install

`1cd /tmp/nagios-plugins-release-2.2.1/ 2sudo ./tools/setup 3sudo ./configure 4sudo make 5sudo make install 6`

Test plugins by going to a host or service object and "Re-schedule the next check" under the Commands menu. The error you previously saw should now disappear and the correct output will be shown on the screen.