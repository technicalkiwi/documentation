In Hyper-V Manager select New  
Then Select Virtual machine  
[![Create_VM1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM1.png)]  

Click Next Passed the Before you Begin Screen  
[![Create_VM2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM2.png)  

Give the Virtual Machine a Name  
Specify Where to Store VM if different from the default.  
[![Create_VM3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM3.png)  

Set the Machine Generation   
[![Create_VM4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM4.png)  

Set The Amount of Memory to be Allocated  
[![Create_VM5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM5.png)  

Attach to a Virtual Swtich  
[![Create_VM6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM6.png)  

Create a Virtual Disk for the VM  
Set Name ( If different from default)  
Set Storage Location ( If different from default)    
Set Size  
[![Create_VM7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM7.png)

Select Install Operating System Later  
[![Create_VM8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Create_VM8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Create_VM8.png)  

Check through Summary to Ensure settings are correct  
Create the Virtual Machine