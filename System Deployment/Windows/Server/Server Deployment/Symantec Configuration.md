Login to [Symantec Web Portal](https://brisym14:8443/console/apps/sepm)


### Host Configuration

Select Clients  
Go to the Default group  
[![sym_Portal1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal1.png)

Find the Client you want to move  
Right click on it   
Select move  
[![sym_Portal2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal2.png)

Move the Client to the Appropriate Group  
[![sym_Portal3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal3.png)

Enter the Group  
Check you can see the Client has been moved Successfully.
[![sym_Portal4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal4.png)


### Update Intrusion Policy

Select Policies  
Double Click on Camera Intrustion Prevention Policy  
[![sym_Portal5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal5.png)

Select Intrusion Prevention  
Select Excluded Hosts  
[![sym_Portal6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal6.png)

Tick the Server you have just moved  
Click Ok, then OK again  
[![sym_Portal7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal7.png)

Select Intrusion Prevention Policy  
[![sym_Portal8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal8.png)

Select Intrusion Prevention  
Select Excluded Hosts   
[![sym_Portal9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal9.png)

Untick the Server you have just moved  
Click Ok, then OK again  
[![sym_Portal10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/sym_Portal10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/sym_Portal10.png)