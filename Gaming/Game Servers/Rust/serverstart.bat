echo off
:start 

C:\steamcmd\steamcmd.exe +login anonymous +force_install_dir c:\rustserver\ +app_update 258550 +quit 

RustServer.exe -batchmode ^
+server.port 28015 ^
+server.level "Procedural Map" ^
+server.seed 1234 ^
+server.worldsize 4000 ^
+server.maxplayers 10  ^
+server.hostname "Name of Server as Shown on the Client Server List" ^
+server.description "Description shown on server connection window." ^
+server.url "http://yourwebsite.com" ^
+server.headerimage "http://yourwebsite.com/serverimage.jpg" ^
+server.identity "server1" ^
+rcon.port 28016 ^
+rcon.password letmein ^
+rcon.web 1

goto start