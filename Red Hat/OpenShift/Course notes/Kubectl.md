
The kubectl CLI provides the following commands:
– kubectl describe: Display the details of a resource.
– kubectl edit: Edit a resource configuration by using the system editor.
– kubectl patch: Update a specific attribute or field for a resource.
– kubectl replace: Deploy a new instance of the resource.
– kubectl cp: Copy files and directories to and from containers.
– kubectl exec: Execute a command within a specified container.
– kubectl explain: Display documentation for a specified resource.
– kubectl port-forward: Configure a port forwarder for a specified container.
– kubectl logs: Retrieve the logs for a specified container.