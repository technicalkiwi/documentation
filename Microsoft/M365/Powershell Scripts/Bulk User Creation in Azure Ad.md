### Purpose
To bulk create new users in Azure AD

### Usage
Fill in new users details in a CSV, the script then runs through and creates each new user in Azure AD  
Example of CSV File  
[![Upload_csv_Example.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/Upload_csv_Example.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/Upload_csv_Example.png)


### Script
```powershell
# Setup Usage Location
$Location = "NZ"

# Pull in CSV of new users
$csvpath = "Path\To\File\AzureAD_Users_Upload.csv"
$NewAzureAdUsers = Import-Csv -Path $csvpath

# Connect to Azure AD Tenant
$AzureAdCred = Get-Credential
Connect-AzureAD -Credential $AzureAdCred

#  Creates Objects for each new user
foreach ($NewAzureAdUser in $NewAzureAdUsers){
$firstname = $NewAzureAdUser.firstname
$lastname = $NewAzureAdUser.lastname
$Username = $NewAzureAdUser.username
$UserPrincipal = $Username + "@briscoegroup.co.nz"
$Password = $NewAzureAdUser.password


# Create Objects to feed to User Creation
$Displayname = $Firstname + " " + $Lastname

# Setup Password object for User Creation
$PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
$PasswordProfile.Password = $Password
$PasswordProfile.EnforceChangePasswordPolicy = $false

# Create New Azure AD User
New-AzureADUser -DisplayName $Displayname -PasswordProfile $PasswordProfile -UserPrincipalName $UserPrincipal -AccountEnabled $true -MailNickName $username
Set-AzureADUser -ObjectID $UserPrincipal -UsageLocation $Location
}
```

