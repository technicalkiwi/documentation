## Extend Physical disk
Log into Vsphere
Extend the disk in VM settings

Rescan the disk after upgrading its size
``` bash
echo 1>/sys/class/block/sda/device/rescan
```

Check disk size with `lsblk`
## Extend Partition
Check which partition the LVM group is using with vgs
``` bash
[root@xxxx ~]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <49.00g 4.00m
  rhel     1   2   0 wz--n- <33.00g    0
[root@bdc-cat-auto1 ~]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda               8:0    0  134G  0 disk
├─sda1            8:1    0    1G  0 part
├─sda2            8:2    0   49G  0 part
│ ├─centos-swap 253:1    0  3.9G  0 lvm
│ └─centos-root 253:3    0 45.1G  0 lvm
├─sda3            8:3    0    1G  0 part /boot
├─sda4            8:4    0    1K  0 part
└─sda5            8:5    0   33G  0 part
  ├─rhel-root   253:0    0 29.6G  0 lvm  /
  └─rhel-swap   253:2    0  3.4G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom

```

Resize the partition the LVM group is sitting on using `parted`
`parted /dev/sda`

View the current partition table:

``` bash
[root@xxxx ~]# parted /dev/sda
(parted) print

Model: VMware Virtual disk (scsi)
Disk /dev/sda: 144GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type      File system  Flags
 1      1049kB  1075MB  1074MB  primary   xfs          boot
 2      1075MB  53.7GB  52.6GB  primary                lvm
 3      53.7GB  54.8GB  1074MB  primary   xfs
 4      54.8GB  90.2GB  35.4GB  extended               lba
 5      54.8GB  90.2GB  35.4GB  logical                lvm

```

From the partition table, determine:
- The minor number of the partition.
- The location of the existing partition and its new ending point after resizing.

> [!NOTE] Resizing
> This uses the partitions new ending point not the total size

Resize the partition:
`(parted) resizepart <Partition_Number>  <New_Ending_Point>GiB`

Or use a % of the free space
`(parted) resizepart <Partition_Number>  100%`

View the partition table to confirm that the resized partition is in the partition table with the correct size:
``` bash
(parted) resizepart 5 120GiB
(parted) print


Model: VMware Virtual disk (scsi)
Disk /dev/sda: 144GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type      File system  Flags
 1      1049kB  1075MB  1074MB  primary   xfs          boot
 2      1075MB  53.7GB  52.6GB  primary                lvm
 3      53.7GB  54.8GB  1074MB  primary   xfs
 4      54.8GB  129GB   74.1GB  extended               lba
 5      54.8GB  129GB   74.1GB  logical                lvm

```

Exit the `parted` shell:
`(parted) quit`

Verify that the kernel registers the new partition: `cat /proc/partitions`
``` bash
[root@xxx ~]# cat /proc/partitions

major minor  #blocks  name

   8        0  140509184 sda
   8        1    1048576 sda1
   8        2   51379200 sda2
   8        3    1048576 sda3
   8        4          0 sda4
   8        5   72350720 sda5
  11        0    1048575 sr0
 253        0   31031296 dm-0
 253        1    4063232 dm-1
 253        2    3567616 dm-2
 253        3   47308800 dm-3

```

## Extend Volume Group Size

Extend the pv:
`pvresize /dev/sda3`

List your volume groups, the size should be adjusted automatically
`vgs`
``` bash
[root@xxx ~]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <49.00g  4.00m
  rhel     1   2   0 wz--n- <69.00g 36.00g
```

## Extend Logical Volume

Extend the logical volume
`lvextend -r -L <New_size>G <volume_name>`
Or to take all free space
`lvextend -r -l +100%FREE <volumename>`

``` bash
[root@xxxx ~]# lvextend -r -L 65G /dev/mapper/rhel-root

  Size of logical volume rhel/root changed from 29.59 GiB (7576 extents) to 65.00 GiB (16640 extents).
  File system xfs found on rhel/root mounted at /.
  Extending file system xfs to 65.00 GiB (69793218560 bytes) on rhel/root...
xfs_growfs /dev/rhel/root
meta-data=/dev/mapper/rhel-root  isize=512    agcount=4, agsize=1939456 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1    bigtime=1 inobtcount=1
data     =                       bsize=4096   blocks=7757824, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=3788, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 7757824 to 17039360
xfs_growfs done
  Extended file system xfs on rhel/root.
  Logical volume rhel/root successfully resized.

```

Check the changes have taken effect with `vgs`
``` bash
 [root@xxx ~]# vgs
 
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <49.00g   4.00m
  rhel     1   2   0 wz--n- <69.00g 608.00m
[root@bdc-cat-auto1 ~]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda               8:0    0  134G  0 disk
├─sda1            8:1    0    1G  0 part
├─sda2            8:2    0   49G  0 part
│ ├─centos-swap 253:1    0  3.9G  0 lvm
│ └─centos-root 253:3    0 45.1G  0 lvm
├─sda3            8:3    0    1G  0 part /boot
├─sda4            8:4    0  512B  0 part
└─sda5            8:5    0   69G  0 part
  ├─rhel-root   253:0    0   65G  0 lvm  /
  └─rhel-swap   253:2    0  3.4G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom
```

## Last check
Check the filesystems with `df -h`

``` bash
[root@xxx ~]# df -h

Filesystem             Size  Used Avail Use% Mounted on
devtmpfs               4.0M     0  4.0M   0% /dev
tmpfs                  7.7G  168K  7.7G   1% /dev/shm
tmpfs                  3.1G   54M  3.1G   2% /run
/dev/mapper/rhel-root   65G   13G   53G  20% /
/dev/sda3             1014M  249M  766M  25% /boot
tmpfs                  1.6G  4.0K  1.6G   1% /run/user/0

```