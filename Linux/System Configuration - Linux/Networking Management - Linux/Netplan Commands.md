# Netplan Commands

`netplan generate`

Use /etc/netplan to generate the required configuration for the renderers.

`netplan apply`

Apply all configuration for the renderers, restarting them as necessary.

`netplan try`

Apply configuration and wait for user confirmation; will roll back if network is broken or no confirmation is given.

`netplan get`

Merge and display all the current available configuration on the system.

`netplan set`

Add new setting by specifying a dotted key=value pair like ethernets.eth0.dhcp4=true.

`netplan info`

Show available feature flags of the currently installed version as YAML.

`netplan ip`

Retrieve IP information from the system.

`netplan help`

Show the help message