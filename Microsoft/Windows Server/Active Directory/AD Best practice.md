When you plan to build an Active Directory infrastructure, it is good to know some tricks to avoid security and configuration issues:

**Rename Domain Admin** – The first user used to start an attack is administrator, so your first step is change the default domain admin name; use a naming completely different from standards, like AdminContosoAD.

**Strong Password for Domain Admin** – Security, security, security! The Domain Admin must have a strong password and the credentials must be reserved.

**Dedicated Credentials for IT** – One of first rule is to separate default credentials from management to avoid security escalation in case of external attack.

**Assign the Right Permission** – If you have multi admins in your infrastructure is fundamental to assign the right permission and credentials for each user. No one should be over Domain Admins to avoid possibility to change the AD Schema or modify the Forest model.

**Configure GPO** – Configuring Group Policies by Users and Computers, this allows the perfect granularity. Remember to avoid too many GPOs but also to consolidate many settings in one single GPO. **Don’t use the Default Domain Policy GPO!**

**Strong Password for Users** – Not only for the Domain Admin, all users must follow the password complexity requirements. If you use Windows 10, one idea is to configure **Windows Hello for Business** to simplify the authentication method, without reducing security.

**Enable Recycle Bin** – The Recycle Bin was introduced in Windows Server 2008 R2 and is the perfect way to restore an item in few seconds, without having to run AD Restore.

**At Least Two Domain Controller** – It does matter if your infrastructure is not an enterprise, you should have two Domain Controller to prevent critical failure.

**Remove Obsolete Items** – Don’t forget to clean your infrastructure from users and computers where they are no longer present or necessary. This is to avoid issues or security problems.

**A Domain Controller is not a Computer** – Don’t install anything inside a Domain Controller! No software, no third-part applications, no roles, nothing! A DC must be clean!

**Naming Convention Rule** – Define a naming convention before build your infrastructure, users, clients, servers, devices and resources (groups, share, more). This will help you to simply management and scalability.

**Patch Your DCs** – Attackers are quick to exploit known vulnerabilities, this means that you must always keep updated your machine. Plan the right schedule time to install the Windows updates.

Auditing – Deploy an auditing solution to know who make changes. This is not a GDPR requirements but is also a way to prevent security issues.

## Best Practice Virtual Machine

There are a couple of rules to bear in mind, when you build a Domain Controller in a virtual machine:

**Virtualized DC is Supported** – Starting Windows Server 2012, when a new feature called _VM Generation-ID_ was added, is supported install a Domain Controller as virtual machine. Should be used a physical DC? Depends on the infrastructure but for the major of companies the answer is no. What is important, is to configure the Start Action as Always Start in 0 seconds.

**Do Not Checkpoint Virtualized** – Checkpoints for DC are now supported but could be better to avoid this operation.

**Disable Time Synchronization** – Domain Controllers expect that they are at the top of the local time hierarchy and leave the Host Time Synchronization Service causes it to override any other source set for the Windows Time service and this could cause issues.

**Do Not Place Domain Controllers in Saved State** – When a virtual machine resumes from a saved state or is reverted to a checkpoint, the only thing that is guaranteed to fix its clock is the Time Synchronization Service. But, as you know from above, you can’t enable that on virtualized domain controllers. If its clock skews too far, it might never fix itself automatically.

**Do Not Convert Domain Controller** – It does matter if you have a physical or virtual DC, the convert is wrong and **not supported**. If you want migrate from VMware to Hyper-V, the Domain Controller must be reinstalled from zero; same thing if you have physical DC.

**Upgrade in Place** – Like conversion, the upgrade in place is not supported so if you want install a new version of Windows Server, plan to deploy a new machine, add into the AD Forest, move the FSMO roles and demote the oldest Domain Controller. There’s no other way!

**Replica** – The replica should not be used in the majority of cases. If you have a remote Disaster Recovery site, could be much better configure another Domain Controller and use the AD Replica system because is better.