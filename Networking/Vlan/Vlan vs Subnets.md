### VLANs vs. subnets: Key differences

Without getting too deep into the inner workings of VLANs vs. subnets, network administrators must understand the VLAN's purpose is to communicate with other devices within the same LAN, which is known as _intra-VLAN communication_. IP subnets, on the other hand, transmit data between VLANs -- dubbed _inter-VLAN communication_.

Where things get muddled, however, is that devices within the same VLAN must also be configured within the same IP subnet. Essentially, intra-VLAN communication requires [both a MAC address and an IP address](https://www.techtarget.com/searchnetworking/answer/What-is-the-difference-between-an-IP-address-and-a-physical-address) within the same subnet to communicate.

### VLANs and subnets in practice

As a business adds more devices to a LAN, a single VLAN [becomes overutilized](https://blog.ipspace.net/2020/04/stupidity-stretched-vlan.html) due to an increase in broadcast traffic. These broadcasts bog down the network until it can no longer transmit and receive data efficiently.

To counter this, network administrators can create additional VLANs. Creating multiple VLANs breaks up the broadcast domains and reduces the amount of broadcast traffic on each LAN segment. However, when a device on one VLAN needs to talk to another device on a different VLAN, it cannot do so without going through a router interface.

Router interfaces operate at Layer 3 of the OSI model and require IP to communicate. Static routes or dynamic routing protocols can direct traffic initiated from one VLAN to another. Thus, each VLAN must be configured to split into separate broadcast domains at Layer 2 and include how to communicate between VLANs at Layer 3.

To accomplish this configuration, a network administrator must first create a unique IP subnet for each VLAN. Next, the administrator must assign the subnet a gateway IP address, which is the router interface's IP address. When traffic travels between VLANs, the packets are directed first to the VLAN's default gateway. From there, the router will perform a routing table lookup to find the destination IP subnet and send the traffic to the corresponding gateway address.