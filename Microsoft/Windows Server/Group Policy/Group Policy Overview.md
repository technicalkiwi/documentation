# What is Group Policy?

According to Microsoft, “Group Policy is an infrastructure that allows you to specify managed configurations for users and computers through Group Policy settings and Group Policy Preferences.” Simply put, Group Policy is a way for an administrator to control very specific attributes and rules for a collection of computers.

For example, let’s say a library has a lab for public use, but they want the computers inside to only use a restricted internet browser and disable the use of Command Prompt or PowerShell. These objectives can be achieved by using Group Policy.

# Managing Existing Policies

An administrator has many options for viewing or editing already implemented policies. The easiest to use is the Group Policy Management Console found in the ‘RSAT: Group Policy Management Tools’ download found under "Settings" > "Apps" > "Manage optional features" > "Add feature" on Windows 10 Enterprise.

The console has a variety of views, buttons, and options, but the most important information is found under the domain on the sidebar. The lines with folder icons are the names of the Organizational Units (OU) in the domain. The policies applied to a specific OU can be found next to the parchment icons located underneath the respective OU folder. Note that any sub-OU automatically inherits the GPOs linked to its parent OU.

All policies, or Group Policy Objects (GPO), are stored under the ‘Group Policy Objects’ folder (also found under the domain). This folder has all of the GPOs in the domain. These GPOs can then be applied, or “linked,” to any desired OU.

When a GPO is linked to an OU, its policies are applied to all computers contained within. For example, if a GPO setting the default wallpaper is applied to an OU then all the computers inside will have the new wallpaper. Any computer that is added to the OU will inherit all of its parent’s GPOs as well.

A lot of thought should go into the creation or linking of a new GPO. Consider testing a GPO on a small collection of computers before deploying for general use.

## Linking an Existing GPO

This can be done in multiple ways, but the easiest is to right-click an OU folder and select ‘Link an Existing GPO.’

This will spawn a dialog titled ‘Select GPO’ that contains a list of GPOs found in the domain. Select a GPO and click ‘Ok.’ The GPO is now linked to the OU.

The next step is to ensure that the computers are updated to accept the newly linked GPO. Do this by right-clicking the OU folder and selecting ‘Group Policy Update….’ This will spawn a dialog asking to confirm that an update was requested. Clicking ‘Yes’ will run gpupdate on the computers contained in the OU. Note that computers will regularly check for policy compliance, but this step helps to speed up the process.

# Creating a New GPO

Again, this can be done several ways, but the easiest way is to right-click an OU folder and select ‘Create a GPO in this domain, and Link it here….’ Enter a name for the GPO in the ‘New GPO’ dialog. It is best for beginners to select ‘(none)’ as the source for the starter GPO. Clicking ‘OK’ will create a new GPO inside the ‘Group Policy Objects’ folder and then link it to the OU that was right-clicked. Currently the new GPO has no rules or attributes applied.

# Editing a GPO

First, locate the GPO in either the location it is linked or in the ‘Group Policy Objects’ folder. Right-click the GPO and click ‘Edit.’ This will spawn the ‘Group Policy Management Editor.’

Editing a GPO can be quite complicated and can have unintended side effects. Despite this, editing a GPO can become less daunting by becoming familiar with the policy options contained in both ‘Computer Configuration’ and ‘User Configuration’ sections as it is not always obvious where a desired rule might be. The first is for policies that apply to the computer as a whole (usually this includes all users) while the second is mostly for user-by-user bases.

Let’s return to the library example. Say the librarians want the public lab computers to have an option to logon as a premium user to use for paid library services instead of limiting the logon to the generic public user account. They still want the lab somewhat locked down, yet still have two users. They can achieve this by double-clicking on the ‘Allow log on locally’ policy (Computer Configuration > Policies > Windows Settings > Security Settings > Local Policies > User Rights Assignment) and adding the premium user to the allowed list. Now they only need to exit the editor and send a “Group Policy Update” (as mentioned earlier) for their new policy to be applied.

# A Few Helpful Tips

-   When a computer is moved from one OU (we’ll call it A) to another OU (B), it will be unlinked from the GPOs applied to A and linked to the GPOs applied to B. The unlinking process does not “undo” the changes made by the GPO. This means that the computer will need to be updated (manually or via another GPO) to remove or edit undesirable leftover changes. (This is called tattooing and does not apply to some policies added by newer versions of Windows
    
-   GPOs can interfere with one another, so be sure to change the link order if this happens (done by selecting a folder and clicking the arrows in the folder window).
    
-   As mentioned earlier, each GPO should be thoroughly tested on a small collection of computers before deploying for general use. (For local testing before sending to a test group, use the registry editor or local policy editor
    
-   Sometimes a GPO will not take effect until after the target computer has been restarted.
    
-   GPOs can become confusing unless all objects follow the enterprise's conventions. Some options are: combining all related settings in one object or breaking up objects so each one only defines one setting. This should also apply to GPOs that include user or computer configurations.
    
-   Remember processing order. GPOs are applied first to the local security policy; second, to the GPOs linked to the AD site; third, GPOs linked to the AD domain; fourth, GPOs applied to the OU structure from the top down. Errors in the ordering can affect how rules are applied
    
-   Use RSOP and gpresult for general debugging and troubleshooting
    
-   A powerful feature (and occasional source of trouble) is security filtering. This feature--found under the 'Scope' tab--restricts the GPO to certain groups, users, and computers within the OU it is linked to.