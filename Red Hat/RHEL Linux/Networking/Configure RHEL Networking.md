
RHEL 9 uses `nmcli` to manage its networking configuration

## General Commands

Display the IP settings of the NIC
`ip address show <NIC>`

Display the IPv4 default gateway:
`ip route show default`

Display the DNS settings:
`cat /etc/resolv.conf`

## NMCLI Commands
`
List the Network Manager connection profiles:
`nmcli connection show`

Create an additional connection profile
`nmcli connection add con-name <connection-name> ifname <device-name> type ethernet`

Rename the connection profile:
`nmcli connection modify "Wired connection 1" connection.id "Internal-LAN"`

Display the current settings of the connection profile:
`nmcli connection show Internal-LAN`

To customize other settings in the profile
`nmcli connection modify <connection-name> <setting> <value>`

### NMCLI IPV4 Commands

To use DHCP
`nmcli connection modify Internal-LAN ipv4.method auto`

To set a static IPv4 address, network mask, default gateway, DNS servers, and search domain
`nmcli connection modify <connection profile> ipv4.method manual ipv4.addresses xxxxx/24 ipv4.gateway xxxxx ipv4.dns xxxxx ipv4.dns-search example.com`

`nmcli connection modify ens 192 ifname ens192 ipv4.method manual ipv4.addresses xxxxx/22 ipv4.gateway 172.20.36.1`

Add routes
`nmcli con modify <connection profile> +ipv4.routes "<destination subnet> <route gateway>`

`nmcli con modify ens224 +ipv4.routes "10.60.60.0/24 172.20.36.1"`


## NMCLI Interactive Editor

**Start `nmcli` in interactive mode**
	To create an additional connection profile
	`nmcli connection edit type ethernet con-name "<connection-name>"`
	To modify an existing connection profile
	`nmcli connection edit con-name "<connection-name>"`


Rename the connection profile
`nmcli> set connection.id Internal-LAN`

Display the current settings of the connection profile:
`nmcli> print`

If you create a new connection profile, set the network interface:
`nmcli> set connection.interface-name "connection-name"`

To use DHCP:
`nmcli> set ipv4.method auto`

To set a static IPv4 address, network mask, default gateway, DNS servers, and search domain
``` bash
nmcli> ipv4.addresses 192.0.2.1/24
Do you also want to set 'ipv4.method' to 'manual'? [yes]: yes
nmcli> ipv4.gateway 192.0.2.254
nmcli> ipv4.dns 192.0.2.200
nmcli> ipv4.dns-search example.com
```

Save and activate the connection
`nmcli> save persistent`

Leave the interactive mode:
`nmcli> quit`
