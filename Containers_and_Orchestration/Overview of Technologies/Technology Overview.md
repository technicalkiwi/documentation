## What is Docker
- Primarily Docker is a container runtime,
This means docker is a piece of software that is designed to implement and support containers
- Docker allows you to run Containers on systems, as well as tools for creating and managing containers and images

 
## What is Kubernetes
- Kubernetes is a container orchestration tool
- It allows you to build and manage your container infrastructure and automation.
- Allows functionality like self-healing applications, automated scaling and easy automated deployments.


## Micro-services
- Microservices are a type of application architecture that involves splitting the application into a series of small independent services.
- Micro-services can be built, modified, and scaled separately, with little impact on each other.
- Allows for rapid development in the different areas of your application

## Cloud Transformation
- Cloud transformation is the process of migrating existing infrastructure to the cloud.
- Relatively easy to wrap existing software in containers.
- Using Containers in the cloud we can benefit from flexibility and automatability


## Automated Scaling 
- Automated Scaling refers to automatically de/provisioning resources in response to real-time data metrics, Without this you must provision enough resources to cover peak usage at all times
- With Scaling you can detect when usage has changed and start/stop additional servers as needed
- Automated Scaling relies on the ability to spin up a new instance quickly and efficiently
- This also allows for less downtime due to high loads and prevents paying for unnecessary resources.

 

## Continuous Deployment Pipelines
- The Practice of deploying new code automatically and frequently
- Rather than a large set of changes deployed all at once on an infrequent schedule
- Allows you to get new functionality in front of customers faster.
- Also has lower risk as changes are small and frequent rather than one massive set of changes infrequently
- Automation allows for these frequent deployments to maintain stability and consistency.

 

## Self-Healing Applications
- These are applications that are able to automatically detect problems and then take steps to rectify
- Also applies to things like rebooting a hung server, restarting a container.
- With containers the start-up is so quick that they can be destroyed and replaced efficiently