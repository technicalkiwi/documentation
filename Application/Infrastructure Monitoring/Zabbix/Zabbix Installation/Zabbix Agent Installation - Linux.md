## Add in Package Repository

Add in the package repository for your distribution

**CentOS / Redhat**

`1rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm 2yum clean all`

**Ubuntu**

`1wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb 2dpkg -i zabbix-release_5.0-1+focal_all.deb 3apt update`

## Install the Agent

Install the agent from the package repository

**CentOS / Redhat**

`yum install zabbix-agent2`

**Ubuntu**

`apt install zabbix-agent2`

## Configuring the Agent

Open config file for the Agent

`sudo nano /etc/zabbix/zabbix_agent2.conf`

Change the following lines
``` 
Server=$ZabbixServerIP 
ServerActive=$ZabbixServerIP 
Hostname=$ServerHostname
```
Save changes and exit

Restart Zabbix Agent

`sudo systemctl restart zabbix-agent2`