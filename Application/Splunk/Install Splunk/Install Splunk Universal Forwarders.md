### Install Splunk Universal Forwarder

Copy Splunk Forwarder Rpm to local server

Install rpm

This will install to /opt/splunkforwarder and create the user/group splunk

### Set User seed

Add `/opt/splunkforwarder/etc/system/local/user-seed.conf`

``` conf
# /opt/splunkforwarder/etc/system/local/user-seed.conf 
[user_info] USERNAME = admin 
HASHED_PASSWORD = 
```
### Set Deployment client

Production deployment Server: 10.50.50.197:8089

Non prod deployment server: 10.60.60.182:8089

``` conf
# deploymentclient.conf 
[target-broker:deploymentServer] 
targetUri = {{ deploymentserver }}
```

### Set Limts file

Create `/opt/splunkforwarder/etc/apps/SplunkUniversalForwarder/default/limits.conf` and set maxKBps to 0

``` conf
# /opt/splunkforwarder/etc/apps/SplunkUniversalForwarder/default/limits.conf
#   Version 7.2.5.1
[thruput]
maxKBps = 0
```

### Set ownership of splunk directory

`chown -R splunk:splunk /opt/splunkforwarder`

### Accept license and Initial Run

Run the command to start splunk and accept license

`/opt/splunkforwarder/bin/splunk start --answer-yes --no-prompt --accept-license`

Stop Splunk

`/opt/splunkforwarder/bin/splunk stop`

### Create Systemd Unit file

`/opt/splunkforwarder/bin/splunk enable boot-start -systemd-managed 1`

Comment out ExecStartPost lines

``` conf
#ExecStartPost=/bin/bash -c "chown -R splunk:splunk /sys/fs/cgroup/cpu/system.slice/%n"
#ExecStartPost=/bin/bash -c "chown -R splunk:splunk /sys/fs/cgroup/memory/system.slice/%n"
```

Example File

``` systemd
This unit file replaces the traditional start-up script for systemd
#configurations, and is used when enabling boot-start for Splunk on
#systemd-based Linux distributions.

[Unit]
Description=Systemd service file for Splunk, generated by 'splunk enable boot-start'
After=network.target

[Service]
Type=simple
Restart=always
ExecStart=/opt/splunkforwarder/bin/splunk _internal_launch_under_systemd
KillMode=mixed
KillSignal=SIGINT
TimeoutStopSec=360
LimitNOFILE=65536
SuccessExitStatus=51 52
RestartPreventExitStatus=51
RestartForceExitStatus=52
User=splunk
Group=splunk
Delegate=true
CPUShares=1024
MemoryLimit=8061063168
PermissionsStartOnly=true
#ExecStartPost=/bin/bash -c "chown -R splunk:splunk /sys/fs/cgroup/cpu/system.slice/%n"
#ExecStartPost=/bin/bash -c "chown -R splunk:splunk /sys/fs/cgroup/memory/system.slice/%n"

[Install]
WantedBy=multi-user.target
```

### Start and Enable Splunk Forwarder

`systemctl start SplunkForwarder`
`systemctl enable SplunkForwarder`
