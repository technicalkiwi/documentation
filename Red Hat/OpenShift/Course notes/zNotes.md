## Container Overview
Openshift is build ontop of a Optimized OS called coreOS
CoresOS comes pre-installed with [cri-o](https://cri-o.io/)runtime 
Openshift prefers OCI image format

![[Pasted image 20240520132744.png]]

![[Pasted image 20240520132802.png]]

![[Pasted image 20240520133047.png]]

## K8'sOverview

K8s is the container orchestration software, It works alongside the container engine to manger containers in a cluster of nodes

![[Pasted image 20240520133927.png]]


One pod cannot communicate directly with another pod, It will go through a "service"
![[Pasted image 20240520134206.png]]

# OpenShift Overview

OCP adds feature sets to K8's
* OCP can build as well and deploy images so can be intergrated into CI/CD Pipelines
* Integrated web ui
* Add routers to handle ingress traffic to the pods
* Extended metrics and logging - Intergrated Grafana
* 
