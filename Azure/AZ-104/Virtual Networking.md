Configure how the azure resources will connect to each other


##  Virtual networks (Vnet)

#### Create Virtual Network
Select or create a resource group
Next > Security tab
Leave all blank for now
Next > IP Addresses
Create Address Space ( IP Range ) ( Go big as this can be divided up later)
Create subnets ( More can be added later)

Add Tags if needed

Review and create



#### Create Public IP Address
Go to public IP's
Create new public ip address
Name the IP address
Configure Pub IP
Dnslabel if using Azure DNS


Associate the PUB IP with specific resources
* Virtual machine
* Load balancer
* VPN Gateway
* Application Gateway




## Network Security Groups

Limits network traffic to resources in vnet
Lists security rules that allow/ deny traffic
Associate with subnet's or NIC's
#### Create Network Security Groups (NSG)
Go to "NSG"
Create new nsg
Create In/outbound security rules


#### NSG RULES
Enable filtering of network traffic flowing in and our of vnet subnets and interfaces
Evaluated independently for subnet and NIC
So matching rules must exist at both levels


##### Create NSG rules
Needed information to create a nsg rule
Source
Destination:
Service
Priority
![[nsg_rule4.png]]

#### Application Security Groups
Extends your application structure
Logical grouping of virtual machines
Defines rules to control the traffic flow
Wraps the ASG with a NSG for added security






## Host Domain on Azure DNS
Create private DNS zone
Select domain name - You must own this

Add virtual network link


Verify Delegation of DNS
Add nameservers to Domain config





