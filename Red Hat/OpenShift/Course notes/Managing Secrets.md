## Creating Secrets

If a pod requires access to sensitive information, then create a secret for the information before you deploy the pod. Both the oc and kubectl command-line tools provide the `create secret` command. Use one of the following commands to create a secret

Create a generic secret that contains key-value pairs from literal values that are typed on the command line:
`oc create secret generic secret_name --from-literal key1=secret1 --from-literal key2=secret2`


## Configuration Maps
The syntax for creating a configuration map and for creating a secret closely match. You can enter
key-value pairs on the command line, or use the content of a file as the value of a specified key.
You can use either the oc or kubectl command-line tools to create a configuration map. The
following command shows how to create a configuration map:

# PG 292


