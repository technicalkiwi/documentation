
## Azure Backup
Backup service
![[backup1.png]]

Vaults and policies
![[backup2.png]]

### Implement Azure Backup Center
Single plane of glass to manage backups across the azure environment
Data-source centric management focused on backup targets
Connected experiences with native integrations



