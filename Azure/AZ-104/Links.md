[Browse Credentials | Microsoft Learn](https://learn.microsoft.com/en-us/credentials/browse/?credential_types=applied%20skills)


https://docs.microsoft.com/azure/virtual-network/tutorial-restrict-network-access-to-resources
https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Design%20and%20implement%20a%20virtual%20network%20in%20Azure


## Networking
https://learn.microsoft.com/training/modules/improve-app-scalability-resiliency-with-load-balancer/
https://learn.microsoft.com/training/modules/load-balance-web-traffic-with-application-gateway/

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Create%20and%20configure%20an%20Azure%20load%20balancer

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Restrict%20network%20access%20to%20PaaS%20resources%20with%20virtual%20network%20service%20endpoints

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Design%20and%20implement%20a%20virtual%20network%20in%20Azure

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Configure%20DNS%20settings%20in%20Azure

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Create%20and%20configure%20an%20Azure%20load%20balancer

https://mslabs.cloudguides.com/guides/AZ-700%20Lab%20Simulation%20-%20Deploy%20Azure%20Application%20Gateway


## Storage

https://docs.microsoft.com/rest/api/storageservices/delegate-access-with-shared-access-signature
https://docs.microsoft.com/azure/storage/common/storage-sas-overview
https://docs.microsoft.com/learn/modules/control-access-to-azure-storage-with-sas/


### Data Protection
[Quickstart - Back up a VM with the Azure portal by using Azure Backup - Azure Backup | Microsoft Learn](https://learn.microsoft.com/en-us/azure/backup/quick-backup-vm-portal)

### Monitoring
[Improve incident response with Azure Monitor alerts - Training | Microsoft Learn](https://learn.microsoft.com/en-us/training/modules/incident-response-with-alerting-on-azure/)



26547449669

udJfrpcP864

