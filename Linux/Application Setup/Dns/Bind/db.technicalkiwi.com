;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     tkiwi.localt. aaron.technicalkiwi.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      technicalkiwi.com.
@       IN      A       192.168.1.207
@       IN      AAAA    ::1
tk-lnx-svr01    IN      A       192.168.1.202
svr01           IN      A       192.168.1.202
tk-lnx-svr02    IN      A       192.168.1.206
svr02           IN      A       192.168.1.206
homarr          IN      A       192.168.1.206
kavita          IN      A       192.168.1.206
jellyfin        IN      A       192.168.1.206
jellyseer       IN      A       192.168.1.206
lidarr          IN      A       192.168.1.206
muxi            IN      A       192.168.1.206
panel           IN      A       192.168.1.204
plex            IN      A       192.168.1.206
portainer       IN      A       192.168.1.206
prowlarr        IN      A       192.168.1.206
qbit            IN      A       192.168.1.206
lidarr          IN      A       192.168.1.206
radarr          IN      A       192.168.1.206
readarr         IN      A       192.168.1.206
sonarr          IN      A       192.168.1.206
sonarr.anime    IN      A       192.168.1.206
sonarr-anime    IN      A       192.168.1.206
nas             IN      A       192.168.1.206
firefly         IN      A       192.168.1.206
tandoor         IN      A       192.168.1.206
monica          IN      A       192.168.1.206
lldap           IN      A       192.168.1.206

; External
vps             IN      A       216.127.190.108
vpn             IN      A       45.79.236.93