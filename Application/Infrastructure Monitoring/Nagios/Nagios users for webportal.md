### Creating a New User

Use the below command to create a new user for the nagios web portal

`1htpasswd /usr/local/nagios/etc/htpasswd.users {USERNAME}`

You can view all the current users by opening the nagios htpasswd.users file

`nano /usr/local/nagios/etc/htpasswd.users`

![](blob:https://technicalkiwi.atlassian.net/05e20816-8c41-4eea-ab29-2b43b8adcf85#media-blob-url=true&id=598cdcf9-b92a-48e4-852f-e1092d68bb95&collection=contentId-1116405765&contextId=1116405765&mimeType=image%2Fpng&name=Nagios_users1.png&size=7389&height=81&width=752&alt=)
![Nagios Users](./resources/nagios_users.png)

### Changing Access levels of a User

Changing permissions of the users is done in the Nagios Cgi.cfg file located here
**/usr/local/nagios/etc/cgi.cfg**  
A full list of the options is available here [CGI Configuration File Options (nagios.com)](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/configcgi.html "https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/configcgi.html")

Open the cgi.cfg file as a sudo user

`sudo nano /usr/local/nagios/etc/cgi.cfg`

Scroll down to the section you need, in this case its the GLOBAL HOST/SERVICE VIEW ACCESS  
Add in the user you want to grant this permission to
![Nagios](./resources/nagios_users1.png)
![](blob:https://technicalkiwi.atlassian.net/6bf2e269-8c72-4c0c-a682-ced2878dd009#media-blob-url=true&id=9c02e831-0a46-429c-bf56-ae1d4d9bc7dc&collection=contentId-1116405765&contextId=1116405765&mimeType=image%2Fpng&name=nagios_installation2.png&size=20043&height=294&width=430&alt=)

Save the changes

Reload nagios

`sudo systemctl reload nagios`