
``` bash
[root@pdc-int-rps1 abuchan]# insights-client --check-results
[root@pdc-int-rps1 abuchan]# systemctl status insights-client-results.service
○ insights-client-results.service - Check for insights from Red Hat Cloud Services
     Loaded: loaded (/usr/lib/systemd/system/insights-client-results.service; static)
     Active: inactive (dead) since Wed 2024-08-28 08:32:19 NZST; 8min ago
TriggeredBy: ● insights-client-results.path
       Docs: man:insights-client(8)
    Process: 2715685 ExecStart=/usr/bin/insights-client --check-results (code=exited, status=0/SUCCESS)
   Main PID: 2715685 (code=exited, status=0/SUCCESS)
        CPU: 2.170s

Aug 28 08:32:15 pdc-int-rps1 systemd[1]: Starting Check for insights from Red Hat Cloud Services...
Aug 28 08:32:19 pdc-int-rps1 systemd[1]: insights-client-results.service: Deactivated successfully.
Aug 28 08:32:19 pdc-int-rps1 systemd[1]: Finished Check for insights from Red Hat Cloud Services.
Aug 28 08:32:19 pdc-int-rps1 systemd[1]: insights-client-results.service: Consumed 2.170s CPU time.

```