#### System Routes

Directs network traffic between virtual machines, on premise networks and he internet
Vm - VM
VM subnet1 - Vm subnet2
VM- Internet
VM - VM via VNet-Vnet VPN
Site-Site and ExpressRoute communication through the VPn gateway


#### User Defined routes
A route table contains the ruels to specify how packets should be routed in a vnet
Custom routes that control traffic by defining the next "hop" for the packet


#### Create Routing table
Go to Route Table
Add route table
Configure Route table
Propagate routes if on-premise need access
Add Tag
Create

###### Create Routes
Go to route table
Got to settings > Routes
Create routes

Associate with a subnet



#### Service Endpoints
Endpoints limit network access to specific services

![[service_endpoint1.png]]

#### Private links
Private connectivity to services on Azure
Traffic stays on MS network
Integration with on premise / peered vnets
In event of breach only mapped resource would be available.

