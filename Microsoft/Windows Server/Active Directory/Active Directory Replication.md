

## Active Directory Replication Health Checks

### Check the replication health

`Repadmin /replsummary`

The “/replsummary” operation quickly summarizes replication state and relative health of a forest.

![](blob:https://mclarensnz.atlassian.net/13b1ede5-a3b5-4ed2-b529-366be275687a#media-blob-url=true&id=caca01dc-3bb2-43b1-806b-ac34acb94e71&collection=contentId-40665104&contextId=40665104&mimeType=image%2Fpng&name=image-20221121-024347.png&size=40913&width=615&height=272&alt=)

### Check the inbound replication requests that are queued.

`Repadmin /Queue`

This command lists elements that are remaining in the replication queue. It displays inbound replication requests that the Domain Controller needs to issue in order to become consistent with its source replication partners.

![](blob:https://mclarensnz.atlassian.net/4682f7a3-d7b0-44af-b331-2a503c7edf39#media-blob-url=true&id=909e521c-c4a5-4721-a593-456fe570064b&collection=contentId-40665104&contextId=40665104&mimeType=image%2Fpng&name=image-20221121-024407.png&size=22560&width=510&height=88&alt=)

### Check the replication status

`Repadmin /Showrepl`

This command displays the replication status when the specified domain controller last attempted to implement an inbound replication of Active Directory partitions. It helps in figuring out the replication topology and replication failure.

![](blob:https://mclarensnz.atlassian.net/4f5eaee1-51b7-4971-a2d8-1475864e9f3a#media-blob-url=true&id=557f9c99-aeaf-4832-91a4-99981797df8f&collection=contentId-40665104&contextId=40665104&mimeType=image%2Fpng&name=image-20221121-024425.png&size=144267&width=512&height=702&alt=)

### Synchronize replication between replication partners

`Repadmin /syncall`

It ensures synchronization between replication partners

### Force the KCC to recalculate the topology

`Repadmin /KCC`

This command forces the KCC (Knowledge Consistency Checker) on targeted domain controller(s) to immediately recalculate its inbound replication topology. It checks and creates the connections between the Domain Controllers. By default KCC runs in the background every 15 minutes to check if a new connection has been established between DCs.

### Force replication

`Repadmin /replicate`

This command forces the replication of the specified directory partition to the destination domain controller from the source DC.