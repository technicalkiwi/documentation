# Create a new pipleline

Within your repository create a file called `azure-pipelines.yml`



``` yaml

# Define a trigger in which to trigger the pipeline
# This is usually a commit to  a branch
trigger:
#- main
 - none

# Define the pool of runners in which to use
pool:
  vmImage: ubuntu-latest

# Set variables to be used within the pipeline
# These have the sytnax of
# name: define the name of the variable, this will also be what is used to reference
# value: Definfe the value of the variable
variables:
  - name: environment
    value: test
  - name: location
    value: australiaeast
  - name: subscriptionId
    value: 4958aba6-0284-xxxx-xxxx-xxxxxxx
  - name: serviceConnectionName
    value: AzTera
  - name: resource_group_name
    value: Test_Env
  - name: storage_account_name
    value: terrastore

# Define the steps to take within the runner
steps:
- script: echo Hello, world!
  displayName: 'Run a one-line script'

# Define the tasks to run
# These have their own syntax
- task: TerraformInstaller@0
  displayName: install terraform
  inputs:
    terraformVersion: latest

- task: AzureCLI@2
  displayName: 'Create resource group $(resource_group_name)'
  condition: true
  inputs:
    azureSubscription: $(serviceConnectionName)
    scriptType: 'bash'
    scriptLocation: 'inlineScript'
    inlineScript: |
      az group create -n $(resource_group_name) -l $(location)


- task: AzureCLI@2
  displayName: 'Create storage account $(storage_account_name) for terraform state files'
  condition: true
  inputs:
    azureSubscription: $(serviceConnectionName)
    scriptType: 'bash'
    scriptLocation: 'inlineScript'
    inlineScript: |
      az storage account create -n $(storage_account_name) -g $(resource_group_name) -l $(location) --sku Standard_LRS


- task: TerraformTaskV2@2
  displayName: 'Terraform init'
  inputs:
    command: 'init'
    provider: 'azurerm'
    workingDirectory: '$(System.DefaultWorkingDirectory)'
    backendServiceArm: '$(serviceConnectionName)'
    backendAzureRmResourceGroupName: '$(resource_group_name)'
    backendAzureRmResourceGroupLocation: '$(location)'
    backendAzureRmStorageAccountName: '$(storage_account_name)'
    backendAzureRmContainerName: 'mnztfcontainer'
    backendAzureRmKey: 'terraform.tfstate'
    commandOptions: '-lock=false'

- task: TerraformTaskV3@0
  displayName: 'Terraform plan'
  inputs:
    command: 'plan'
    workingDirectory: '$(System.DefaultWorkingDirectory)'
    environmentServiceNameAzureRM: '$(serviceConnectionName)'
    commandOptions: '-var "environment=$(environment)" -var "resource_group_name=$(resource_group_name)" -var "location=$(location)" -input=false'


- task: TerraformTaskV3@0
  displayName: 'Terraform apply'
  condition: true  # True to run, False to skip
  inputs:
    command: 'apply'
    workingDirectory: '$(System.DefaultWorkingDirectory)'
    environmentServiceNameAzureRM: '$(serviceConnectionName)' 
    commandOptions: '-var "environment=$(environment)" -var "resource_group_name=$(resource_group_name)" -var "location=$(location)"  -input=false'


- task: TerraformTaskV4@4
  displayName: 'Terraform 'output'
  condition: true # True to run, False to skip 
  inputs:
#    provider: 'azurerm'
    command: 'custom'
    customCommand: 'output'
    outputTo: 'file'
    fileName: 'output.json'
    environmentServiceNameAzureRM: '$(serviceConnectionName)'


- task: TerraformTaskV3@0
  displayName: 'Terraform destroy'
  condition: false # set to false to disable destroy
  inputs:
    command: 'destroy'
    workingDirectory: '$(System.DefaultWorkingDirectory)'
    environmentServiceNameAzureRM: '$(serviceConnectionName)' 
    commandOptions: '-var "environment=$(environment) -var "resource_group_name=$(resource_group_name)" -var "location=$(location)"'

```

