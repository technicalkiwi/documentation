# Mount a Samba Share

Install Cifs Utils

`sudo apt install cifs-utils`

Create a location to mount the share

`mkdir /nas`

Mount the Share

`sudo mount -t cifs -o username=user_name //server_name/share_name /mnt/data`

E.g

`sudo mount -t cifs username=tkiwi,password=USERPASS //192.168.1.205/Media /nas`

## Fstab entry to mount smb
The fstab entries make sure that your mount is persistent over reboot.

Open fstab

`nano /etc/fstab`

Add in a line with your connection
`//SERVER/SHARE /MOUNTPATH smbfs username=USER,password=USERPASS,iocharset=utf8,file_mode=0777,dir_mode=0777`
E.g
`//192.168.1.205/Media /nas/ cifs username=tkiwi,password=USERPASS,iocharset=utf8,file_mode=0777,dir_mode=0777`

### Use a credentials file
The /etc/fstab is readable by everyone so its best practice to aviod having a plain text password.
The way to get around this is, is to use a credentials file.

Use the Credentials modifier to specifyt a location for the file.
`//servername/sharename /mountdirectory smbfs credentials=/home/myhomedirectory/.smbpasswd 0 0`
E.g
`//192.168.1.205/Media /nas/ cifs credentials=/home/tkiwi/.smbpasswd,iocharset=utf8,file_mode=0777,dir_mode=0777`
Below echo command can be used to create the credentials file:

`sudo echo username=mywindowsusername > .smbpasswd`
`sudo echo password=mywindowspassword >> .smbpasswd`

Modify the permissions on the file so only you have permission to read and write to it.

`sudo chmod 600 .smbpasswd`