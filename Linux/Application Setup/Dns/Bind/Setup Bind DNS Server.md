# Setup 

## Install Bind

`sudo apt install -y bind9 bind9utils bind9-doc dnsutils`

## Configure Bind DNS Server

The main configuration file is named.conf.options
Open the file 
`sudo nano /etc/bind/named.conf.options`

### Listen on 
The "listen-on" directive allows you to specify the networks that the DNS server will serve. 
Don't write this or write "any;" to work for all addresses.

``` bash
listen-on {
10.10.10.0/24;
10.1.0.0/16;
...
};
```

### Allow Query
BIND9 only allows local queries by default. Add the necessary IP addresses to the "allow-query" directive or "any;" to allow all requests.

``` bash
allow-query { any; };
```

### Forwarders
Forwarders contain the IP addresses of DNS servers to which the request is redirected if our server does not contain the required data.

``` bash
forwarders {
8.8.8.8;
8.8.4.4;
};
```

Save and close the file. Check the configuration: 
`sudo named-checkconf`

If no errors appear, then everything is in order. Restart the service for the changes to take effect. 
`sudo systemctl restart bind9`

## Test Bind DNS Server

To check if the DNS server is working properly, enter the following command on any other remote computer. Replace dns-server-ip-address with the IP address of the DNS server.

`nslookup ubuntu.com dns-server-ip-address`

Output:
``` bash

Server: dns-server-ip-address
Address: dns-server-ip-address#53
Non-authoritative answer:
Name: ubuntu.com
Address: 91.189.88.181
...
```