- [[#Openshift Overview|Openshift Overview]]
- [[#Containers Overview|Containers Overview]]
- [[#Kubernetes Overview|Kubernetes Overview]]
	- [[#Kubernetes Overview#K8s Features|K8s Features]]
		- [[#K8s Features#Service discovery and load balancing|Service discovery and load balancing]]
		- [[#K8s Features#Horizontal scaling|Horizontal scaling]]
		- [[#K8s Features#Self-healing|Self-healing]]
		- [[#K8s Features#Automated rollout|Automated rollout]]
		- [[#K8s Features#Secrets and configuration management|Secrets and configuration management]]
		- [[#K8s Features#Operators|Operators]]


## Containers Overview

A container is a process that runs an application independently from other containers on the host. Containers are created from a container image, which includes all the runtime dependencies of the application. Containers can then be executed on any host, without requiring the installation of any dependency on the host, and without conflicts between the dependencies of different containers.

Containers use Linux kernel features, such as namespaces and control groups (cgroups). For example, containers use cgroups for resource management, such as CPU time allocation and system memory. Namespaces isolate a container's processes and resources from other containers and the host system
![[Pasted image 20240520122321.png]]

![[Pasted image 20240520123659.png]]
## Kubernetes Overview
Kubernetes is a platform that simplifies the deployment, management, and scaling of
 containerized applications.
 
  Kubernetes creates a cluster that runs applications across multiple nodes. If a node fails, then
 Kubernetes can restart an application in a different node.
 
 Kubernetes uses a declarative configuration model. Kubernetes administrators write a definition of
 the workloads to execute in the cluster, and Kubernetes ensures that the running workloads match
 the definition. 
 
 Kubernetes defines workloads in terms of pods. A pod is one or multiple containers that run in
 the same cluster node. Pods with multiple containers are useful in certain situations, when two
 containers must run in the same cluster node to share some resource. 

### K8s Features

#### Service discovery and load balancing
 Distributing applications over multiple nodes can complicate communication between
 applications.
 Kubernetes automatically configures networking and provides a DNS service for pods. 
 Multiple pods can back a service for performance and reliability. 

####  Horizontal scaling
 Kubernetes can monitor the load on a service, and create or delete pods to adapt to the load.
 With some configurations, Kubernetes can also provision nodes dynamically to accommodate
 cluster load.

####  Self-healing
 If applications declare health check procedures, then Kubernetes can monitor, restart, and
 reschedule failing or unavailable applications. Self-healing protects applications both from
 internal failure (the application stops unexpectedly) or external failure (the node that runs the application becomes unavailable)

####  Automated rollout
 Kubernetes can gradually roll out updates to your application's containers. 
 If something goes wrong during the rollout, then Kubernetes can roll back to the previous version of the deployment. 
 Kubernetes routes requests to the rolled out version of the application, and deletes pods from the previous version when the rollout completes.

####  Secrets and configuration management
 You can manage the configuration settings and secrets of your applications without requiring
 changes to containers. 
 Application secrets can be usernames, passwords, and service endpoints, or any configuration setting that must be kept private
 **NOTE: Kubernetes does not encrypt secrets.**

####  Operators
 Operators are packaged Kubernetes applications that can manage Kubernetes workloads
 in a declarative manner. 



Kubernetes provides many features to build clusters of servers that run containerized applications. However, Kubernetes does not intend to provide a complete solution, but rather provides extension points so system administrators can complete Kubernetes. OpenShift builds on the extension points of Kubernetes to provide a complete platform
