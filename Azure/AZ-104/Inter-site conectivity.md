
vnet peering

2 Types of peering
* Global - Connections between regions  AU East - US East
* Regional - Connections within a region.  Au East - Au West

Connect 2 vnets.  Can peer across subscriptions an tenants
Peered networks use the AZURE backbone for privacy and isolation
Easy to setup, seamless data transfer



Gateway transit allows peered vnets to share a gateway and get access to a resources
NO VPN gateway is required in a peered spoke network
Default vnet peering provides full connectivity


#### Create Vnet Peering
Go to an existing Vnet
Go to peering's
Add a peering

Configure Information for peering


#### Service Chaining
Leverage user defined routes and service chaining to implement custom routing
Implement a Vnet Hub with network virtual appliance or VPN gateway
Enables directing traffic from one vnet to a vapp or vnet gateway in a peered vnet through user-defined routes

