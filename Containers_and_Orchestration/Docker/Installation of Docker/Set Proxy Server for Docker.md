# Setting Proxy for Docker Containers
The Docker services will not use environment variables to get proxy information. Instead, you will have to configure the service to use a proxy. This tutorial will show you how to set the proxy for Docker on Ubuntu servers.

Services in Ubuntu 16.04 and above are registered with SystemD. Docker is one of the few packages that determine proxy information from a service configuration in SystemD, rather than from an environment variable.

## Systemd Docker Service
In order to the set the proxy for Docker, you will need to create a configuration file for the Docker service. No configuration files exist by default, so one will have to be created.

All Systemd service configuration are stored under /etc/systemd/system. In order to keep things organized, we can create a child directory docker.service.d, rather than creating a file called /etc/systemd/system/docker.service.

Within this configuration file, we can set our HTTP and HTTPS proxy. We can also set our NO_PROXY value, too.

## Creating Proxy Configuration
Create a new directory for our Docker service configurations.

`sudo mkdir -p /etc/systemd/system/docker.service.d`

Create a file called proxy.conf in our configuration directory.

`sudo vi /etc/systemd/system/docker.service.d/proxy.conf`

Add the following contents, changing the values to match your environment.

<pre>
[Service]
Environment="HTTP_PROXY=http://myproxy.hostname:8080"
Environment="HTTPS_PROXY=https://myproxy.hostname:8080/"
Environment="NO_PROXY="localhost,127.0.0.1,::1"
</pre>
Save your changes and exit the text editor.

Reload the daemon configuration.

`sudo systemctl daemon-reload`

Restart Docker to apply our changes.

`sudo systemctl restart docker.service`

After the service is restarted Docker should be able to pull images from external repositories. You can test this by attempting to pull down an image. If the download completes and does not timeout, your proxy settings have been applied.