# Set Up Unbound DNS Resolver

### Install Components
Install some basic DNS tools
`sudo apt update`
`sudo apt install bind9-utils dnsutils net-tools -y`

Install Unbound DNS
`apt-get install unbound -y`

### Configure Unbound DNS
The Unbound main configuration file is located at /etc/unbound/unbound.conf
However, it is recommended to create a separate configuration file:

Create a new unbound.conf file
`nano /etc/unbound/unbound.conf.d/myunbound.conf`

Add in the below
``` bash
server:
port: 53
verbosity: 0
num-threads: 2
outgoing-range: 512
num-queries-per-thread: 1024
msg-cache-size: 32m
interface: 0.0.0.0
rrset-cache-size: 64m
cache-max-ttl: 86400
infra-host-ttl: 60
infra-lame-ttl: 120
access-control: 127.0.0.0/8 allow
access-control: 0.0.0.0/0 allow
username: unbound
directory: "/etc/unbound"
logfile: "/var/log/unbound.log"
use-syslog: no
hide-version: yes
so-rcvbuf: 4m
so-sndbuf: 4m
do-ip4: yes
do-ip6: no
do-udp: yes
do-tcp: yes
remote-control:
control-enable: yes
control-port: 953
control-interface: 0.0.0.0
```

Save and close the file, then validate the configuration file with the following command:

`unbound-checkconf /etc/unbound/unbound.conf.d/myunbound.conf`

You should get the following output:
`unbound-checkconf: no errors in /etc/unbound/unbound.conf.d/myunbound.conf`

Next, create a log file for Unbound and set proper permissions:

`touch /var/log/unbound.log`
`chown unbound:unbound /var/log/unbound.log`

### Start the Unbound DNS Service
Restart the Unbound service and enable it to start at system reboot:
`systemctl restart unbound`
`systemctl enable unbound`

Verify the status of Unbound with the following command:
`systemctl status unbound`

At this point, the Unbound service is started and listening on port 53. You can check it using the following command:
`ss -antpl | grep 53`

Sample
``` bash
LISTEN    0         256                0.0.0.0:53               0.0.0.0:*        users:(("unbound",pid=3407,fd=6))                                              
LISTEN    0         256                0.0.0.0:53               0.0.0.0:*        users:(("unbound",pid=3407,fd=4))                                              
LISTEN    0         4096         127.0.0.53%lo:53               0.0.0.0:*        users:(("systemd-resolve",pid=356,fd=13))                                      
LISTEN    0         256                0.0.0.0:953              0.0.0.0:*        users:(("unbound",pid=3407,fd=7))    
```

### Test Unbound DNS
Use the dig command and perform some DNS queries to test the Unbound DNS server.
We will use ubuntu.com for testing.

`dig ubuntu.com @localhost`

Example
``` bash
; <<>> DiG 9.16.1-Ubuntu <<>> ubuntu.com @localhost
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 6037
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ubuntu.com.			IN	A

;; ANSWER SECTION:
ubuntu.com.		60	IN	A	91.189.88.181
ubuntu.com.		60	IN	A	91.189.88.180

;; Query time: 307 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Sun Aug 15 06:32:18 UTC 2021
;; MSG SIZE  rcvd: 71
```

As you can see the query time is 307 msec in the first query
Your query is now cached.

Run the same query again:
`dig ubuntu.com @localhost`

Example
``` bash
; <<>> DiG 9.16.1-Ubuntu <<>> ubuntu.com @localhost
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37832
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ubuntu.com.			IN	A

;; ANSWER SECTION:
ubuntu.com.		49	IN	A	91.189.88.180
ubuntu.com.		49	IN	A	91.189.88.181

;; Query time: 0 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Sun Aug 15 06:33:36 UTC 2021
;; MSG SIZE  rcvd: 71
```
The query time is 0 msec.

### Testing from Client Machine
You can also test the Unbound DNS server from the client machine. In this case, you will need to specify your Unbound DNS server IP with the query:

dig ubuntu.com @unbound_server_ip
``` bash
; <<>> DiG 9.9.5-3ubuntu0.4-Ubuntu <<>> ubuntu.com @69.87.221.220
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 28051
;; flags: qr rd ad; QUERY: 0, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available
;; Query time: 365 msec
;; SERVER: 69.87.221.220#53(69.87.221.220)
;; WHEN: Sun Aug 15 12:04:37 IST 2021
;; MSG SIZE  rcvd: 12
```


