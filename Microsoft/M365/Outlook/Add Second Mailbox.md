# Adding a Second Mailbox

1.  Open Outlook
2.  Click on "File" tab in the top left
3.  Account Settings  > Account Settings (see image below)

![image-20200729141315530](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141315530.png)

 

2.  Click on "New" to create a new email account

![image-20200729141329972](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141329972.png)

3. Type the shared mailbox address into the "E-mail Address" field 
   Click on advanced options
   Tick the "Let me setup my account manually" checkbox

![image-20200729141413843](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141413843.png)

4. Select Exchange 2013 or earlier
   ![image-20200729141551924](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141551924.png)

5. Leave Settings as they are
   ![image-20200729141616232](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141616232.png)

6. Select Done
   ![image-20200729141701798](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141701798.png)

7. Restart Outlook

8. When the credentials box pops up asking for your username and password, Select more choices,
   Login using the Mailbox Credentials
   This will authenticate your credentials and verify if you have access to this shared mailbox. 
   ![image-20200729141952103](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729141952103.png)

   

9. The Mailbox will now be available for use
   ![image-20200729142104491](C:\Users\bgraaronb\AppData\Roaming\Typora\typora-user-images\image-20200729142104491.png)



