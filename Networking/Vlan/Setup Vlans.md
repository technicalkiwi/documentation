# Setting up Vlans
- [[#How to set up a VLAN|How to set up a VLAN]]
	- [[#How to set up a VLAN#1. Configure the VLAN|1. Configure the VLAN]]
	- [[#How to set up a VLAN#2. Set up access control lists|2. Set up access control lists]]
	- [[#How to set up a VLAN#3. Apply command-line interfaces|3. Apply command-line interfaces]]
	- [[#How to set up a VLAN#4. Consider management packages|4. Consider management packages]]
- [[#Identifying VLANs with tagging|Identifying VLANs with tagging]]
	- [[#Identifying VLANs with tagging#Tag protocol identifier|Tag protocol identifier]]
	- [[#Identifying VLANs with tagging#Tag control information|Tag control information]]
	- [[#Identifying VLANs with tagging#Ethernet-based VLANs|Ethernet-based VLANs]]

### How to set up a VLAN

Setting up a VLAN can be complicated and time-consuming, but VLANs offer significant advantages to enterprise networks, such as [improved security](https://www.techtarget.com/searchsecurity/tip/How-to-configure-a-VLAN-to-achieve-the-benefits-of-VLAN-security). The steps to set up a VLAN are as follows.

#### 1. Configure the VLAN

All the switches that carry the VLAN must be configured for that VLAN. Whenever teams make changes to the network configuration, they must take care to update switch configurations, swap out a switch or add an additional VLAN.

#### 2. Set up access control lists

[ACLs](https://www.techtarget.com/searchsoftwarequality/definition/access-control-list), which regulate the access granted to each user connected to a network, also apply to VLANs. Specifically, VLAN ACLs (VACLs) control the access to a VLAN wherever it spans a switch or where packets enter or exit the VLAN. Network teams should pay close attention when configuring VACLs, as setting them up is often complicated. Network security can be compromised if an error occurs when VACLs are configured or modified.

#### 3. Apply command-line interfaces

Each OS and switch vendor specifies a set of CLI commands to configure and modify VLANs on its product. Some administrators construct files containing these commands and edit them when necessary to modify the configuration. A single error in the command files can cause one or more applications to fail.

#### 4. Consider management packages

Equipment vendors and third parties offer management software packages that automate and simplify the effort, reducing the chance of error. Because these packages often maintain a complete record of each set of configuration settings, they can quickly reinstall the last working configuration in the case of an error.

### Identifying VLANs with tagging

The IEEE 802.1Q standard defines how to identify VLANs.

The destination and source media access control addresses appear at the beginning of Ethernet packets, and the 32-bit VLAN identifier field follows.

#### Tag protocol identifier

The TPID makes up the first 16 bits of the VLAN frame. The TPID contains the [hexadecimal](https://www.techtarget.com/whatis/definition/hexadecimal) value 8100, which identifies the packet as a VLAN packet. Packets without VLAN data contain the EtherType field in the packet position.

#### Tag control information

The 16-bit TCI field follows the TPID. It contains the 3-bit priority code point (PCP) field, the single-bit drop eligible indicator (DEI) and the 12-bit VLAN identifier (VID) field.

The **PCP field** specifies the [quality of service](https://www.techtarget.com/searchunifiedcommunications/definition/QoS-Quality-of-Service) required by the applications sharing the VLAN. The IEEE 802.1p standard defines these levels as the following values:

-   Value 0 indicates a packet that the network makes its best effort to deliver.
-   Value 1 identifies a background packet.
-   Value 2 through value 6 identify other packets, including values indicating video or voice packets.
-   Value 7, the highest priority, is reserved for network control packets.

The **single-bit DEI** follows the PCP field, and it adds further information to the PCP field. The DEI field identifies a packet that can be dropped in a congested network.

The **VID field** consists of 12 bits that identify any of the 4,096 VLANs a network can support.

#### Ethernet-based VLANs

Ethernet-based VLANs can be extended to Wi-Fi using a VLAN-aware access point (AP). These APs separate incoming wired traffic using the subnet address associated with each VLAN. End nodes receive only the data on the configured subnet.

Security over the air cannot be enforced as it can on a wire. Service set identifiers, or [SSIDs](https://www.techtarget.com/searchmobilecomputing/definition/service-set-identifier), with different passwords are used where necessary, such as in guest networks.

VLANs were developed early in the evolution of networking technology to limit broadcasts and prioritize traffic. They have proven to be useful as networks have increased in size and complexity.