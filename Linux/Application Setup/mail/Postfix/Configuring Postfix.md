
## Installing Postfix

Install postfix with 
`sudo apt install postfix `

### Intial Setup

**General type of mail configuration:** Internet Site since this matches our infrastructure needs.
**System mail name:** This is the base domain used to construct a valid email address when only the account portion of the address is given. 
**Root and postmaster mail recipient:** This is the Linux account that will be forwarded mail addressed to root@ and postmaster@. Use your primary account for this. 
**Other destinations to accept mail for:** This defines the mail destinations that this Postfix instance will accept. If you need to add any other domains that this server will be responsible for receiving, add those here. Otherwise, the default will be sufficient.
**Force synchronous updates on mail queue:** Since you are likely using a journaled filesystem, accept No here.
**Local networks:** This is a list of the networks for which your mail server is configured to relay messages. The default will work for most scenarios. If you choose to modify it, though, make sure to be very restrictive in regards to the network range.
**Mailbox size limit:** This can be used to limit the size of messages. Setting it to 0 disables any size restriction.
**Local address extension character:** This is the character that can be used to separate the regular portion of the address from an extension (used to create dynamic aliases). The default, + will work for most situations
**Internet protocols to use:** Choose whether to restrict the IP version that Postfix supports.

E.g
<pre>
General type of mail configuration?: Internet Site
System mail name: technicalkiwi.com
Root and postmaster mail recipient: tkiwi
Other destinations to accept mail for: $myhostname, technicalkiwi.com, mail.technicalkiwi.com, localhost.technicalkiwi.com, localhost
Force synchronous updates on mail queue?: No
Local networks: 127.0.0.0/8 10.10.1.0/24
Mailbox size limit: 0
Local address extension character: +
Internet protocols to use: all
</pre>


Install Mailutils with
`sudo apt install mailutils`

## Configuring Postfix








### Rejecting Domains

Open the postfix config file and add in the following line
`nano /etc/postfix/main.cf`

<pre>smtpd_recipient_restrictions = check_recipient_access hash:/etc/postfix/blacklisted_domains, permit_mynetworks, reject_unauth_destination, permit </pre>

Create the file blacklisted_domains within the postfix config folder
`nano /etc/postfix/blacklisted_domains`

Add in the domains or addresses to reject 
Format is domain/address *Tab* REJECT

e.g
<pre>
bad.domain1    REJECT
bad.domain2    REJECT
baduser@bad.domain3    REJECT
</pre>

Once you have added in the domains to reject, add the file to postfix with
`postmap /etc/postfix/blacklisted_domains`

**Make sure postfix has read access to the file or this will fail**

