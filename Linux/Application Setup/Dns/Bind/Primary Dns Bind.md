# Setup Primary DNS Zone
- [[#Configure DNS Zone|Configure DNS Zone]]
- [[#Zone file configuration|Zone file configuration]]


### Configure DNS Zone

Let’s add zone information to the configuration.

`sudo nano /etc/bind/named.conf.local`

Add the following to it

``` bash
zone "tkiwi.local" {
        type master;
        file "/etc/bind/db.tkiwi.local";
        allow-transfer { 10.10.10.1; };
        also-notify { 10.10.10.1; };
};
```

- **type** may be master, slave, forward, hint;
- **file** - indicates the path to the new zone file;
- **allow-transfer** - list of DNS servers that are allowed to transfer the zone;
- **also-notify** - the primary DNS server will notify these servers of zone changes.

Restart the service.

`systemctl reload bind9`

### Zone file configuration
Create a zone file from the template and open it.

`sudo cp /etc/bind/db.local /etc/bind/db.tkiwi.local`

`sudo nano /etc/bind/db.tkiwi.local`

Replace localhost in the SOA record with the FQDN of your server with the "." character at the end. In the example, this is "ns.domain-name.com.". 

Replace "root.localhost" with your valid admin email address with "." instead of "@" in it and "." at the end.

Serial - serial number of the change. You have to manually increment it every time you change the zone file. The secondary server monitors changes in the zone using this parameter.


``` bash
;
;
;
$TTL    604800
@       IN      SOA     ns.domain-name.com. admin.domain-name.com. (
                              2        ; Serial
                         604800        ; Refresh
                          86400        ; Retry
                        2419200        ; Expire
                         604800 )      ; Negative Cache TTL
;
@       IN      NS      ns.domain-name.com.
@       IN      A       10.1.1.1
ns      IN      A       10.1.1.9
ns2     IN      A       10.1.1.10
mx      IN      A       10.1.1.15
```

The bottom of the file contains DNS records.
 The format of the record: hostname *tab* class *tab* DNS record type *tab* value. 

Where:

- **hostname** - most often this value is a third-level domain name, and “domain-name.com” is filled in automatically. @ or none means an entry for the zone name (in this case, domain-name.com). You can also specify the FQDN with a dot at the end (for example, ns.domain-name.com.);
- **class** is IN (Internet), indicates the type of network;
- The most common types of DNS records: A, NS, MX, CNAME, TXT. "A" contains the IP address of the domain name, "NS" is the IP address of the zone's DNS server, "MX" - the mail server, "CNAME" - alias referring to the value of the specified record, "TXT" - custom entry;
- **value** - IP address, host name, text information.

Example
``` bash
;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     tkiwi.local. aaron.technicalkiwi.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      tkiwi.local.
@       IN      A       10.10.10.1
@       IN      AAAA    ::1
tk-lnx-svr01    IN      A       10.10.10.3
tk-lnx-svr02    IN      A       10.10.10.7
svr01           IN      A       10.10.10.3
svr02           IN      A       10.10.10.7
jellyfin        IN      A       10.10.10.3
lidarr          IN      A       10.10.10.3
muxi            IN      A       10.10.10.3
plex            IN      A       10.10.10.3
portainer-svr01 IN      A       10.10.10.3
prowlarr        IN      A       10.10.10.3
qbit            IN      A       10.10.10.3
radarr          IN      A       10.10.10.3
readarr         IN      A       10.10.10.3
sonarr          IN      A       10.10.10.3
sonarr.anime    IN      A       10.10.10.3
traefik         IN       CNAME     tk-lnx-svr01
```

Restart the rndc.

`sudo rndc reload`

You can check the DNS server. Enter this command from any remote computer.

`nslookup radarr.tkiwi.local 10.10.10.1`

Your domain's DNS A-record will be used as the response. In the given example, this is 10.10.10.3

``` bash
Server:         10.10.10.1
Address:        10.10.10.1#53

Name:   radarr.tkiwi.local
Address: 10.10.10.3
```
