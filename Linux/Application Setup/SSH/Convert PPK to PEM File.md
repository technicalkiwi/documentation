# Convert PPK to PEM
## Using Command Line Tools
Install the putty tools, if you don`t have on Linux:

`sudo apt-get install putty-tools`

Generate the pem file run the following command:

`puttygen keyname.ppk -O private-openssh -o keyname.pem`

Place the pemkey.pem file in your ~/.ssh directory:

`cp keyname.pem ~/.ssh`

Set the pem file to have the proper permissions:

`chmod 400 keyname.pem`
 
## Using Putty Gen
Open PuttyGen

Click Load

Load your private key

Go to Conversions->Export OpenSSH and export your private key

![Puuty Convert](../resources/putty_convert1.png)
 
Save the key

![Putty Converts](../resources/putty_convert2.png) 

Copy your private key to ~/.ssh/id_dsa (or id_rsa).

## Create Pub Key from new Pem key.
Create the RFC 4716 version of the public key using ssh-keygen

`ssh-keygen -e -f ~/.ssh/id_dsa > ~/.ssh/id_dsa_com.pub`

Convert the RFC 4716 version of the public key to the OpenSSH format:

`ssh-keygen -i -f ~/.ssh/id_dsa_com.pub > ~/.ssh/id_dsa.pub`