### Assign the required licenses
*To manage licenses, the account must be a license administrator, user administrator, or global administrator*  

Sign in to the [Azure AD admin center](https://aad.portal.azure.com/)   
Select Azure Active Directory  
Select Licenses  
[![License_by_SecurityGroup1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup1.png)  

Select All Products  
Select the License you want to assign  
Select Assign  
[![License_by_SecurityGroup2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup2.png)  

Select Users and Groups  
Search for and select the group you want to Assign the license to
[![License_by_SecurityGroup3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup3.png)

Select Assignment Options  
Alter Service plans to fit needs  
[![License_by_SecurityGroup4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup4.png)

Check on the Overview  
Assign Licenses  
[![License_by_SecurityGroup5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup5.png)

Azure will begin to process license assignment for the group  
[![License_by_SecurityGroup6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup6.png)

### Checking Assignment

Go to Azure Active Directory  
Go to Groups  
[![License_by_SecurityGroup7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup7.png)  

Select the Group the Licenses where assigned to  
[![License_by_SecurityGroup8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup8.png)  

Select Licenses  
Check on the Status of license assignment  
[![License_by_SecurityGroup9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/License_by_SecurityGroup9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/License_by_SecurityGroup9.png)