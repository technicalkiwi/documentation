The name of the program itself gives you an idea about its use case is. We tell "expect" what to look for while executing another interactive program. We also tell what the response should be.

Expect uses majority of the features provided by TCL language. TCL supports many different types of programming methods. It's simple and powerful.

### Install expect

install the expect command with the following  
`apt-get install expect`

### Expect Commands

Before we talk about expect command, Let’s see some of the Expect command which used for interaction:

`spawn` :Starts a script or a program.
`expect`  : Waits for program output.
`send` :Sends a reply to your program.

interact :Allows you to interact with your program.

-   The spawn command starts a script or a program like the shell, FTP, Telnet, SSH, SCP, and so on.
-   The send command sends a reply to a script or a program.
-   The Expect command waits for input.
-   The interact command allows you to define a predefined user interaction.

### Syntax

#### Example
``` bash
#!/user/bin/expect
spawn ./fw.sh
expect "show inventory"
send -- "\r"
send -- "exit\r"
expect eof
```
-   Set the parser to be expect using the shebang
-   Spawn will run the script defined  
-   expect parses the output of the script searching for the defined trigger 
-   once triggered it will run the next set of commands defined
-   sends the “\r” command to the shell ( return key) 
-   sends “exit\r” command to the shell ( the word exit, then the return key) 
-   `expect eof` indicates that the script ends here

### Embedding inside a bash Script

`expect`, expects its commands to be provided in a file, or as the argument of a `-c`, per the [man page](https://linux.die.net/man/1/expect "https://linux.die.net/man/1/expect"). Three options are below.

1.  [Process substitution](http://wiki.bash-hackers.org/syntax/expansion/proc_subst "http://wiki.bash-hackers.org/syntax/expansion/proc_subst") with here-document:
    
``` bash
expect <(cat <<'EOD'
spawn ... (your script here)
EOD
)
```

   
The `EOD` ends the here-document, and then the whole thing is wrapped in a `<( )` process substitution block. The result is that `expect` will see a temporary filename including the contents of your here-document.
    
As @Aserre noted, the quotes in `<<'EOD'` mean that everything in your here-document will be treated literally. Leave them off to expand Bash variables and the like inside the script, if that's what you want.
    
2.  **Edit** Variable+here-document:
    
``` bash
    IFS= read -r -d '' expect_commands <<'EOD'
spawn ... (your script here)
interact
EOD

expect -c "${expect_commands//
/;}"
```
    
 Yes, that is a real newline after `//` - it's not obvious to me how to escape it. That turns newlines into semicolons, which the [man page](https://linux.die.net/man/1/expect "https://linux.die.net/man/1/expect") says is required.
    
 Thanks to [this answer](https://stackoverflow.com/a/1655389/2877364 "https://stackoverflow.com/a/1655389/2877364") for the `read`+heredoc combo.
    
3.  Shell variable
``` bash
expect_commands='
spawn ... (your script here)
interact'
expect -c "${expect_commands//
/;}"
```

Note that any `'` in the expect commands (e.g., after `id_rsa`) will need to be replaced with `'\''` to leave the single-quote block, add a literal apostrophe, and then re-enter the single-quote block. The newline after `//` is the same as in the previous option.

Example

``` bash
#!/bin/bash
for IP in $(cat ./fws.txt)
do
echo "sshpass -f ~/.ssh/fwpass.conf -v ssh -o '"StrictHostKeyChecking no"' briadmin@$IP show inventory | tee ssh-session.log" > ./fw.sh
chmod +x ./fw.sh

  /usr/bin/expect <(cat << EOD
spawn ./fw.sh
expect "show inventory"
send -- "\r"
send -- "exit\r"
expect eof

EOD
)

cat ssh-session.log >> log
done 
```

