## Implement Azure Policies

Create restrictions and requirements for resource creation
e.g
* Limit VM SKU to selection
* Limit resource to selected regions
* Require tags and their values



## Role Based Access and Control (RBAC)

EntraID roles vs Azure RBAC roles
* Entra ID roles are limited to Entra ID objects
* Azure RBAC manage access to Azure resources
