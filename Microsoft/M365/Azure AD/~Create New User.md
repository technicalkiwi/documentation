Go to the [AzureAD Admin Portal](https://aad.portal.azure.com/#@briscoegroup.co.nz/dashboard)  
Logon using your Microsoft Online Account ( Same as Office 365)  

* Select Users in Navigation Pane  
[![AzureAD_Create_User1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/AzureAD_Create_User1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/AzureAD_Create_User1.png)

* Select New User   
[![AzureAD_Create_User2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/AzureAD_Create_User2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/AzureAD_Create_User2.png)

* Select Create User  
* Enter Identity Information  
* Select Auto-Generate Password  
[![AzureAD_Create_User3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/AzureAD_Create_User3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/AzureAD_Create_User3.png)

* Under Groups click on 0 selected,
* Add into Security Group for licensing
* Roles should have User by default.
* Set Usage Location to New Zealand
* Select Create
[![AzureAD_Create_User4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/AzureAD_Create_User4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/AzureAD_Create_User4.png)

* Save Username and Password to keypass
[![AzureAD_Create_User5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-06/scaled-1680-/AzureAD_Create_User5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-06/AzureAD_Create_User5.png)