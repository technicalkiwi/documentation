Go to the [Teams Admin Console](https://admin.teams.microsoft.com/dashboard)  
Select Teams > Manage Teams  
This will take you to the Teams overview
[![G3jTeams_manage_teams1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/G3jTeams_manage_teams1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/G3jTeams_manage_teams1.png)

Select the Team you want to manage  
It will display information about the team
[![OD3Teams_manage_teams2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/OD3Teams_manage_teams2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/OD3Teams_manage_teams2.png)

Under the Channel tab you can see the channels within the team  
[![Teams_manage_teams4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_manage_teams4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_manage_teams4.png)

Under the Settings Tab you can set team specific settings.  
[![W48Teams_manage_teams3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/W48Teams_manage_teams3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/W48Teams_manage_teams3.png)