# Managing ZFS Datasets

## Creating a Dataset

### Create a Dataset
Use the following command to create a new dataset under a zpool

ZFS automatically mounts the newly created file system if it is created successfully. By default, file systems are mounted as /dataset, using the path provided for the file system name in the create subcommand. In this example, the newly created bonwick file system is mounted at /zpool/media

`zfs create zpool/media`

### Create a new Mountpoint

You can set file system properties when the file system is created.

In the following example, a mount point of /share is created for the zpool/media file system:

`zfs create -o mountpoint=/share zpool/media`
<br></br>

## Renaming a Dataset
### Rename Dataset

File systems can be renamed by using the zfs rename command. With the rename subcommand, you can perform the following operations:

* Change the name of a file system.

* Relocate the file system within the ZFS hierarchy.

* Change the name of a file system and relocate it within the ZFS hierarchy.

## Destroying a Dataset
### Destroy a Datset
To destroy a ZFS file system, use the zfs destroy command. The destroyed file system is automatically unmounted and unshared

**No confirmation prompt appears with the destroy subcommand. Use it with extreme caution**

`zfs destroy zpool/media`
<br></br>

### Force Destroy
If the file system to be destroyed is busy and cannot be unmounted, the zfs destroy command fails. To destroy an active file system, use the -f option. Use this option with caution as it can unmount, unshare, and destroy active file system

`zfs destroy zpool/media`

*cannot unmount 'tank/home/ahrens': Device busy*

`zfs destroy -f zpool/media`
<br></br>

### Recursive Destroy
The zfs destroy command also fails if a file system has descendents. To recursively destroy a file system and all its descendents, use the -r option. Note that a recursive destroy also destroys snapshot

`zfs destroy tank/ws`
<pre>
cannot destroy 'zpool/media': filesystem has children
use '-r' to destroy the following datasets:
zpool/media/movies
zpool/media/tv
zpool/media/comedy
</pre>
`zfs destroy -r zpool/media`