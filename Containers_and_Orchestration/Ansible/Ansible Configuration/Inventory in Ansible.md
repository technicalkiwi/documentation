# Inventory File
[How to build your inventory — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

## INI Style
### Creating Inventory File
Used to specify Machines using Ip or DNS Name
Seperate into groups using "[Title]"
Then insert the machines in that group

e.g
```
# Servers running git
[git]
mcl-lnx-git01

# Servers running docker
[docker]
mcl-lnx-dkr01
```


### Specify Nested Group

Use ":children" to tell ansible that the group contains other groups rather than machines

e.g
``` yaml
  [ubuntu:children]
  git
  docker
```

### Specify Group Settings
Add in Variable for specific group
Use ":vars" to specify settings for just that group

e.g
```
  [ubuntu:vars]
  ansible_user=tkiwi
  ansible_ssh_private_key_file=~/.ssh/id_rsa
```


## YAML STYLE

