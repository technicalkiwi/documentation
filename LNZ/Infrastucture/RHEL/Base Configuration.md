
OS: Red Hat Enterprise Linux Minimal Install
SE Linux: Enabled

OPENSCAP Control Baseline with PCI-DSS

IP tables and firewalld disabled

No root access.
Password SSH disabled - SSH key only

Configure NTP to use Lotto internal

OSSEC agents installed and configured


## Monitoring
Syslog or Splunk agents deployed


## Repos
Restrict to RHEL and EPEL only
Register with Red Hat Satellite

