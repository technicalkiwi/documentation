# Turn external sharing on or off for a site

1. Go to the [Active sites page of the new SharePoint admin center](https://admin.microsoft.com/sharepoint?page=siteManagement&modern=true), and sign in with an account that has [admin permissions](https://docs.microsoft.com/en-US/sharepoint/sharepoint-admin-role) for your organization.

2. In the left column, select a site.

3. Select **Sharing**.

4. Select an external sharing option (see the following table).

   ![Changing the external sharing setting for a site](https://docs.microsoft.com/en-US/sharepoint/sharepointonline/media/external-sharing-site.png)
   
5. If you want to limit the sharing of this site by domain, select the **Limit sharing by domain** check box, and add the domains that you want to allow or block.

   ![SP_External_Sharing2](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\M365\Sharepoint\resources\SP_External_Sharing2.png)

6. If you want to change the default sharing link type, permissions, or expiration setting for this site, clear the **Same as organization-level setting** check box and set the value that you want to use for this site. For more info, see [Change the default sharing link for a site](https://docs.microsoft.com/en-US/sharepoint/change-default-sharing-link).

   ![SP_External_Sharing3](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\M365\Sharepoint\resources\SP_External_Sharing3.png)

7. Select **Save**.