# Installing Teams on your Mobile Phone

### Download the App from the Play store

Open the play store App

![](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (1).jpg)

Search for Teams

Select Microsoft Teams

![Teams_mobile_install (2)](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (2).jpg)

Download the App

![Teams_mobile_install (3)](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (3).jpg)



### Logging into Teams

Once Downloaded Open the App

Login using your Office 365 Account ( Contact the IT team for details if needed)

![Teams_mobile_install (5)](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (5).jpg)



Enter Password

![Teams_mobile_install (6)](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (6).jpg)

### Using Teams

Along the Bottom of the App you have the different feature tabs you can switch between

![Teams_mobile_install (7)](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Teams\resources\Teams_mobile_install (7).jpg)