## Running Ad-Host Commands
You can run commands as needed using ansible on an Ad-Hoc basis
Run it directly from the command line from within the project folder
```bash
Ansible {Group} -i {Inventory File} -m {Module} -a {Command to run} -u {User to connect as}
```
e.g

`ansible docker -i ./inventory -m command -a "date" -u tkiwi`

## Command Line flags
-i is used to specify a inventory file
-m is used to specify the module
-a is the action to use with the module
-u is the user to connect as
-k is used to specify the --ask-pass flag
-f specify the number of forks


## Examples

Gather System information
`ansible lnx -i ./inventory -m command -m setup -u tkiwi`
