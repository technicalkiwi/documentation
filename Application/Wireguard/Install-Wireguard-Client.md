# Configuring a WireGuard Peer
### Install Wireguard
Ensure that you have the WireGuard package installed 

`sudo apt update`
`sudo apt install wireguard`

### Creating the WireGuard Peer’s Key Pair
Create the private key for the peer
`wg genkey | sudo tee /etc/wireguard/private.key`
`sudo chmod go= /etc/wireguard/private.key`

A copy of the output is also stored in the /etc/wireguard/private.key.

Next use the following command to create the public key file:

`sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key`

A copy of the output is also stored in the /etc/wireguard/public.key. 
You will need to distribute the public key to the WireGuard Server in order to establish an encrypted connection.

### Creating the WireGuard Peer’s Configuration File
Now that you have a key pair, you can create a configuration file for the peer that contains all the information that it needs to establish a connection to the WireGuard Server.

You will need a few pieces of information for the configuration file:

* The private key that you generated on the peer.
* The IPv4 address range sthat you defined on the WireGuard Server.
* The public key from the WireGuard Server.
* The public IP address and port number of the WireGuard Server.
    * Or a DNS entry set to point to the public IP of the Wireguard Server

Create a config file /etc/wireguard/wg0.conf
`sudo nano /etc/wireguard/wg0.conf`

Add the following lines to the file, substituting in the various data into the highlighted sections as required:

<pre>
# /etc/wireguard/wg0.conf
[Interface]
PrivateKey = peer_private_key_goes_here
Address = 10.10.10.2/24

[Peer]
PublicKey = server_public_key_goes_here
AllowedIPs = 10.10.10.0/24
Endpoint = vpn.technicalkiwi.com:51820
</pre>

- **PrivateKey** The private key of the Wireguard Peer

 - **Address** defines the private IPv4 for the WireGuard Peer. Each peer in the VPN network should have a unique value for this field.

- **PublicKey** Enter the public key of the Wireguard Server that the peer will be connecting to

- **AllowedIPs** The IP addresses to route the traffic of
    - Setting this to 0.0.0.0/0 will route all traffic through the wireguard server
    - Setting this to 10.10.10.0/24 will route only traffic for ips 10.10.10.0/24 through the wireguard server

- **Endpoint** The address and port of the Wireguard Server


### Adding Peer Information to Wireguard Server
On the Wireguard server
Open wg0.conf
`nano /etc/wireguard/wg0.conf`
Add a new section for the peer

<pre>
[Peer]
PublicKey = peer_public_key_goes_here
AllowedIPs = 10.10.10.2/32

</pre>

- **PublicKey** Enter the public key of the peer that will be connecting to the server
- **AllowedIPs** The IP address the peer will be using when connected to the VPN network
   - This should match the Address set on the peer.