### Nagios Core

## Include URL in Notifications

**Setup URL**

There is a configuration directive called website_url that needs to be defined in nagios.cfg before you can use the URL in your notifications.

Execute the following command to open the nagios.cfg file in vi:

`1vi /usr/local/nagios/etc/nagios.cfg`

Add the following line to the file:

`1website_url=http://xxx.xxx.xxx.xxx/nagios`

Change xxx.xxx.xxx.xxx to the IP Address or DNS record of your Nagios Core server, for example:

`1website_url=http://core-015/nagios`

Save your changes and close the nagios.cfg file.

  
The next step is to update your host and service notification commands to use the relevant macros. In this example these are defined in the commands.cfg file. Execute the following command to open the file in vi:

`1vi /usr/local/nagios/etc/objects/commands.cfg`

#### **Configure Command File**

For the **host** notification command you use the $HOSTINFOURL$ as per the following example:

``` bash
define command{
        command_name    notify-host-by-email
        command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\n
                        State: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n
                        Host Info URL: $HOSTINFOURL$\n" | /bin/mail -s "** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **" $CONTACTEMAIL$
        }
```

For the **service** notification command you use the $SERVICEINFOURL$ as per the following example:
``` bash
define command{
        command_name    notify-service-by-email
        command_line    /usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\n
                        Host: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n
                        Service Info URL: $SERVICEINFOURL$\nAdditional Info:\n\n$SERVICEOUTPUT$\n" | /bin/mail -s "** $NOTIFICATIONTYPE$
                        Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **" $CONTACTEMAIL$
        } 
 ```

Save your changes and close the commands.cfg file.

Restart the nagios service for these changes to take effect.