# Using Apt with Ansible
Ansible Apt is one of the modules of Ansible that is used to manage packages on Debian or Ubuntu-based OS. For example, installing packages to the hosts, removing packages from the hosts, and updating packages.

## Installing a single package on the hosts using the apt module
In order to install any package using apt-module, we need just provide the name of the package, optionally we can set the ‘state’ attribute to ‘present’ as below Ansible playbook. We can also set the ‘state’ attribute to ‘latest’ in order to install the latest version of the package.

``` bash
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Install nginx
  apt:
  name: nginx
  state: present
```

**Explanation**: In the above example, the playbook is running under user $username and it requires sudo privilege to manage packages so also set the ‘become’ attribute to ‘yes’ and it installed ‘nginx’ on the host $hostname

## Installing multiple packages to the host using the apt module
There are different ways to install multiple packages using apt, the first one is to use ‘pkg’ attribute and write all the packages that need to be installed on the hosts and the second one is to use ‘Ansible Loop’ and pass the value to the ‘name’ attribute one by one.

### Using the pkg attribute
``` yaml
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Install mulitple packages
apt:
pkg:
- nginx
- curl
state: present
```

### Using a Loop
<pre>
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Install mulitple packages
apt:
name: "{{ item }}"
state: present
loop:
- nginx
- curl
</pre>

## Removing or uninstalling package from the hosts using the apt module
We need to just change the ‘state’ attribute to ‘absent’ in order to remove or uninstall any package from the hosts as below Ansible playbook.

<pre>
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Uninstall/removenginx
apt:
name: Nginx
state: absent
</pre>
**Explanation**: In the above example, the playbook uninstalled the ‘nginx’ from the host $hostname however, there is not much difference in the console output as compare to the output of ‘Install nginx’ task except the name of the task.

 

### Update repositories cache and install the package
We use the ‘update_cache’ attribute to update the repositories cache and the name of the package to install, here we don’t need to use the ‘state’ attribute. We can also use the ‘cache_valid_time’ attribute to update the cache only if the last update is 3600 seconds ago.

<pre>
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: update repositories cache and install nginx package
apt:
name: nginx
update_cache: yes
cache_valid_time: 3600
</pre>

### Installing Debian package using the apt-module
We can install .deb package to the host using ‘deb’ attribute and pass the path of the Debian package. Debian package must be available on the host on which we need to install the deb package and we provide the path where the package is available locally.

<pre>
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Installing debian package
apt:
deb: /home/ssingh/curl_7.58.0-2ubuntu3.8_amd64.deb
</pre>

### Removing useless packages and dependencies
We need to set ‘autoclean’ attribute to ‘yes’ to remove useless packages from the cache as below: 
We use the ‘autoremove’ attribute to remove the dependencies that are no longer required from the hosts.

<pre>
- hosts: $hostname
become: yes
remote_user: $username
tasks:
- name: Removing useless packages from the cache
apt:
autoclean: yes

tasks:
- name: Removing old dependencies that is no longer required
apt:
autoremove: yes
</pre>