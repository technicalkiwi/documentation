# Creating Playbook
Start the file with --- on the top line

Define the name of playbook.
Define the hosts to run the playbook against.

 

### Tasks
Define the Name of the task
Define the package to use for the Task

 

<pre>
--- # Install Nginx
- name: Install Nginx
  hosts: # Define the Hosts to run the playbook against.
  become: true
  vars_files: # Define any Var files to include
    - vars/default.yml

  tasks: # Start a task
    - name: Install Nginx # Give the task a name
        apt: # Define the Package to use for the task 
        name: "{{item}}" 
        state: lastest # Define the State of the package
        update_cache: yes # Update the apt cache
      loop: 
        - nginx

    - name: Check Nginx Service has Started #Give the task a name
      service: # Define the Package to use for the task  
        name: nginx # Define the Package to check
        state: started #Define the desired State of the package
        enabled: yes #Set Enabled State of Package
</pre>