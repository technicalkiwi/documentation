There are 2 ways to manage an openshift cluster

Through the web ui and via the command line tool 1`oc`



## Command Line 

Login to the cluster
`oc login -u developer -p developer https://api.ocp4.example.com:6443`

#### Info Gathering
**Show console address** 
`oc whoami --show-console`

**Show OCP Version**
`oc version`

**Show cluster info**
The cluster-info command prints the address of the control plane and other cluster services.
`oc cluster-info`

**Show api versions**
The command prints the supported API versions on the server, in the form of "group/version".
`oc api-versions`

Identify fields for a resource
`oc explan $resource`
e.g `oc explain pod.spec.securityContext` 


##### Show brief information about resources
Generally, this command shows only the most important characteristics of the resources, and omits more detailed information
`oc get $resourcename`

Use the oc get clusteroperator command to see a list of the cluster operators
`oc get clusteroperator`

Return the list of the pod resources in the current project:
`oc get pod`

Iterate through the major resource types for the current project, and prints a summary of their information
`oc get all`

List all projects
`oc get project`



**Show detailed information about resources** 
Use the oc describe command to show in depth information about a resource
`oc describe $resourcename`


#### Managing resources
**List resources**
`oc api-resources`

List all namespaced resources
`oc api-resources --namespaced`

List all resources belonging to a specific group
`oc api-resources --api-group $groupname`


#### Managing Pods
List pods in current project
`oc get pods`

View configurations events for pod
`oc describe pod $podname`

#### Project Management

List current Project
`oc project`

Set project 
`oc project $projectname`

Create a new project
`pc new-project $projectname`



#### Health Checks
Examine the compute resource usage of cluster nodes and pods
`oc adm top $resourcename`

List the total memory and CPU usage of all pods in the cluster, 
* --sum  to print the sum of the resource usage
* -A to include pods from all namespaces
`oc adm top pods -A --sum`



Get logs from node
`oc adm node-logs $nodename`


Get events
`oc get events`

Get events from a specific namespace
`oc get events -n $namespace`


#### Debug 

Shell into the node to gather more information
`oc debug node/$nodename`
You will need to run `chroot /host` to run most commands

Gather debugging information for the cluser
`oc adm must-gather --dest-dir $directory`

Gather debugging information for specific operators
`oc adm inspect $operatorname --dest-dir $directory`

## Web ui

#### Login
Go to the console address
Select Identify provider
Login


Get Login token for CLI
Select user name
Select copy login command
![[Pasted image 20240520164255.png]]

It should generate and display your token as well as a prebuilt login command
![[Pasted image 20240520164328.png]]

#### Create new project
Go to the +add page
![[Pasted image 20240520142347.png]]

Select the project drop down
Select Create project
![[Pasted image 20240520142510.png]]

Create your new project
![[Pasted image 20240520142311.png]]


## Health Checks
### Check application Status
##### Project status
Go to the home page
Select your project
Go the details page
![[Pasted image 20240520143137.png]]

##### In depth
Go the workloads page
Select the component to check the status of
![[Pasted image 20240520142742.png]]
It will show the status of the various parts 
![[Pasted image 20240520142849.png]]


### Monitor Cluster health

#### Cluster Overview
go to home > overview
![[Pasted image 20240520155113.png]]


#### Cluster Alerts
Go to to observer menu
Select alerting
Filter alerts as needed
![[Pasted image 20240520154928.png]]



C, ace, abd , be, bde,