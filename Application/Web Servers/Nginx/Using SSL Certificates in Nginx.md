## Editing Server Blocks

To use our newly generated certificates we need to edit our Nginx Server block to listen on port 443 and tell Nginx where the certs are located.

Create a new server listening on port 443 and include to SSL parameter like below

For Nginx to be able to function under SSL and make use of our new certs, we have to point to each of the components that make up the ssl certificates.

For example
![ssl_certs](ssl_block.png)
![](blob:https://technicalkiwi.atlassian.net/6ba2c795-c43d-4c2a-bd56-7beb65723a30#media-blob-url=true&id=64b50330-579c-4f57-a39f-5998899f30ea&collection=contentId-1252491265&contextId=1252491265&mimeType=image%2Fpng&name=ssl_config.PNG&size=9365&height=56&width=547&alt=)

##### Redirecting to HTTPS

Now that we have the server listening on port 443 and serving up traffic secured by SSL, we want to force all incoming traffic onto https.

To do this we setup at new server listening on port 80. Once traffic hits this server it is then redirected to use the HTTPS version of the URL.

Redirect Example
![ssl_certs](ssl_redirect.png)
![](blob:https://technicalkiwi.atlassian.net/bbab02f1-cd92-4368-b499-8833ecef601c#media-blob-url=true&id=33455f50-dcb4-4e46-9c6e-a7de9f7e4df3&collection=contentId-1252491265&contextId=1252491265&mimeType=image%2Fpng&name=https_redirect.PNG&size=8815&height=137&width=488&alt=)

##### Server Block

Below is an example server block in its entirety

``` bash
server{  
    listen 443 ssl;  
    listen [::]:443 ssl;  
​
    server_name example.technicalkiwi.com;
​
    # SSL
    ssl_certificate /etc/letsencrypt/live/example.technicalkiwi.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.technicalkiwi.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/example.technicalkiwi.com/chain.pem;
​
    location / {
            proxy_pass http://localhost:5001;
            proxy_set_header Host $http_host;
            proxy_set_header X-Fowarded_for $remote_addr;
    }  
}  
​
    #Http Redirect  
server{  
    listen 80;  
    listen [::]:80;
​
    server_name example.technicalkiwi.com www.example.technicalkiwi.com;
​
    location / {
            return 301 https://example.technicalkiwi.com$request_uri;
​
    }
}
```

![ssl_certs](ssl_block1.png)