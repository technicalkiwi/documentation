## Prerequisites 
Tomcat requires Java to function
#### OpenJDK-17

Create Java directory
`mkdir /opt/java-17`

Download Openjdk
Copy java tar.gz to /tmp on the  server
Unpack to /tmp/java-17-xxxxx
Copy files from /tmp/java-17-xxx to /opt/java-17/*

Set JAVA_HOME in /etc/environment
`JAVA_HOME=/opt/java-17 >> /etc/environment`

Add Java to path
`export PATH="$JAVA_HOME/bin:$PATH"`

## Tomcat 10

Create Tomcat Group
``` shell
groupadd tomcat
```

Create Tomcat User and add to tomcat group
``` shell
usradd -G tomcat tomcat
```

Create Tomcat directory
``` shell
mkdir /opt/tomcat10
```

Download tomcat 10  
Copy tomcat tar.gz to /tmp on the RPS server

Unpack to /tmp/tomcat-xxxxx

Copy files from /tmp/tomcat-xxx to /opt/tomcat10/*

Set ownership recursively of /opt/tomcat10 to tomcat user/group
``` shell
chown -R tomcat:tomcat /opt/tomcat10
```

Symlink tomcat10 to tomcat
```shell 
ln -s /opt/tomcat10 /opt/tomcat
```

Restore SELINUX for tomcat dir if needed  
`restorecon -RFv /opt/tomcat`

Create Tomcat service

`[Unit] Description=Tomcat 10 After=network.target [Service] Type=forking User=tomcat Group=tomcat Environment="JAVA_HOME=/opt/java-17" Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom" Environment="CATALINA_BASE=/opt/tomcat" Environment="CATALINA_HOME=/opt/tomcat" Environment="CATALINA_PID=/opt/tomcat/temp/tomcat.pid" Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC" ExecStart=/opt/tomcat/bin/startup.sh ExecStop=/opt/tomcat/bin/shutdown.sh [Install] WantedBy=multi-user.target`

Enable and start tomcat service.