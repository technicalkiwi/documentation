#### Demotion
Run CMD as and Administrator  
Run Command " DCPROMO"  
[![Server_Upgrade4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade4.png)

Leave the delete domain box UNTICKED  
Click Next  
[![Server_Upgrade5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade5.png)

Do not Delete DNS Delegations  
[![Server_Upgrade6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade6.png)

Set New Adminitrator Password  
[![Server_Upgrade7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade7.png)

Review the Summary  
Click next to kick off the Demotion  
[![Server_Upgrade8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade8.png)

#### Role Removal
Once the Demotion is Complete Open Server manager  
Select Roles Dropdown  
Then Select Remove Roles  
[![Server_Upgrade9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade9.png)

Untick the Active Directory Domain Controller Box  
[![Server_Upgrade10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade10.png)

Confirm the Removal  
[![Server_Upgrade11.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade11.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade11.png)

The Removal will begin to initialize
[![Server_Upgrade12.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade12.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade12.png)

#### Check Operation was Succesfull
Check the Server has been moved into the Computers OU in active Directory  
[![Server_Upgrade13.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade13.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade13.png)

Right Click and Open Properties  
[![Server_Upgrade14.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade14.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade14.png)

Check DC Type is Workstation or Server  
[![Server_Upgrade15.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Server_Upgrade15.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Server_Upgrade15.png)