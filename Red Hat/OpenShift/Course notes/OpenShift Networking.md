Kubernetes implements software-defined networking (SDN) to manage the network
 infrastructure of the cluster and user applications. The SDN is a virtual network that encompasses
 all cluster nodes

This virtual network enables communication between any container or pod inside the cluster, however, the SDN is not accessible from outside the cluster, nor to regular processes on cluster nodes

Kubernetes networking provides the following capabilities:
 • Highly coupled container-to-container communications
 • Pod-to-pod communications
 • Pod-to-service communications
 • External-to-service communication


## Services

Containers inside Kubernetes pods must not connect directly to each other's dynamic IP address.
 Instead, Kubernetes assigns a stable IP address to a service resource that is linked to a set of
 specified pods. The service then acts as a virtual network load balancer for the pods that are
 linked to the service.
 
 If the pods are restarted, replicated, or rescheduled to different nodes, then the service endpoints
 are updated, thus providing scalability and fault tolerance for your applications. Unlike the IP
 addresses of pods, the IP addresses of services do not change

![[Pasted image 20240522112407.png]]


With services, containers in one pod can open network connections to containers in another pod.
 The pods, which the service tracks, are not required to exist on the same compute node or in the
 same namespace or project. Because a service provides a stable IP address for other pods to
 use, a pod also does not need to discover the new IP address of another pod after a restart. The
 service provides a stable IP address to use, no matter which compute node runs the pod after
 each restart
 
The SERVICE object provides a stable IP address for the CLIENT container on NODE X to send a
 request to any one of the API containers.
 
 Kubernetes uses labels on the pods to select the pods that are associated with a service. To
 include a pod in a service, the pod labels must include each of the selector fields of the service.
![[Pasted image 20240522113246.png]]



### Kubernetes DNS for Service Discovery

Kubernetes uses an internal Domain Name System (DNS) server that the DNS operator deploys. The DNS operator creates a default cluster DNS name, and assigns DNS names to services that you define.
When a service does not have a cluster IP address, the DNS operator assigns to the service a DNS record that resolves to the set of IP addresses of the pods behind the service.

The DNS server discovers a service from a pod by using the internal DNS server, which is visible only to pods. Each service is dynamically assigned a Fully Qualified Domain Name (FQDN) that uses the following format: `SVC-NAME.PROJECT-NAME.svc.CLUSTER-DOMAIN`

When a pod is created, Kubernetes provides the container with a /etc/resolv.conf file with the cluster information
e.g
``` shell
## /etc/resolv.conf
 search deploy-services.svc.cluster.local svc.cluster.local
 nameserver 172.30.0.10
 options ndots:5
```

The `nameserver` directive provides the IP address of the Kubernetes internal DNS server. 
The options `ndots` directive specifies the number of dots that must appear in a name to qualify for
 an initial absolute query.
  Alternative hostname values are derived by appending values from the `Search` directive to the name that is sent to the DNS server.
 
 In the `search` directive in this example, the svc.cluster.local entry enables any pod to communicate with another pod in the same cluster by using the service name and project name:
 
 The service name alone is sufficient for pods in the same RHOCP project:

### Service Types

Most real-world applications do not run as a single pod. Because applications need to scale
horizontally, many pods run the same containers from the same pod resource definition, to meet
growing user demand. A service defines a single IP/port combination, and provides a single IP
address to a pool of pods, and a load-balancing client request among member pods.
#### Cluster IP
This type is the default, unless you explicitly specify a type for a service. The ClusterIP type
 exposes the service on a cluster-internal IP address. If you choose this value, then the service
 is reachable only from within the cluster.
 
 The ClusterIP service type is used for pod-to-pod routing within the RHOCP cluster, and
 enables pods to communicate with and to access each other. IP addresses for the ClusterIP
 services are assigned from a dedicated service network that is accessible only from inside the
 cluster. Most applications should use this service type, for which Kubernetes automates the
 management

#### Load Balancer
This resource instructs RHOCP to activate a load balancer in a cloud environment. A load
 balancer instructs Kubernetes to interact with the cloud provider that the cluster is running
 in, to provision a load balancer. The load balancer then provides an externally accessible IP
 address to the application.
 
 Take all necessary precautions before deploying this service type. Load balancers are typically
 too expensive to assign one for each application in a cluster. Furthermore, applications that
 use this service type become accessible from networks outside the cluster. Additional security
 configuration is required to prevent unintended access

#### NodePort
With this method, Kubernetes exposes a service on a port on the node IP address. The port is
 exposed on all cluster nodes, and each node redirects traffic to the endpoints (pods) of the
 service.
 
 A NodePort service requires allowing direct network connections to a cluster node, which is a
 security risk

#### External Name
This service tells Kubernetes that the DNS name in the externalName field is the location of the resource that backs the service. 
When a DNS request is made against the Kubernetes DNS server, it returns the externalName in a Canonical Name (CNAME) record, and directs the client to look up the returned name to get the IP address.



## Routes for External Connectivity
 RHOCP provides resources to expose your applications to external networks outside the cluster.
 You can expose HTTP and HTTPS traffic, TCP applications, and also non-TCP traffic. 
 
 However, you should expose only HTTP and TLS-based applications to external access. 
Applications that use other protocols, such as databases, are usually not exposed to external access (from outside a cluster). 
 
 Routes and ingress are the main resources for handling ingress traffic, routes provide ingress traffic to services in the cluster.

### Creating Routes
To create a route (secure or insecure) with the oc CLI, use the `oc expose service $servicename` command. Include the --hostname option to provide a custom hostname for the route
e.g 
`oc expose service api-frontend --hostname api.apps.acme.com`

If you omit the hostname, then RHOCP automatically generates a hostname with the following structure: `<route-name>-<project-name>.<default-domain>`
e.g create a frontend route in an api project, in a cluster that uses apps.example.com as the wildcard domain, then the route hostname is as follows `frontend-api.apps.example.com`

 **Consider the following settings when creating a route:**
  The name of a service. The route uses the service to determine the pods to direct the traffic to.
* A hostname for the route. A route is always a subdomain of your cluster wildcard domain
* A target port that the application listens to. The target port corresponds to the port that you define in the `targetPort` key of the service.
* An encryption strategy, depending on whether you need a secure or an insecure route

### Sticky Sessions

Sticky sessions enable stateful application traffic by ensuring that all requests reach the same
 endpoint. RHOCP uses cookies to configure session persistence for ingress and route resources.
 The ingress controller selects an endpoint to handle any user requests, and creates a cookie for
 the session. The cookie is passed back in response to the request, and the user sends back the
 cookie with the next request in the session. The cookie tells the ingress controller which endpoint
 is handling the session, to ensure that client requests use the cookie so that they are routed to the
 same pod

### Load Balance and Scale Applications

Developers and administrators can choose to manually scale the number of replica pods in a
deployment. More pods might be needed for an anticipated surge in traffic, or the pod count
might be reduced to reclaim resources that the cluster can use elsewhere.


A Kubernetes service serves as an internal load balancer. Standard services act as a load balancer or a proxy, and give access to the workload object by using the service name. A service identifies a set of replicated pods to transfer the connections that it receives

The router uses the service selector to find the service and the endpoints, or pods, that back the service. When both a router and a service provide load balancing, RHOCP uses the router to load balance traffic to pods. A router detects relevant changes in the IP addresses of its services, and adapts its configuration accordingly


