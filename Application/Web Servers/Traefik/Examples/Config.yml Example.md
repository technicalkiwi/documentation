``` yml
# dynamic.yml
http:
  routers:
    unifi:
      entryPoints:
        - "https"
      rule: "Host(`unifi.techncialkiwi.com`)"
      middlewares:
        - default-headers
        - https-redirectscheme
      tls: {}
      service: unifi
    gitlab:
      entryPoints:
        - "https"
      rule: "Host(`gitlab.technicalkiwi.com`)"
      middlewares:
        - default-headers
        - https-redirectscheme
      tls: {}
      service: gitlab

# Region Services
  services:
    unifi:
      loadBalancer:
        servers:
          - url: https://10.3.30.188:8443/manage
        passHostHeader: true
    gitlab:
      loadBalancer:
        servers:
          - url: http://10.3.30.192:80
        passHostHeader: true

  middlewares:
    https-redirectscheme:
      redirectScheme:
        scheme: https
        permanent: true

    default-headers:
      headers:
        frameDeny: true
        browserXssFilter: true
        contentTypeNosniff: true
        forceSTSHeader: true
        stsIncludeSubdomains: true
        stsPreload: true
        stsSeconds: 15552000
        customFrameOptionsValue: SAMEORIGIN
        customRequestHeaders:
          X-Forwarded-Proto: https

    user-auth:
      basicAuth:
        users:
          - "tkiwi:$apr1$8QFrbb5P$d5EUBaNC/cyAoScM6rNQH0"

tls:
  options:
    default:
      cipherSuites:
        - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
        - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
      minVersion: VersionTLS12
```

