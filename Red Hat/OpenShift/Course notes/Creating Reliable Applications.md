High availability (HA) is a goal of making applications more robust and resistant to runtime failures.
Implementing HA techniques decreases the likelihood that an application is completely unavailable to users.
 
 In general, HA can protect an application from failures in the following contexts:
 * From itself in the form of application bugs
 * From its environment, such as networking issues
 * From other applications that exhaust cluster resources

## Writing Reliable Applications

Applications must work with the cluster so that Kubernetes can best handle failure scenarios.
Kubernetes expects the following behaviors from applications:
 * Tolerates restarts
 * Responds to health probes, such as the startup, readiness, and liveness probes
 * Supports multiple simultaneous instances
 * Has well-defined and well-behaved resource usage
 * Operates with restricted privileges

## Kubernetes Application Reliability

If an application pod crashes, then it cannot respond to requests. Depending on the configuration,
the cluster can automatically restart the pod. If the application fails without crashing the pod, then
the pod does not receive requests. However, the cluster can do so only with the appropriate health probes.
 
 Kubernetes uses the following HA techniques to improve application reliability:
 * Restarting pods: By configuring a restart policy on a pod, the cluster restarts misbehaving instances of an application.
 * Probing: By using health probes, the cluster knows when applications cannot respond to requests, and can automatically act to mitigate the issue.
 * Horizontal scaling: When the application load changes, the cluster can scale the number of replicas to match the load.



## Application Health Probes

Health probes are an important part of maintaining a robust cluster. Probes enable the cluster to determine the status of an application by repeatedly probing it for a response.
 
 A set of health probes affect a cluster's ability to do the following tasks:
* Crash mitigation by automatically attempting to restart failing pods
* Failover and load balancing by sending requests only to healthy pods
* Monitoring by determining whether and when pods are failing
* Scaling by determining when a new replica is ready to receive requests

### Probe Types
#### Readiness Probes
 A readiness probe determines whether the application is ready to serve requests. If the readiness
 probe fails, then Kubernetes prevents client traffic from reaching the application by removing the
 pod's IP address from the service resource.

Kubernetes continues to run the probe even after the application fails. If the probe succeeds
 again, then Kubernetes adds back the pod's IP address to the service resource, and requests are
 sent to the pod again

#### Liveness Probes
Like a readiness probe, a liveness probe is called throughout the lifetime of the application. Liveness probes determine whether the application container is in a healthy state. If an application
fails its liveness probe enough times, then the cluster restarts the pod according to its restart policy.
 
Unlike a startup probe, liveness probes are called after the application's initial start process.
Usually, this mitigation is by restarting or re-creating the pod.

#### Startup Probes
A startup probe determines when an application's startup is completed. Unlike a liveness probe, a startup probe is not called after the probe succeeds. If the startup probe does not succeed after a configurable timeout, then the pod is restarted based on its restartPolicy value


### Type of Probe Checks

#### HTTP GET
Each time that the probe runs, the cluster sends a request to the specified HTTP endpoint.
The test is considered a success if the request responds with an HTTP response code
between 200 and 399. Other responses cause the test to fail.
 
#### Container command
Each time that the probe runs, the cluster runs the specified command in the container. If the
command exits with a status code of 0, then the test succeeds. Other status codes cause the
test to fail.
 
#### TCP socket
Each time that the probe runs, the cluster attempts to open a socket to the container. The test succeeds only if the connection is established


### Adding Probes via YAML
Because probes are defined on a pod template, probes can be added to workload resources such as deployments. To add a probe to an existing deployment, update and apply the YAML file or use the oc edit command. For example, the following YAML excerpt defines a deployment pod template with a probe:

``` yaml
apiVersion: apps/v1
 kind: Deployment
 ...output omitted...
 spec:
 ...output omitted...
  template:
    spec:
      containers:
      - name: web-server
 ...output omitted...
        livenessProbe: 
          failureThreshold: 6 
          periodSeconds: 10 
          httpGet: 
            path: /health 
            port: 3000
```

* LivenessProbe: Defines a liveness probe.
 * failureThreshold: Specifies how many times the probe must fail before mitigating.
* periodSeconds: Defines how often the probe runs.
* httpGet: Sets the probe as an HTTP request and defines the request port and path.
* path: Specifies the HTTP path to send the request to.
* port: Specifies the port to send the HTTP request over

### Adding Probes via the CLI
The oc set probe command adds or modifies a probe on a deployment.
For example, the following command adds a readiness probe to a deployment called front-end:

``` bash
oc set probe deployment/front-end 
--readiness 
--failure-threshold 6 
--period-seconds 10 
--get-url http://:8080/healthz`
``` 

* Defines a readiness probe.
* Sets how many times the probe must fail before mitigating.
* Sets how often the probe runs.
* Sets the probe as an HTTP request, and defines the request port and path.


## Autoscaling

Kubernetes can autoscale a deployment based on current load on the application pods, by means
 of a HorizontalPodAutoscaler (HPA) resource type.
 
 A horizontal pod autoscaler resource uses performance metrics that the OpenShift Metrics
 subsystem collects. The Metrics subsystem comes preinstalled in OpenShift. To autoscale a
 deployment, you must specify resource requests for pods so that the horizontal pod autoscaler
 can calculate the percentage of usage.
 
 The autoscaler works in a loop. Every 15 seconds by default, it performs the following steps:
 • The autoscaler retrieves the details of the metric for scaling from the HPA resource.
 • For each pod that the HPA resource targets, the autoscaler collects the metric from the metric
 subsystem.
 • For each targeted pod, the autoscaler computes the usage percentage, from the collected
 metric and from the pod resource requests.
 • The autoscaler computes the average usage and the average resource requests across all the
 targeted pods. It establishes a usage ratio from these values, and then uses the ratio for its
 scaling decision.
 
 The simplest way to create a horizontal pod autoscaler resource is by using the oc autoscale
 command, for example:
 This command creates a horizontal pod autoscaler resource that changes the number of replicas on the hello deployment to keep its pods under 80% of their total requested CPU usage.
`oc autoscale deployment/hello --min 1 --max 10 --cpu-percent 80`


