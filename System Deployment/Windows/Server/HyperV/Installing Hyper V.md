Open Server Manager  
Select Add Roles And Features  
[![Add_HyperV1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV1.png)

Click Next through the before you begin window  
[![Add_HyperV2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV2.png)  

Select Role Based or Feature Based Installation  
[![Add_HyperV3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV3.png)  

Select the local server from the server Pool  
[![Add_HyperV4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV4.png)

Select Hyper-V from the list  
Accept the Features Required for Hyper-V  
Check the box to Include Management tools  
[![Add_HyperV5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV5.png)

Do not add any Features  
[![Add_HyperV6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV6.png)  

Click Next Past Hyper-V Initial Screen  
[![Add_HyperV7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV7.png)  

Do not Create a Virtual Switch  
[![Add_HyperV8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV8.png)  

Do not Allow server to send and Receive live migrations  
[![Add_HyperV9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV9.png)  

Set Default Location to Store Virtual Disks and Configuration Files  
[![Add_HyperV10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV10.png)  

Tick The Check box for Restart if required  
[![Add_HyperV11.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV11.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV11.png)  

Watch the Installation Begin  
[![Add_HyperV12.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Add_HyperV12.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Add_HyperV12.png)