Go to the Splunk deployment server

Login

Go to Settings > Forwarder Management
![splunk_ds_filtering1.png](./resources/splunk_ds_filtering1.png)

Go to Server Classes > Class to change
![splunk_ds_filtering2.png](./resources/splunk_ds_filtering2.png)

Select Edit > Edit Clients
![splunk_ds_filtering3.png](./resources/splunk_ds_filtering3.png)

Update the Include and Exclude lists as required
Save
![splunk_ds_filtering4.png](./resources/splunk_ds_filtering4.png)

Check server now shows in the matched hosts
![splunk_ds_filtering5.png](./resources/splunk_ds_filtering5.png)