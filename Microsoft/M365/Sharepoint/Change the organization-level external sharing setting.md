## Change the organization-level external sharing setting

1. Go to the [Sharing page of the new SharePoint admin center](https://admin.microsoft.com/sharepoint?page=sharing&modern=true), and sign in with an account that has admin permissions for your organization.

2. Under **External sharing**, specify your sharing level for SharePoint and OneDrive. The default level for both is "Anyone."

   - The SharePoint setting applies to all site types, including those connected to Microsoft 365 groups.

   - The OneDrive setting can be more restrictive than the SharePoint setting, but not more permissive.

   - The SharePoint external sharing setting on this page is the same as the one in the Microsoft 365 admin center, under **Settings** > **Services & add-ins** > **Sites**. These settings are also the same as those in the OneDrive admin center.

     

     ![External sharing settings](https://docs.microsoft.com/en-US/sharepoint/sharepointonline/media/externalsharing.png)

     

     **This setting is for your organization overall. Each site has its own sharing setting which you can set independently, though it must be at the same or more restrictive setting as the organization.**

