## What are Containers

- Containers are all about portable software
- The technology allows you to run the software on a variety of systems.
- This speeds up deployment, simplifies automation, and ensures the code runs consistently
- Like Virtualization, containers wrap your software in a standardized environment
- Containers are smaller, use less resource, and easier to automate the VM's
- Does not require a full copy of the OS in order to function
- Excel at managing a large number of small, independent workloads.

 

## What is Orchestration
- Orchestration refers to processes used to manage containers
- Also refers to the automation of container management.
- Rather than spin up multiple instances of a container, we can let a tool like Kubernetes handle this.
- The complex the container requirements the more useful orchestration becomes.
- Allows Zero-downtime deployment
- Allows easy management and automation of deployment, scaling and connection of micro-services.

 

## Advantages and Limitations

| Advantages | Limitations 
|------------|-------------
| Isolation & Portability | Less Flexibility
| Lightweight | Different set of challenges around orchestration
|Fast startup time | 
| Small footprint (disk size) |
| Fast & Simple automation. |

 