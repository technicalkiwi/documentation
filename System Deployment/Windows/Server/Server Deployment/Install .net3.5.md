Copy Accross SXS folder to C:\Installs\sxs
[![Install_NET.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_NET.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_NET.png)

From Server Manager Select Add Roles and Features
[![Install_NET1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_NET1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_NET1.png)

Select Role based Installation  
[![Install_NET2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_NET2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_NET2.png)

Select Server from server pool  
Select Local Server  
[![Install_NET3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_NET3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_NET3.png)

Skip the add roles Page  
[![Install_NET4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_NET4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_NET4.png)

Check the Box for .NET3.5