# Add Disk To Existing Zpool

You can dynamically add disk space to a pool by adding a new top-level virtual device. This disk space is immediately available to all datasets in the pool.

The virtual device that you add should have the same level of redundancy as the existing virtual device. However, you can change the level of redundancy by using the –f option.

To add a new virtual device to a pool, use the zpool add command.

`zpool add $poolname $Keyword $DeviceName`

**Note -  With zpool add –n, you can perform a dry run before actually adding devices.**


### Adding Disks to a Mirroed ZFS Pool
In the following example, a mirror is added to a ZFS configuration that consists of two top-level mirrored devices.

`zpool add zpool mirror c0t3d0 c1t3d0`

``` bash
$ zpool status zpool
pool: mpool
state: ONLINE
scrub: none requested
config:

NAME           STATE     READ  WRITE  CKSUM
mpool          ONLINE       0      0      0
   mirror-0    ONLINE       0      0      0
      c0t1d0   ONLINE       0      0      0
      c1t1d0   ONLINE       0      0      0
   mirror-1    ONLINE       0      0      0
      c0t2d0   ONLINE       0      0      0
      c1t2d0   ONLINE       0      0      0
   mirror-2    ONLINE       0      0      0Added mirrored device.
      c0t3d0   ONLINE       0      0      0
      c1t3d0   ONLINE       0      0      0

errors: No known data errors
```

### Adding Disks to a RAID-Z Configuration
This example shows how to add one RAID-Z device consisting of three disks to an existing RAID-Z storage pool that also contains three disks.

`zpool add zpool raidz c2t2d0 c2t3d0 c2t4d0`

``` bash
$ zpool status rzpool
pool: rzpool
state: ONLINE
scrub: none requested
config:

NAME           STATE     READ  WRITE  CKSUM
rzpool         ONLINE       0      0      0
   raidz1-0    ONLINE       0      0      0
      c1t2d0   ONLINE       0      0      0
      c1t3d0   ONLINE       0      0      0
      c1t4d0   ONLINE       0      0      0
   raidz1-1    ONLINE       0      0      0Added RAID-Z device.
      c2t2d0   ONLINE       0      0      0
      c2t3d0   ONLINE       0      0      0
      c2t4d0   ONLINE       0      0      0

errors: No known data errors
```

### Adding Cache Devices
This example shows how to add a cache device to a pool.

`zpool add system1 cache c2t5d0 c2t8d0`

``` bash
$ zpool status system1
pool: system1
state: ONLINE
scrub: none requested
config:

NAME           STATE     READ  WRITE  CKSUM
system1        ONLINE       0      0      0
   mirror-0    ONLINE       0      0      0
      c2t0d0   ONLINE       0      0      0
      c2t1d0   ONLINE       0      0      0
      c2t3d0   ONLINE       0      0      0
   cache                                   Added cache device.
      c2t5d0   ONLINE       0      0      0
      c2t8d0   ONLINE       0      0      0

errors: No known data errors

```