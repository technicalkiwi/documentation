### Restricting Sharepoint Access to Certain IP ranges

Go to the [Sharepoint Admin Center](https://briscoegroup-admin.sharepoint.com/_layouts/15/online/AdminHome.aspx#/home)  
Open Polices tab  
Select Access Control  
[![Sharepoint_restrict_access1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Sharepoint_restrict_access1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Sharepoint_restrict_access1.png)

Select Network Location
[![Sharepoint_restrict_access2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Sharepoint_restrict_access2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Sharepoint_restrict_access2.png)

Turn On Network Location Access Control  
Enter Ip Ranges  
Make Sure to include the local network IP's  
[![Sharepoint_restrict_access3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-04/scaled-1680-/Sharepoint_restrict_access3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-04/Sharepoint_restrict_access3.png)