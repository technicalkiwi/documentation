## Git configuration
 Add username with
`git config --global user.name <username>`

 Add email with
`git config --global user.email <email>`

 Set Default editor with
`sudo git config --system core.editor <nano>`

### Working With Git Locally
 Create an empty git repository with as well create the .git file
`Git init`

Displays the Status of the current repository, including
branch, untracked files, and changes to be committed.
`Git status`

 Adds the specified file to be included when you commit
`Git add <filename>`  

 Commit the current changes to the git repository
`Git commit  - will ask for message`

 Include a messages along with the commit
`Git commit -m "enter message here"`

 Display the log of changes
`Git log`

 Remove a file from the repository
`Git rm <filename>`

 Clone the Repository ( this will start a new log history) 
`Git clone < local repository> <new repository>` 
`Git clone < online repository url> <new repository>`

## See the history of a repository
Git log ( Displays relevant information)
Git log --oneline ( Displays only comment of commit)
Git log -p ( Displays all information of changes)
Git log -- filename (Displays only commits on this file)
Git log --online filename ( Shows only comments on the file)

## Ignoring Files
You can setup a .gitignore file which may contain a list of filenames or patterns which git will ignore.

 It is also possible to exclude file paths with the command

`git config --global core.excludesfile <path>`

## Working with Git remotely
 Link a remote repository to current Git location
`Git remote add origin https://github.com/<username>/Repository`

Change Remote URL
`git remote set-url origin https://github.com/<username>/Repository`

 Push local files up to repository
`Git push -u origin <branch>`

 Pull files from repository to local 
`Git pull  origin <branch>`

 

## Working With Branches in Git
 Create a new branch
`Git branch <new branch>`

 Begin to work on the new branch
`Git checkout <branch>`

 Create and checkout new branch
`Git checkout -b <branch>`

 Merge changes from a branch back into current repo
`Git merge <branch>`

 