Install on Ansible Host from the Distributions Repository (Ubuntu)

`sudo apt install ansible`
 

Install on Remote Nodes from the Distributions Repository (Ubuntu) 

`sudo apt install ansible`

## Common Errors

### Python Error
Installing Ansible should also install python however this doesn't always create the correct PATH
To check run which pythonand if it comes back blank you willl need to create the PATH

To do this create a symlink from python3 to python

`sudo ln -s /usr/bin/python3 /usr/bin/python`