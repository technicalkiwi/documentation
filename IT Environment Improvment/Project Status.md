# Project Status



### DR Documentation

Mostly Complete, Networking setup and review of System Classification Remaining



### Win 10 Upgrade





### Office 365 Software Rollout

Support office, Dc, Contact Center Complete.

Half of stores done, waiting on win10 upgrades to progress.

Will bundle this in with the upgrade.



### Office Online Setup

This is up to date but will continue to grow as our use case expands



### Migration to Exchange Online

Support office, Dc, Loss Prevention, Contact Center  Complete

Half of stores done, waiting on Win10/ 365 Rollout 







## Other Things

### PowerShell Automation

* User Management, ( Onboarding - Termination)
*  Menial but re-occurring Tasks
  * Check Till Disk Space
  * Restart Sap Sprint Service if its stopped on svr
  * Restart POS Service if stopped
  * Inventory information Gathering
  * New PC Setup - Join to domain, move to OU.
  * App folder remove from new builds
* Store Server Role Setup



### Linux Deployment

* Gather list of system that need to be updated
* Rank in order of ease/impact
* Build Custom Image.



### Linux Configuration Management.

* Learn and Setup Basic Ansible Playbooks.