Login to the [Teams Admin Center](https://admin.teams.microsoft.com/)  

### External Access  
Turned off External Access  
[![Teams_External_access.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_External_access.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_External_access.png)  

### Guest Access
Disallowed Guest Access  
[![Teams_Guest_access.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Guest_access.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Guest_access.png)

### App Access
Allowed All Microsoft Apps  
Denyed all but white listed 3rd Party Apps  
[![Teams_permission_policy.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_permission_policy.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_permission_policy.png)


### Pinned Apps Setup
Turned off Custom Apps  
Pinned the Standard MS apps to the side  
[![Teams_Setup_Policy.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_Setup_Policy.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_Setup_Policy.png)

### Messaging Policies
Left as Standard for now.
[![Teams_messaging_policies.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/Teams_messaging_policies.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/Teams_messaging_policies.png)