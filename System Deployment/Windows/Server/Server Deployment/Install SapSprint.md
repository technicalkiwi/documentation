Copy Across Installation Pacakge if needed  
Package Located here "I:\Programs Installs Folder\SAP Branch Setup\SAPSPRINT 7.3 NEW"
## Installation
Run the Latest Installtion Package  
[![SapSprint_intall1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall1.png)

Start the Installation
[![SapSprint_intall2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall2.png)

Select Install SAP Print Service  
[![SapSprint_intall3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall3.png)

Leave Installation Directory as Default  
[![SapSprint_intall4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall4.png)

Change LPD to 516  
[![SapSprint_intall5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall5.png)

Installation should complete successfully  
[![SapSprint_intall6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/SapSprint_intall6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/SapSprint_intall6.png)