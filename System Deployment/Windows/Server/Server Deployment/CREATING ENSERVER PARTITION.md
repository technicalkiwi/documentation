## CREATING ENSERVER PARTITION
Open Partition Manager  
[![Create_Enserver_share1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share1.png)

Right Click on C: and select Shrink Volume  
[![Create_Enserver_share2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share2.png)

Shrink the Volume by 5000 MB  
[![Create_Enserver_share3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share3.png)

Right click the new blank partition and select New Simple Volume  
[![Create_Enserver_share4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share4.png)

Select Next   
[![Create_Enserver_share5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share5.png)

Leave the Size as default  
[![Create_Enserver_share6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share6.png)

Assign Drive letter D  
[![Create_Enserver_share7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share7.png)

Format Volume to NTFS  
Name the Volume Enserver  
[![Create_Enserver_share8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share8.png)

Review Changes  
[![Create_Enserver_share9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share9.png)

Check the new partition has been created  
[![Create_Enserver_share10.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Create_Enserver_share10.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Create_Enserver_share10.png)

## CONFIGURING ENSERVER SHARING
Open File Explorer  
[![enserver_setup.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup.png)

Right Click on D: drive and select Properties  
[![enserver_setup2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup2.png)

Go to the Sharing tab  
Select Advanced Sharing  
[![enserver_setup3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup3.png)

Tick Share this folder
Make sure share name is enserver  
Select Permissions  
[![enserver_setup4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup4.png)

Set Everyone to have Full control  
[![enserver_setup5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup5.png)

Copy Data Across From old server  
[![enserver_setup6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup6.png)

Check you can see the data  
[![enserver_setup7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/enserver_setup7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/enserver_setup7.png)