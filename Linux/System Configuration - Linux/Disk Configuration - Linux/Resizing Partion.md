[Ubuntu: Extend your default LVM space](https://packetpushers.net/ubuntu-extend-your-default-lvm-space/)

Check for free space by running `cfdisk`
![resize_partition](../resources/resize_partition1.png)

Select the Partition to expand
Select Resize from the menu along the bottom
Confirm the new size of the partition


Select Write from the menu along the bottom to make the changes live
Select quit or hit "q" to exit cfdisk utility