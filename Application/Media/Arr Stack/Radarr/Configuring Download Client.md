Go to Radarr
Select Settings from left panel
Select Download Clients
![Sonarr_download1.png](../resources/Sonarr_download1.png)

Click on the Big plus symbol
![Sonarr_download2.png](../resources/Sonarr_download2.png)

Select Download client e.g Qbittorrent
![Sonarr_download3.png](../resources/Sonarr_download3.png)

Enter Connection settings
![Sonarr_download4.png](../resources/Sonarr_download4.png)


Test connection
![Sonarr_download5.png](../resources/Sonarr_download5.png)

If pass then save