Adding Users
You add users in ansible using the built in users module
The password must be hashed for the playbook to run.

``` yaml
  vars_files:
    - ./vars/users.yml
  tasks:
  - name: Adding User {{ item.username }}
    user:
      name: "{{ item.username}}"
      create_home: yes
      comment: "Created Via Ansible"
      password: "{{ 'Hello123' | password_hash('sha512') }}"
      update_password: on_create
      groups: TK_Admins
    with_items: "{{ users }}"
```
 

As above you can import the users to create from a yaml file

``` yaml
users:
  - username: "User1"
  - username: "User2"
  - username: "User3"
  - username: "User4"
```

Complete Playbook
``` yaml
- hosts: all
  become: yes
  vars_files:
    - ./vars/users.yml
  tasks:

  - name: Creating Admin Group
    group:
      name: TK_Admins
      state: present
  
  - name: Adding User {{ item.username }}
    user:
      name: "{{ item.username}}"
      create_home: yes
      comment: "Created Via Ansible"
      password: "{{ 'Hello123' | password_hash('sha512') }}"
      update_password: on_create
      groups: TK_Admins
    with_items: "{{ users }}"

  - name: Create SSH Folders
    file:
      path: /home/{{ item.username }}/.ssh
      owner: "{{ item.username }}"
      state: directory
    with_items: "{{ users }}"

  - name: Set Authorized_Keys
    authorized_key: 
      user: "{{ item.username }}"
      state: present
      key: "{{ lookup('file', './vars/{{ item.username }}.pub')}}"
    with_items: "{{ users }}"

  - name: Create Authorized Keys File
    file:
      path: "/home/{{ item.username }}/.ssh/Authorized_Keys"
      owner: "{{ item.username }}"
      state: touch
    with_items: "{{ users }}"
```