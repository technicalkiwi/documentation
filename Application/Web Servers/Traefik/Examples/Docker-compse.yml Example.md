``` yml
version: '3'

services:
  traefik:
    image: traefik:latest
    container_name: traefik
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    networks:
      - proxy
    ports:
      - 80:80
      - 8080:8080
      - 443:443
    environment:
      - TZ=Pacific/Auckland
      - CF_API_EMAIL=${CF_EMAIL}
      - CF_API_KEY=${CFKEY}
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./data/traefik.yml:/traefik.yml:ro
      - ./data/traefik_config:/configurations
      - ./data/config.yml:/config.yml:ro
      - ./letsencrypt:/letsencrypt
     # - ./data/acme.json:/acme.json
    #  - ./data/acme:/etc/traefik/acme
    #  - ./data/traefik_cert:/certs
      - ./data/traefik_static:/traefik_static:ro
      - ./data/traefik_logs:/logs
    command:
      - --api.insecure=true
      - --providers.docker
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.rule=Host(`traefik.tecnicalkiwi.com`)"
      #- "traefik.http.middlewares.traefik-auth.basicauth.users=tkiwi:Welcome1"
      - "traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https"
      - "traefik.http.middlewares.sslheader.headers.customrequestheaders.X-Forwarded-Proto=https"
      - "traefik.http.routers.traefik.middlewares=traefik-https-redirect"
      #- "traefik.http.services.traefik.loadbalancer.server.port=8080"
      - "traefik.docker.network=proxy-network"
      - "traefik.http.routers.traefik-secure.entrypoints=https"
      - "traefik.http.routers.traefik-secure.rule=Host(`traefik.technicalkiwi.com`)"
      - "traefik.http.routers.traefik-secure.middlewares=traefik-auth"
      - "traefik.http.routers.traefik-secure.service=api@internal"
      - "traefik.http.routers.traefik-secure.middlewares=user-auth@file"    
      - "traefik.http.routers.traefik-secure.tls=true"
      - "traefik.http.routers.traefik-secure.tls.certresolver=cloudflare"
      - "traefik.http.routers.traefik-secure.tls.domains[0].main=*.technicalkiwi.com"

networks:
  proxy:
    external:
      name: proxy-network
```

