Open the Sudoers File
sudo nano /etc/sudoers
![Passwordless Sudo](../resources/passwordless_sudo1.png)

Find the line includedir /etc/sudoers.d

Below that line add: username ALL=(ALL) NOPASSWD: ALL ,
where username is your passwordless sudo username
![Passwordless Sudo](../resources/passwordless_sudo2.png)

Save and Close the File

From now on, the user specified will be able to execute sudo any_command without providing a sudo password.
![Passwordless Sudo](../resources/passwordless_sudo3.png)