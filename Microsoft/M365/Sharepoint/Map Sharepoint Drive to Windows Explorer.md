# Map Sharepoint Drive to Windows Explorer



Open Windows Explorer

Right click on this pc

Select Map Network Drive

![Map_Sharepoint_5](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Sharepoint\resources\Map_Sharepoint_5.png)



Select a Drive Letter

Add in the link to folder

* Shared Documents

  https://briscoegroup.sharepoint.com/sites/IT/Shared%20Documents

* IT Documentation

  https://briscoegroup.sharepoint.com/sites/IT/Shared%20Documents/IT%20Documentation

Select Reconnect at sign in

![Map_Sharepoint_6](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Sharepoint\resources\Map_Sharepoint_6.png)



Click Finish

Drive should now be mapped

![Map_Sharepoint_7](C:\Users\bgraaronb\OneDrive - briscoegroup.onmicrosoft.com\Documentation\Microsoft\Office\Sharepoint\resources\Map_Sharepoint_7.png)