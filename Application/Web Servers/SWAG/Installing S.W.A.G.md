# Creating a SWAG container

Most of the initial settings for getting a webserver with ssl certs up are done through the docker run/create or compose yaml parameters. Here's a list of all the settings available including the optional ones. It is safe to remove unnecessary parameters for different scenarios.

## Docker Compose Example
``` yaml
---
version: "2.1"
services:
  swag:
    image: ghcr.io/linuxserver/swag
    container_name: swag
    cap_add:
      - NET_ADMIN
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Pacific/Auckland
      - URL=yourdomain.url
      - SUBDOMAINS=www, # Subdomains to incude in the certs
      - VALIDATION=http
      - CERTPROVIDER= #optional
      - DNSPLUGIN=cloudflare #optional
      - DUCKDNSTOKEN="token" #optional
      - EMAIL="e-mail" #optional
      - ONLY_SUBDOMAINS=false #optional
      - EXTRA_DOMAINS="extradomains" #optional
      - STAGING=false #optional
    volumes:
      - /path/to/appdata/config:/config
    ports:
      - 443:443
      - 80:80 #optional
    restart: unless-stopped
```


## Authorization method
.
**Our image currently supports three different methods to validate domain ownership:**
-   **http:**
    
    -   Let's Encrypt (acme) server connects to domain on port 80
        
    -   Can be owned domain or a dynamic dns address
        
-   **dns:**
    
    -   Let's Encrypt (acme) server connects to dns provider
        
    -   Api credentials and settings entered into `ini` files under `/config/dns-conf/`
        
    -   Supports wildcard certs
        
    -   Need to have own domain name (non-free)
        
-   **duckdns:**
    
    -   Let's Encrypt (acme) server connects to DuckDNS
        
    -   Supports wildcard certs (only for the sub-subdomains)
        
    -   No need for own domain (free)
        

The validation is performed when the container is started for the first time. Nginx won't be up until ssl certs are successfully generated.

The certs are valid for 90 days. The container will check the cert expiration status every night and if they are to expire within 30 days, it will attempt to auto-renew. If your certs are about to expire in less than 30 days, check the logs under `/config/log/letsencrypt` to see why the auto-renewals failed.

<br></br>
## Cert Provider (Let's Encrypt vs ZeroSSL)

As of January 2021, SWAG supports getting certs validated by either [Let's Encrypt](https://letsencrypt.org/ "https://letsencrypt.org/") or [ZeroSSL](https://zerossl.com/ "https://zerossl.com/"). Both services use the [ACME protocol](https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment "https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment") as the underlying method to validate ownership. Our Certbot client in the SWAG image is ACME compliant and therefore supports both services.

Although very similar, ZeroSSL does (at the time of writing) have a couple of advantages over Let's Encrypt:

-   ZeroSSL provides unlimited certs via ACME and has no rate limits or throttling (it's quite common for new users to get throttled by Let's Encrypt due to multiple unsuccessful attempts to validate)
    
-   ZeroSSL provides a web interface that allows users to list and manage the certs they have received
    

SWAG currently defaults to Let's Encrypt as the cert provider so as not to break existing installs, however users can override that behavior by setting the environment variable `CERTPROVIDER=zerossl` to retrieve a cert from ZeroSSL instead. The only gotcha is that ZeroSSL requires the `EMAIL` env var to be set so the certs can be tied to a ZeroSSL account for management over their web interface.

<br></br>
## Port forwards

Port 443 mapping is required for access through `https://domain.com`. However, you don't necessarily need to have it listen on port 443 on the host server. All that is needed is to have port 443 on the router (wan) somehow forward to port 443 inside the container, while it can go through a different port on the host.

For instance, it is ok to have port 443 on router (wan) forward to port 444 on the host, and then map port 444 to port 443 in docker run/create or compose yml.

Port 80 forwarding is required for `http` validation only. Same rule as above applies, and it's OK to go from 80 on the router to 81 on the host, mapped to 80 in the container.

<br></br>
## Docker networking

SWAG container happily runs with bridge networking. However, the default bridge network in docker does not allow containers to connect each other via container names used as dns hostnames. Therefore, it is recommended to first create a [user defined bridge network](https://docs.docker.com/network/bridge/ "https://docs.docker.com/network/bridge/") and attach the containers to that network.

If you are using docker-compose, and your services are on the same yaml, you do not need to do this, because docker-compose automatically creates a user defined bridge network and attaches each container to it as long as no other networking option is defined in their config.

For the below examples, we will use a network named `lsio`. We can create it via `docker network create lsio`. After that, any container that is created with `--net=lsio` can ping each other by container name as dns hostname.

> Keep in mind that dns hostnames are meant to be case-insensitive, however container names are case-sensitive. For container names to be used as dns hostnames in nginx, they should be all lowercase as nginx will convert them to all lowercase before trying to resolve.
