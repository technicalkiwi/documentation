# Current Projects



### DR Documentation

- [x] Process Overview Guide
- [x] Restore Process Guide
- [x] VM Networking Guide
- [x] Status Checklist



### Office 365 Software Rollout

- [x] Support Office
- [x] Distribution Center
- [x] Contact Center
- [ ] Stores



### Office Online Setup

- [x] Office Licensing Security Groups
- [x] Sharepoint Access Security Groups
- [x] Admin Access Security Groups



### Exchange Online Mailbox Migration

- [x] Support Office
- [x] Distribution Center
- [x] Loss Prevention
- [x] Contact Center
- [x] Store Managers
- [ ] Store Admins
- [ ] Store FMT



### Linux System Overhaul

- [x] Base OS Chosen
- [x] Customized Image Created
- [x] Cockpit + SSH pre-setup upon installation
- [x] Gather List of systems in need of upgrade
- [ ] Rank depending on ease / impact
- [x] List new systems that would be advantageous 



### Ansible Configuration Management

- [ ] System Configuration for BGR Environment.
- [x] User and Group Management.
- [x] Security Configuration.
- [ ] Application Installation.



