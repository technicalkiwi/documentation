# Sharing Microsoft Forms

Click on the Share Button

Under Send and Collect Responses change the permissions to Anyone can respond

Tick the box to shorten URL


Select the Copy button
Do not copy the link in the address bar.


You can now share the URL with the end users.

e.g https://forms.office.com/r/ZGLcHHZLcb