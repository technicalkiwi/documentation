## Detecting USB hard drive
After you plug in your USB device to your USB port, Linux system adds a new block device into /dev/ directory. At this stage, you are not able to use this device as the USB filesystem needs to be mouted before you can retrieve or store any data. To find out what name your block device file have you can run fdisk -l command.

NOTE:fdisk command required administrative privileges to access the required information, thus from this reason the commands needs to be executed as a root user or with sudo prefix:

``` bash
# fdisk -l 
OR
$ sudo fdisk -l
OR
$ lsblk
```
Upon executing the above command you will get an output similar to the one below:

``` bash
Disk /dev/sdc: 7.4 GiB, 7948206080 bytes, 15523840 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device     Boot Start      End  Sectors  Size Id Type
/dev/sdc1  *     8192 15523839 15515648  7.4G  b W95 FAT32
```

The above output will most likely list multiple disks attached to your system. Look for your USB drive based on its size and filesystem. Once ready, take a note of the block device name of the partition you intent to mount. For example in our case that will be /dev/sdc1 with FAT32 filesystem.

## Create mount point
Before we are able to use mount command to mount the USB partition, we need to create a mount point. Mount point can be any new or existing directory within your host filesystem. Use mkdir command to create a new mount point directory where you want to mount your USB device:

`mkdir /mnt/usb-drive`

## Mount USB Drive
At this stage we are ready to mount our USB's partition /dev/sdc1 into /media/usb-drive mount point:

`mount /dev/sdc1 /mnt/usb-drive/`

To check whether your USB drive has been mounted correctly execute mount command again without any arguments and use grep to search for USB block device name:

`mount | grep sdc1`
`/dev/sdc1 on /mnt/usb-drive type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=utf8,shortname=mixed,errors=remount-ro`

If no output has been produced by the above mount command your USB partition is not mounted. Alternatively, double-check whether you have used a correct block device name in the above command.

Accessing USB Data
If all went well, we can access our USB data simply by navigating to our previously created mount point /mnt/usb-drive:


`cd /mnt/usb-drive`

## USB Unmount
Before we are able to unmount our USB partition we need to make sure that no process is using or accessing our mount point directory, otherwise we will receive an error message similar to the one below:

``` bash
umount: /media/usb-drive: target is busy
(In some cases useful info about processes that
use the device is found by lsof(8) or fuser(1).)
```

Close your shell or navigate away from USB mount point and execute the following linux command to unmount your USB drive:


`umount /media/usb-drive`