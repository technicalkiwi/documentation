Manages web app requests
Routes traffic to a pool of webservers based on URL of request
Webservers can be VM's VM scale sets. App service or on-prem servers

![app_gateway1.png](app_gateway1.png)


Determine application gateway routing
![app_gateway2.png](app_gateway2.png)
