Go to Splunk Cluster

login
Go to Alerts
![splunk_search_query1.png](./resources/splunk_search_query1.png)

Find the alert you want to change and select open in search
![splunk_search_query2.png](./resources/splunk_search_query2.png)

Make changes to query
![splunk_search_query4.png](./resources/splunk_search_query3.png)

Change the timeframe from realtime to last 30days then run the query to ensure it’s pulling in data
![splunk_search_query4.png](./resources/splunk_search_query4.png)

If its picking up events then change timeframe back to Alltime (realtime) and save
![splunk_search_query5.png](./resources/splunk_search_query5.png)