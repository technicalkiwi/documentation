
`terraform init`
Initialize your current directory in your terminal
![Terraform Init](./resources/terraform_init.png)

`terraform fmt`
Automatically updates configurations in the current directory for readability and consistency
Format your configuration. Terraform will print out the names of the files it modified

`terraform validate`
Make sure your configuration is syntactically valid and internally consistent
![Terraform Init](./resources/terraform_validate.png)

`terraform apply`
Apply your configuration.

This output shows the execution plan and will prompt you for approval before proceeding. If anything in the plan seems incorrect or dangerous, it is safe to abort here with no changes made to your infrastructure. Type `yes` at the confirmation prompt to proceed.
![Terraform Init](./resources/terraform_apply.png)

When you apply your configuration, Terraform writes data into a file called `terraform.tfstate`. This file contains the IDs and properties of the resources Terraform created so that it can manage or destroy those resources going forward. Your state file contains all of the data in your configuration and could also contain sensitive values in plaintext, so do not share it or check it in to source control.

`terraform show`
Inspect the current state
![Terraform Init](./resources/terraform_show.png)

`terraform output`
See the values of the outputs defined in outputs.tf
![Terraform output](./resources/terraform_output.png)


`terraform state`
See a full list of available commands to view and manipulate the configuration's state

`terraform state list`
See a list of the resources you created with Terraform 


`terraform destroy`
Destroy all infrastructure managed by Terraform
![Terraform Init](./resources/terraform_destroy.png)
Will then output the results of the command
![Terraform Init](./resources/terraform_destroy1.png)