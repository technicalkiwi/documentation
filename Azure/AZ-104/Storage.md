Configure storage accounts
Configure blob storage
Configure storage security
Configure Azure Files and file sync


#### Storage Services
![[storage1.png]]

Storage Account Kinds
![[storage2.png]]

Replication Stratagies
![[storage3.png]]


Locally Redundant Storage ( LRS )
Zone Redundant Storage ( ZRS )
Geo Redundant Storage ( GRS )
Geo Redundant Storage with Read access to Secondary ( RA-GRS )
Geo-zone Redundant Storage ( GZRS )
Geo-zone Redundant Storage with Read access to Secondary  ( RA-GZRS )

## Securing Storage Endpoints


## Blob Storage
![[storage4.png]]

Blob storage replication 
![[storage5.png]]


## Storage Security

Storage security strategies
![[storage6.png]]

Shared Access Signatures
![[storage7.png]]

URI and SAS Parameters
![[storage8.png]]


Storage Service Encryption
![[storage9.png]]


Customer Managed Keys
![[storage10.png]]


## Storage Best Practices
![[storage11.png]]



## Azure Files and File Sync

File share vs Blob data
![[storage12.png]]

![[storage13.png]]

Snapshots
![[storage14.png]]

Soft Delete options
![[storage15.png]]


### Storage Explorer
![[storage16.png]]

