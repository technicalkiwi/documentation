Make Sure Virtual Machine is Off  
Highlight the VM you want to edit  
Select Settings under the VM's heading
[![SVR_VM_Config1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config1.png)

Select Processor Tab  
Set Number of Virtual Processors to 4  
[![SVR_VM_Config2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config2.png)

Select Network Adapter Tab  
Connect VM to a Virtual Switch  
[![SVR_VM_Config3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config3.png)

Select Intergration Services Tab  
Deselect the Option for Time Synchronization  
[![SVR_VM_Config6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config6.png)

Select Automatic Start Action Tab  
Set to Always Start Machine Automatically  
Set Startup Delay to 60 Seconeds  
[![SVR_VM_Config4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config4.png)

Select Automatic Stop Action Tab  
Set to Shut down the Guest Operating System  
[![SVR_VM_Config5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-03/scaled-1680-/SVR_VM_Config5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-03/SVR_VM_Config5.png)