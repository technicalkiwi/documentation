
## Template




## Terraform or Manual
Set hostname
Configure Networking
* Set IP
* Interface name
* Set Gateway




## Ansible

Add routes to networks via BDC_Orchestration
disable ivp6
Add http proxies
Register with Red Hat Satellite

``` bash
ip route add 10.50.50.0/24 via 172.20.36.1 dev ens224
ip route add 10.51.51.0/24 via 172.20.36.1 dev ens224
ip route add 10.52.52.0/24 via 172.20.36.1 dev ens224
ip route add 10.60.60.0/24 via 172.20.36.1 dev ens224
ip route add 10.61.61.0/24 via 172.20.36.1 dev ens224
ip route add 10.62.62.0/24 via 172.20.36.1 dev ens224
ip route add 10.100.0.0/24 via 172.20.36.1 dev ens224
ip route add 10.100.1.0/24 via 172.20.36.1 dev ens224
ip route add 172.20.20.0/24 via 172.20.36.1 dev ens224

```




Install Crowd Strike and Rapid 7









