## Image Tags
The full name of a container image is composed of several parts. For example, you can decompose
 the registry.access.redhat.com/ubi9/nginx-120:1-86 image name into the following
 elements:
 
 • The registry server is registry.access.redhat.com.
 • The namespace is ubi9.
 • The name is nginx-120. In this example, the name of the image includes the version of the
 software, Nginx version 1.20.
 • The tag, which points to a specific version of the image, is 1-86. If you omit the tag, then most
 container tools use the latest tag by default.

### Floating Tag Issues
 Vendors, organizations, and developers who publish images manage their tags and establish their
 own lifecycle for floating tags. They can reassign a floating tag to a new image version without
 notice.
 
 As a user of the image, you might not notice that the tag that you were using now points to a
 different image version.
 
 Suppose that you deploy an application on OpenShift and use the latest tag for the image. The
 following series of events might occur:
 1. When OpenShift deploys the container, it pulls the image with the latest tag from the container registry.
 2. Later, the image developer pushes a new version of the image, and reassigns the latest tagto that new version.
 3. OpenShift relocates the pod to a different cluster node, for example because the original node fails.
 4. On that new node, OpenShift pulls the image with the latest tag, and thereby retrieves the new image version.
 5. Now the OpenShift deployment runs with a new version of the application, without your awareness of that version update


## Selecting a Pull Policy

 When you deploy an application, OpenShift selects a compute node to run the pod. On that node,
OpenShift pulls the image and then starts the container.
 
By setting the imagePullPolicy attribute in the deployment resource, you can control how OpenShift pulls the image.

``` yaml
apiVersion: apps/v1
 kind: Deployment
 ...output omitted...
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: myapp
    spec:
      containers:
      - image: registry.access.redhat.com/ubi9/nginx-120:1-86
 imagePullPolicy: IfNotPresent
        name: nginx-120
```

The imagePullPolicy attribute can take the following values

##### IfNotPresent
 If the image is already on the compute node, because another container is using it or because
 OpenShift pulled the image during a preceding pod run, then OpenShift uses that local image.
 Otherwise, OpenShift pulls the image from the container registry.

If you use a floating tag in your deployment, and the image with that tag is already on the
node, then OpenShift does not pull the image again, even if the floating tag might point to a
newer image in the source container registry.

##### Always
OpenShift always verifies whether an updated version of the image is available on the source
container registry. To do so, OpenShift retrieves the SHA ID of the image from the registry. If a
local image with that same SHA ID is already on the compute node, then OpenShift uses that
image. Otherwise, OpenShift pulls the image

If you use a floating tag in your deployment, and an image with that tag is already on the node,
 then OpenShift queries the registry anyway to ensure that the tag still points to the same
 image version. However, if the developer pushed a new version of the image and updated the
 floating tag, then OpenShift retrieves that new image version.
 
 OpenShift sets the imagePullPolicy attribute to Always by default when you use the
 latest tag, or when you do not specify a tag.

##### Never
OpenShift does not pull the image, and expects the image to be already available on the
node. Otherwise, the deployment fails.

To use this option, you must prepopulate your compute nodes with the images that you plan
to use. You use this mechanism to improve speed or to avoid relying on a container registry for
these images



## Pruning Images from Cluster Nodes

When OpenShift deletes a pod from a compute node, it does not remove the associated image.
OpenShift can reuse the images without having to pull them again from the remote registry.
 
Because the images consume disk space on the compute nodes, OpenShift needs to remove, or
prune, the unused images when disk space becomes sparse. The kubelet process, which runs on
the compute nodes, includes a garbage collector that runs every five minutes. If the usage of the
file system that stores the images is above 85%, then the garbage collector removes the oldest
unused images. Garbage collection stops when the file system usage drops below 80%.


From a compute node, you can run the `crictl imagefsinfo` command to retrieve the name of
the file system that stores the images:

``` bash
oc debug node/node-name
chroot /host
crictl imagefsinfo
```

 From the compute node, you can use the `crictl rmi` to remove an unused image. However,
 pruning objects by using the crictl command might interfere with the garbage collector and the
 kubelet process.
 
 It is recommended that you rely on the garbage collector to prune unused objects, images, and
 containers from the compute nodes. The garbage collector is configurable to better fulfill custom
 needs that you might have.