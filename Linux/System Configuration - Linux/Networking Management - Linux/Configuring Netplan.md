# Using Netplan
## Netplan Overview
Netplan uses YAML description files to configure network interfaces and, from those descriptions, will generate the necessary configuration options for any given renderer tool.
Examples are available at [Netplan](https://netplan.io/examples/)

You will find the configuration files for Netplan in the /etc/netplan directory
`cd /etc/netplan`

Once in that directory, you will probably only see a single file:
`01-netcfg.yaml`

Should you have more than one interface, you could name the second .yaml configuration file 02-netcfg.yaml.

Netplan will apply the configuration files in numerical order, so 01 will be applied before 02. Create as many configuration files as needed for your server.


## Netplan Yaml Entries
- ETHERNETS is the Header for the Ethernet Ports.
- ENS5 is the actual device name to be configured. ( This will change depending on the NIC)
- DHCP4: Is a yes/no option to enable or disable dhcp4.
- ADDRESSES: Is the IP address and NETMASK for the device.
- ROUTES is the IPV4 address for your gateway.
- NAMESERVERS Sets DNS servers and search domains, for manual address configuration
  - ADDRESSES is the comma-separated list of DNS nameservers.
  - SEARCH is a list of search domains.


## Configuring a Static IP Address
To set a Static IP with Netplan, the configuration file would look something like this:

Example

```yml
network:
  version: 2
  renderer: networkd
  ethernets:
    ens5:
      dhcp4: no
      addresses: [192.168.1.230/24]
      routes:
        - to: default
          via: 192.168.1.1
      nameservers:
        addresses: [8.8.4.4,8.8.8.8]
        search: [lab, home]
```

Save and close that file. Test the file with:

`sudo netplan try`

The above command will validate the configuration before applying it. If it succeeds, you will see Configuration accepted.
You can then try to connect to the new IP configuration

If all goes well you can then apply the changes.

`sudo netplan apply`
 
## Configuring DHCP
To use DHCP with Netplan, the configuration file would look something like this:

``` yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    ens5:
      Addresses: []
      dhcp4: true
      optional: true
```
Save and close that file. Test the file with:

`sudo netplan try`

The above command will validate the configuration before applying it. 
If it succeeds, you will see Configuration accepted.

You can then apply the changes

`sudo netplan apply`


## Mutliple IPs

Change the addresses section to be an array
Set each line to a seperate IP addresses

```yaml 
      dhcp4: false
      addresses:
        - 192.168.1.202/24
        - 192.168.1.203/24
      routes:
        - to: default
          via: 192.168.1.1
```