#### **What is grep?**

Its full name, **global regular expression print**, obscures its simple yet powerful purpose: to "search a file for a pattern"

  
It can be used by itself  `grep Error /var/log/messages`  
Or as part of a pipe  `find .  -name *0.txt | grep taxes`

### Regular Expressions

 You search for matches at the beginning of the line with ^  
e.g `grep '^lin'  file.txt` will search for any lines in file.txt starting with "lin"

 You search for matches at the end of the line with $  
e.g `grep 'my$'  file.txt` will search for any lines in file.txt ending with "my"

To turn of the special meaning of the next character you can use \  
e.g `grep '^\$10'` will search for any lines starting with "$10"

To match more than the one character we use [  ]  
e.g `grep '^[*%]'` will search for any lines starting with either * OR %

To find lines not starting with certain characters we use [^ ]  
e.g `grep '^[^*%]'` will search for any lines NOT starting with either * OR %

### Quick List

**Command**

`Grep` : Search for matching pattern within files/output

`Grep -I` : Makes the search case insensitive

`Grep -r` : Search recursively through directory structure

`Grep -v` : Search for all instances where there is no pattern match

`Grep -w` : Search for the exact word not any pattern match

`Grep '^text'`` : Search for any lines beginning with "text"

`Grep 'text$'`` : Search for any lines ending with "text"