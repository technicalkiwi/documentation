
### Resource Groups


``` terraform
resource "azurerm_resource_group" "alias" {
  name     = "example"
  location = "West Europe"
}
```
#### **Arguments Reference**
* resource - (Required) Specifies the type of resource, followed by an terraform specific alias
* location - (Required) The Azure Region where the Resource Group should exist. Changing this forces a new Resource Group to be created.
* name - (Required) The Name which will be used for this Resource Group within Azure. Changing this forces a new Resource Group to be created.
* managed_by - (Optional) The ID of the resource or application that manages this Resource Group.
* tags - (Optional) A mapping of tags which should be assigned to the Resource Group.
#### **Attributes Reference**
In addition to the Arguments listed above - the following Attributes are exported:
* id - The ID of the Resource Group.