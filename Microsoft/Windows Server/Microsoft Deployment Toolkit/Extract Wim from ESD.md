Download lastest ISO from MS using the Media Creation Tool
Mount the Iso

Open CMD as an Admin
Change directory into the mounted ISO
Change directory into sources folder

Run the below command to list the Operating Systems contained within the ESD File
`dism /Get-WimInfo /WimFile:install.esd`

Read through the list and take note of the version you to extract
![./resources/list_OS.png](./resources/list_OS.png)


Export the specified OS to a Wim with the below command
`dism /export-image /SourceImageFile:install.esd /SourceIndex:6 /DestinationImageFile:install.wim /Compress:max /CheckIntegrity`

Replace the SourceIndex number with the number the matches the Version
Change DestinationImageFile: to the location the .wim will be saved in

![./resources/list_OS.png](./resources/export_image.png)

