## INSTALLING EEPOS
Run Setup  
[![Install_EEPOS1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS1.png)

Continue on with SQL 2014  
[![Install_EEPOS2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS2.png)

Select next on install shield   
[![Install_EEPOS3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS3.png)

Leave Install Location as Default  
[![Install_EEPOS4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS4.png)

Begin Installation  
[![Install_EEPOS5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS5.png)

Wait while EEPOS is Installed   
[![Install_EEPOS6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS6.png)

Check Installation Succeeded  
Select Finish  
[![Install_EEPOS7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Install_EEPOS7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Install_EEPOS7.png)

## CONFIGURING EEPOS
The Pos Configuration should automatically open after installation  

Begin Configuration  
[![Configure_EEPOS1.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS1.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS1.png)

Selcect Install and Configure  
[![Configure_EEPOS2.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS2.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS2.png)

Enter Branch and Till code ( 99 for Servers)
[![Configure_EEPOS3.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS3.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS3.png)

Leave User Group as default  
[![Configure_EEPOS4.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS4.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS4.png)

Select install SQL Express Edition  
Install Latin1_General_CI_AS
[![Configure_EEPOS5.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS5.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS5.png)

Leave Database as Default
[![Configure_EEPOS6.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS6.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS6.png)

Enter Site Key, Available from keypass   
Enter Replication Source SQL Server Instance Name, bgr-pos-ee  
Enter Replication Souce Database Name,  REBEL or BRISCOES  
[![Configure_EEPOS7.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS7.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS7.png)
Review Installation  
[![Configure_EEPOS8.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS8.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS8.png)

Installtion Should begin
[![Configure_EEPOS9.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS9.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS9.png)


sss

ss  
ss
Select Drop down and change to most recent Version  
Start Update.
[![Configure_EEPOS13.png](http://192.168.10.42:9015/uploads/images/gallery/2020-05/scaled-1680-/Configure_EEPOS13.png)](http://192.168.10.42:9015/uploads/images/gallery/2020-05/Configure_EEPOS13.png)