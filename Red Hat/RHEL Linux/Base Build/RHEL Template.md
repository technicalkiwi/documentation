
OS: RHEL 9
CPU: 2
RAM: 8 GB
Disk: 20 GB

Users created
* root
	* Using common pass will need to be changed by ansible
*  ansible 
	* SSH pub key added

Packages installed
* vmware tools

Added routes to jumphost and apollo networks
*  `nmcli con modify ens224 +ipv4.routes "10.60.60.0/24 172.20.36.1"`
*  `nmcli con modify ens224 +ipv4.routes "10.62.62.0/24 172.20.36.1"`

Disable IPV6
* `nmcli connection modify Example ipv6.method "disabled"`

Delete SSH Fingerprints
* `rm /etc/ssh/ssh_h*`

