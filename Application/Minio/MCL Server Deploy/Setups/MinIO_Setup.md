**Links**
* [Deploy MinIO: Multi-Node Multi-Drive — MinIO Object Storage for Linux](https://min.io/docs/minio/linux/operations/install-deploy-manage/deploy-minio-multi-node-multi-drive.html#minio-mnmd)
* [Configure NGINX Proxy for MinIO Server — MinIO Object Storage for Linux](https://min.io/docs/minio/linux/integrations/setup-nginx-proxy-with-minio.html#integrations-nginx-proxy) 


## VM Setup
2x disks
1 for OS
1 for Data  - Individual mount points

### Prerequisite Setup
Set Hostname
sudo hostnamectl set-hostname MCL-PRD-MIN01



Label Disks
 ``` bash
mkfs.xfs /dev/sdb -L DISK1
```

Create Directory for MinIO to store its data
`sudo mkdir /minio`

Add Disks to mount points in `/etc/fstab`
``` bash
  # <file system>  <mount point>  <type>  <options>         <dump>  <pass>
  LABEL=DISK1      /minio     xfs     defaults,noatime  0       2
```


**Create minio-user group and user**
``` bash
sudo groupadd -r minio-user
sudo useradd -m -r -g minio-user minio-user
```

**Give ownership of the data directorya to the MinIO user and group**
```bash
sudo chown -R minio-user:minio-user /minio
```

**Create DNS Records for each node**

* min01.connectme365.com  
* min02.connectme365.com 
* min03.connectme365.com 

> [!DNS Records]
> MinIO _requires_ using expansion notation `{x...y}` to denote a sequential series of MinIO hosts when creating a server pool. MinIO therefore _requires_ using sequentially-numbered hostnames to represent each [`minio server`](https://min.io/docs/minio/linux/reference/minio-server/minio-server.html#command-minio.server "minio.server") process in the deployment.
> 
> Create the necessary DNS hostname mappings _prior_ to starting this procedure.
> You can specify the entire range of hostnames using the expansion notation `minio{1...4}.example.com`.

**Time Synchronization**

Multi-node systems must maintain synchronized time and date to maintain stable internode operations and interactions. Make sure all nodes sync to the same time server regularly.



## Minio Setup
**Download mino Deb from source to /tmp**

``` bash
cd /tmp
wget https://dl.min.io/server/minio/release/linux-amd64/minio_20230907020502.0.0_amd64.deb 
```


**dpkg deb of minio**
``` bash
sudo dpkg -i minio_*.deb
```

**Install Minio Client**
``` bash
cd /tmp
sudo wget https://dl.min.io/client/mc/release/linux-amd64/mc
sudo chmod +x mc
sudo mv mc /usr/local/bin/mc
```


**Create and open MinIO’s environment file**
`sudo nano /etc/default/minio`

``` bash
MINIO_VOLUMES="https://min0{1...3}.connectme365.com:9000/minio/disk{1...4}/minio"
MINIO_OPTS="--console-address :9001"
MINIO_ROOT_USER=minioadmin
MINIO_ROOT_PASSWORD=Bmc2wyk1iVJQMZycKFBzdM99sFC53P5bANnT8nWwtJBhRu9U6
MINIO_SERVER_URL="https://minio.connectme365.com:9000"

```

**Create exception in firewll**
`sudo ufw allow 9000:9001/tcp`


**Create or Move Certs for HTTPS**
MinIO enables (TLS) 1.2+ automatically upon detecting a valid x.509 certificate (`.crt`) and private key (`.key`) in the MinIO `${HOME}/.minio/certs` directory.

Make directory for certs
``` bash
sudo mkdir /home/minio-user/.minio
sudo mkdir /home/minio-user/.minio/certs
```

Place TLS certificates into /home/minio-user/.minio/certs
`cp xxx/cert.crt  /home/minio-user/.minio/certs
`cp xxx/privkey.key  /home/minio-user/.minio/certs`

Own cert directory as minio-user
``` bash
sudo chown -R minio-user:minio-user /home/minio-user/.minio 
```


> [!Custom Cert location]
> Alternatively, specify a custom certificate directory using `--certs-dir` argument
> Modify the `MINIO_OPTS` variable in `/etc/defaults/minio` to set this option.
> `MINIO_OPTS="--console-address :9001 --certs-dir /opt/minio/certs"`


**Start Service**
`sudo systemctl start minio`



## Setup Load Balancer

Install Nginx
`sudo apt install nginx`

Setup load balancer site in /etc/nginx/sites-available

``` nginx
``` nginx
upstream minio_console {
   least_conn;
   server min01.connectme365.com:9001;
   server min02.connectme365.com:9001;
}
  
upstream minio_s3 {
   least_conn;
   server min01.connectme365.com:9000;
   server min02.connectme365.com:9000;
}

server {
   listen       80;
   listen  [::]:80;
   server_name  minio.connectme365.com;

   # Allow special characters in headers
   ignore_invalid_headers off;
   # Allow any size file to be uploaded.
   # Set to a value such as 1000m; to restrict file size to a specific value
   client_max_body_size 0;
   # Disable buffering
   proxy_buffering off;
   proxy_request_buffering off;

   location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;

      proxy_connect_timeout 300;
      # Default is HTTP/1, keepalive is only enabled in HTTP/1.1
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      chunked_transfer_encoding off;

      proxy_pass https://minio_s3; # This uses the upstream directive definition to load balance

   }

   location /minio/ui/ {
      rewrite ^/minio/ui/(.*) /$1 break;
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-NginX-Proxy true;

      # This is necessary to pass the correct IP to be hashed
      real_ip_header X-Real-IP;

      proxy_connect_timeout 300;
      # To support websockets in MinIO versions released after January 2023

      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";

      chunked_transfer_encoding off;


      proxy_pass https://minio_console; # This uses the upstream directive definition to load balance
   }
}

```

Link to /etc/nginx/sites-enabled
`sudo ln -s /etc/nginx/sites-available/minio.connectme365.com /etc/nginx/sites-enabled/`

Test 
`sudo nginx -t`

If passes reload Nginx
`sudo systemctl reload nginx`

Should now be reachable at minio.connectme365.com