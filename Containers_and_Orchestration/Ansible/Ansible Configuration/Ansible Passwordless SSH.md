On the Ansible Host create a new SSH Key

`ssh-keygen -t rsa`

Connect to the remote host(s) you want to login to from the Ansible Host using passwordless ssh. and create a .ssh folder under the users home folder

`ssh Ansible@RemoteNode mkdir -p .ssh`

Copy the public key into the Authorized_keys file of the Ansible User on the Remote Nodes
Check Access rights on Authorized_keys file is set to 640