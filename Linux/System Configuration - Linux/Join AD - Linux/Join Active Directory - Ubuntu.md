
### Install Required Packages
Install the required packages to connect to Active Directory

``` bash
sudo apt -y install realmd sssd sssd-tools libnss-sss libpam-sss adcli samba-common-bin oddjob oddjob-mkhomedir packagekit krb5-user
```

### Configure Networking
Change netplan to use the AD server as its name server
`sudo nano /etc/netplan/01-network-manager.yml`

Edit nameserver to point to the IP of the Domain controller

<pre>
# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: NetworkManager
  ethernets:
    wlp1s0:
      nameservers:
        addresses: [10.3.30.1]
</pre>

### Check Domain is Reachable
Check if the computer can reach the domain controller
run 
``` bash
realm discover technicalkiwi.com
```

```
You should get an output similar to below
```
```
technicalkiwi.com
  type: kerberos
  realm-name: technicalkiwi.com
  domain-name: technicalkiwi.com
  configured: no
  server-software: active-directory
  client-software: sssd
  required-package: sssd-tools
  required-package: sssd
  required-package: libnss-sss
  required-package: libpam-sss
  required-package: adcli
  required-package: samba-common-bin
  ```

### Join the Domain
Join the computer to the domain
Run `sudo realm join domain-name -U username`

### Specifying account location with `realm join`

By default, the `realm join` command creates a machine account that is located in the Computers OU


To specify where to create the account, use the **--computer-ou** flag to provide the path for the **realm join** command.

The username@domain-name account must have the permissions that are required to create accounts in the specified OU. 
By default, members of the **Domain Admins** group have this permission. 

`realm join domain-name --computer-ou="OU=org-unit,DC=machine,DC=mid-level,DC=extension" -U username@domain-name`

### Test Authorization in Domain



## Removing a Linux VM from a domain

To remove a Linux VM from the domain-name domain, run the following command. 
`realm leave domain-name -U username@domain-name`



## SSSD Configuration

The _realm_ tool already took care of creating an sssd configuration, adding the pam and nss modules, and starting the necessary services.

Let’s take a look at `/etc/sssd/sssd.conf`:
>[!TIP]
This file must have permissions _0600_ and ownership _root:root_, or else sssd won’t start!

<pre>
# /etc/sssd/sssd.conf
[sssd]
domains = technicalkiwi.com
config_file_version = 2
services = nss, pam

[domain/technicalkiwi.com]
default_shell = /bin/bash
override_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = technicalkiwi.com
realmd_tags = manages-system joined-with-adcli
id_provider = ad
fallback_homedir = /home/%u
override_homedir = /home/%u
ad_domain = technicalkiwi.com
use_fully_qualified_names = False
ldap_id_mapping = True
access_provider = ad
ad_gpo_access_control = permissive
ad_gpo_ignore_unreadable = True
#access_provider = simple
</pre>


- **cache_credentials**: this allows logins when the AD server is unreachable
- **home directory**: it’s by default `/home/<user>@<domain>`. To have the home directory created with just username change this to ``/home/%u`
- **use_fully_qualified_names**:  To login with just username set this to false.
  This should only be changed to False if you are certain no other domains will ever join the AD forest.
- **ad_gpo_access_control**:  If experiencing login issues set this to permissive
  This will make SSSD try to perform GPO operations, but not fail if it can't


## Automatic home directory creation

What the `realm` tool didn’t do for us is setup `pam_mkhomedir`, so that network users can get a home directory when they login. This remaining step can be done by running the following command:
``` bash
sudo pam-auth-update --enable mkhomedir
```


## Checks

You should now be able to fetch information about AD users. In this example, _John Smith_ is an AD user:

``` shell
getent passwd $user
```

Let’s see their groups:
``` shell
$ groups $user
```


## Login

``` shell
$ sudo login
ad-client login: $user
Password: 
Welcome to Ubuntu 20.04 LTS (GNU/Linux 5.4.0-24-generic x86_64)
...
Creating directory '/home/$user'.
$user@ad-client:~$ 
```

Notice how the home directory was automatically created.

You can also use ssh
``` shell
ssh $user$@$ipaddress
```


# Add AD Groups to Sudoers file

Open the sudoers file `/etc/sudoers`
Add in the group that should have sudo permissions

Should be in the format
%GROUPNAME PERMISSIONS

If the group name contains spaces make sure to include the escape character 

e.g Adding in domain admins and linux_root groups with full sudo permissions
<pre>
# User privilege specification
root    ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
%Domain\ Admins ALL=(ALL:ALL) ALL
%linux_root ALL=(ALL:ALL) ALL
</pre>

![Sudoers File](./resources/sudoers.png)
