## New Standalone Lifecycle

Login to Satellite Server

Go to Content > Lifecycle > Lifecycle Environments

![add_lifecycle1.png](./resources/add_lifecycle1.png)


Select Create Environment Path
![add_lifecycle2.png](./resources/add_lifecycle2.png)

Fill in the Environment information
![add_lifecycle3.png](./resources/add_lifecycle3.png)

## Add to Existing Lifecycle path

Login to Satellite Server

Go to Content > Lifecycle > Lifecycle Environments

![add_lifecycle1.png](./resources/add_lifecycle1.png)


Select Create Environment Path
![add_lifecycle2.png](./resources/add_lifecycle2.png)

Fill in the Environment information
![add_lifecycle3.png](./resources/add_lifecycle3.png)
