# Sharing ZFS Pool

## SMB Share
Install SMB 

`sudo apt install samba`

Add your user to the shared users

`sudo smbpasswd -a $User`

Set the new password for the share

## Enable SMB Share

Share the data set

`sudo zfs set sharesmb=on zpool/media`
