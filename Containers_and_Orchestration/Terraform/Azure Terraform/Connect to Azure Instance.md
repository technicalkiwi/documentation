Install Azure CLI
[How to install the Azure CLI | Microsoft Learn](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)

Open terminal
Run Az login > login using MCL account

Validate login
run az account show
``` powershell
az account show
{
  "environmentName": "AzureCloud",
  "homeTenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
  "id": "b248b36c-9414-42f8-8a1d-fc85af79ffcc",
  "isDefault": true,
  "managedByTenants": [],
  "name": "Pay-As-You-Go",
  "state": "Enabled",
  "tenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
  "user": {
    "name": "Aaron.Buchan@mclarens.co.nz",
    "type": "user"
  }
}
```


List Subscriptions available 
`az account list`

``` powershell

 {
    "cloudName": "AzureCloud",
    "homeTenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "id": "b248b36c-9414-42f8-8a1d-fc85af79ffcc",
    "isDefault": true,
    "managedByTenants": [],
    "name": "Pay-As-You-Go",
    "state": "Enabled",
    "tenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "user": {
      "name": "Aaron.Buchan@mclarens.co.nz",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "id": "eb0002e4-a6f6-463b-974f-2ec845cccd1c",
    "isDefault": false,
    "managedByTenants": [],
    "name": "Access to Azure Active Directory",
    "state": "Enabled",
    "tenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "user": {
      "name": "Aaron.Buchan@mclarens.co.nz",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "id": "e8080c2c-9958-4fcc-a43c-502ebcd14737",
    "isDefault": false,
    "managedByTenants": [],
    "name": "McLarens Group (NZ) LTD",
    "state": "Enabled",
    "tenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "user": {
      "name": "Aaron.Buchan@mclarens.co.nz",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "id": "4958aba6-0284-41a0-9c73-b9bd7eb01b5f",
    "isDefault": false,
    "managedByTenants": [],
    "name": "Azure subscription 1",
    "state": "Enabled",
    "tenantId": "bb2deee9-47ee-47de-9363-964c4daaf0ba",
    "user": {
      "name": "Aaron.Buchan@mclarens.co.nz",
      "type": "user"
    }
  }
]
```


Set the subscription to use with the `id` field being the `subscription_id` field referenced below
```bash
az account set --subscription="SUBSCRIPTION_ID"
```