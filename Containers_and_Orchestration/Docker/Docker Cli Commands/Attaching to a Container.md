# Connect to Container
There is a docker exec command that can be used to connect to a container that is already running.

Use `docker ps` to get the name of the existing container
<pre>
CONTAINER ID   IMAGE                 COMMAND        CREATED          STATUS         PORTS                                       NAMES
81931e95bb0f   portainer/portainer   "/portainer"   22 minutes ago   Up 4 minutes   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp   portainer-svr02
</pre>

Use the command `docker exec -it <container name> /bin/bash` to get a bash shell in the container



Generically, `use docker exec -it <container name> <command>` to execute whatever command you specify in the container.