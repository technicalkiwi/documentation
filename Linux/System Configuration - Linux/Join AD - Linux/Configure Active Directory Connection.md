

## SSSD Configuration

The _realm_ tool already took care of creating an sssd configuration, adding the pam and nss modules, and starting the necessary services.

Let’s take a look at `/etc/sssd/sssd.conf`:
>[!TIP]
This file must have permissions _0600_ and ownership _root:root_, or else sssd won’t start!

<pre>
# /etc/sssd/sssd.conf
[sssd]
domains = technicalkiwi.com
config_file_version = 2
services = nss, pam

[domain/technicalkiwi.com]
default_shell = /bin/bash
override_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = technicalkiwi.com
realmd_tags = manages-system joined-with-adcli
id_provider = ad
fallback_homedir = /home/%u
override_homedir = /home/%u
ad_domain = technicalkiwi.com
use_fully_qualified_names = False
ldap_id_mapping = True
access_provider = ad
ad_gpo_access_control = permissive
ad_gpo_ignore_unreadable = True
#access_provider = simple
</pre>


- **cache_credentials**: this allows logins when the AD server is unreachable
- **home directory**: it’s by default `/home/<user>@<domain>`. To have the home directory created with just username change this to ``/home/%u`
- **use_fully_qualified_names**:  To login with just username set this to false.
  This should only be changed to Falseif you are certain no other domains will ever join the AD forest.
- **ad_gpo_access_control**:  If experiencing login issues set this to permissive
  This will make SSSD try to perform GPO operations, but not fail if it can't


## Automatic home directory creation

What the `realm` tool didn’t do for us is setup `pam_mkhomedir`, so that network users can get a home directory when they login. This remaining step can be done by running the following command:
``` bash
sudo pam-auth-update --enable mkhomedir
```


## Checks

You should now be able to fetch information about AD users. In this example, _John Smith_ is an AD user:

``` shell
getent passwd $user
```

Let’s see their groups:
``` shell
$ groups $user
```


## Login

``` shell
$ sudo login
ad-client login: $user
Password: 
Welcome to Ubuntu 20.04 LTS (GNU/Linux 5.4.0-24-generic x86_64)
...
Creating directory '/home/$user'.
$user@ad-client:~$ 
```

Notice how the home directory was automatically created.

You can also use ssh
``` shell
ssh $user$@$ipaddress
```


# Add AD Groups to Sudoers file

Open the sudoers file `/etc/sudoers`
Add in the group that should have sudo permissions

Should be in the format
%GROUPNAME PERMISSIONS

If the group name contains spaces make sure to include the escape character 

e.g Adding in domian admins and linux_root groups with full sudo permissions
<pre>
# User privilege specification
root    ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
%Domain\ Admins ALL=(ALL:ALL) ALL
%linux_root ALL=(ALL:ALL) ALL
</pre>

![Sudoers File](./resources/sudoers.png)
