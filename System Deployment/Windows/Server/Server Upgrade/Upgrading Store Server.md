### Prerequisites 

#### CAM01
* [VM Setup on Cam01](http://192.168.10.42:9015/books/windows-server-deployment/page/creating-a-virtual-machine)

#### SVR
* Program installers copied Across
  * Symantec
  * Sap Sprint
  * Magenta POS
* [Install .net 3.5](http://192.168.10.42:9015/books/windows-server-deployment/page/install-net35)
* [Install Print Services Roles](http://192.168.10.42:9015/books/windows-server-deployment/page/add-role-for-print-print-services)
* [Import Printers from Existing Server](http://192.168.10.42:9015/books/windows-server-deployment/page/migrating-printers-ed3)
* [Install SapSprint](http://192.168.10.42:9015/books/windows-server-deployment/page/installing-sapsprint)
* [Create D: Volume and apply Enserver Share](http://192.168.10.42:9015/books/windows-server-deployment/page/create-enserver-share)
* License Server
* Install Symantec (Reboot)


### Procedure

#### Non-Server
* Stop EEEPOS on All Machines in store

#### Old Server
* Copy accross D: Drive contents to new server
* Prep Old server
  * [Demote from Domain Controller](http://192.168.10.42:9015/books/windows-server-deployment/page/demote-domain-controller#bkmrk-demotion)
  * [Remove AD Role](http://192.168.10.42:9015/books/windows-server-deployment/page/demote-domain-controller#bkmrk-role-removal)
  * Rename To brand-store-svrc
* [Let Changes propagate, check on FP03](http://192.168.10.42:9015/books/windows-server-deployment/page/demote-domain-controller#bkmrk-check-operation-was-) 
* Set New Ip  
* Shutdown and Turn off Auto Start on Host

#### New Server
* Set IP to Store Svr Address
* Install DNS and DHCP Roles
* [Install Active Directory Domain Services](http://192.168.10.42:9015/books/windows-server-deployment/page/install-and-configure-active-directory-domain-services)
  * [Promote to Domain Controller](http://192.168.10.42:9015/books/windows-server-deployment/page/install-and-configure-active-directory-domain-services#bkmrk-configuring-active-d)
* [Configure Symantec](http://192.168.10.42:9015/books/windows-server-deployment/page/symantec-configuration)
  * [Move computer to correct folder in Symantec online portal](http://192.168.10.42:9015/books/windows-server-deployment/page/symantec-configuration#bkmrk-host-configuration)
  * [Change Intrusion Polices in Symantec online portal](http://192.168.10.42:9015/books/windows-server-deployment/page/symantec-configuration#bkmrk-update-intrusion-pol)
* [Install and Configure EEPOS](http://192.168.10.42:9015/books/windows-server-deployment/page/install-and-configure-eepos)
  * Check POS is communicating with HO
  
* [Configure DHCP](http://192.168.10.42:9015/books/windows-server-deployment/page/configure-dhcp)

* Install Nagios Agent
  
  
  
##### Non-Server  
* Start POS services on all store Machines
* Configure Nagios at HO