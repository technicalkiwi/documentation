
## Azure Monitor
Key Capabilities
![[monitor1.png]]

Azure Monitor Components
![[monitor2.png]]


Metrics and Logs
![[monitor3.png]]


Activity Log Events
![[monitor4.png]]



### Improve Incident response

Alert Flow
![[monitor5.png]]


## Log Analytics

Uses
![[logs1.png]]


Log Analytics work space
![[log2.png]]

### Query Log Analytics Data
Uses a custom Query language called KQL ( Kusto Query Language )
![[logs3.png]]

Log Analytics Query Structure
![[log4.png]]

``` kql
AzureActivity | where TimeGenerated > ago(1d) | summarize count() by ActivityStatusValue | order by count_ desc
```

```kql
Heartbeat
| summarize count() by Computer,bin(TimeGenerated,5min)
```

``` KQL
Alert
| join MyLog_CL on Computer
```

``` kql
InsightsMetrics 
| where TimeGenerated > ago(1h) 
| where Name == "UtilizationPercentage" 
| summarize avg(Val) by bin(TimeGenerated, 5m), Computer //split up by computer 
| render timechart
```


