Set Hostname of host
sudo hostnamectl set-hostname MCL-PRD-VLT01


Set Ip of Host
``` yaml
network:
  ethernets:
    ens160:
      addresses:
      - 10.3.30.93/24
#      gateway4: 10.3.30.254
      routes:
        - to: default
          via: 10.3.30.254
      nameservers:
        addresses:
        - 10.3.30.1
        - 10.3.31.1
        search:
        - cernonz.net.nz

```

## Install Vault
Add PGP package to allow installation of signing key
`sudo apt update && sudo apt install gpg`

Download and add PGP key to new keyring
``` bash
cd /tmp
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg 
```

Verify Key
``` bash
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint 
```

Add Hashicorp repo
``` bash
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
```

Create Vault User
``` bash
sudo groupadd -r vault
sudo useradd -m -r -g vault vault
```

Set Vault user as owner of vault directory
```bash
sudo chown -R vault:vault /opt/vault
```

Run apt update to check new packages
Then install vault

``` bash
sudo apt update
sudo apt install vault
```

Verify Installation of vault
`vault`

> [!NOTE] Vault Error
> If you get an error that the binary could not be found, then your `PATH` environment variable was not setup properly. Please go back and ensure that your `PATH` variable contains the directory where Vault was installed.

## Configure Vault

Create initial Configuration File
`nano /etc/vault.d/vault-server.hcl`

``` yml
disable_mlock = true
ui            = true
api_addr      = "https://vault.connectme365.com:8200"
  

listener "tcp" {
  address       = "vault.connectme365.com:8200"
  tls_cert_file = "/opt/vault/tls/public.pem"
  tls_key_file  = "/opt/vault/tls/private.key"
}
  

storage "file" {
  path = "/opt/vault/data"
}
```

Export Vault Address and token
``` bash
export HTTP_PROXY="http://vault.connectme365.com"
export HTTPS_PROXY="https://vault.connectme365.com"
export VAULT_ADDRESS="https://vault.connectme365.com"
```


Start Vault
``` bash
vault server -config /etc/vault.d/vault-server.hcl
```

Initialize Vault
``` bash
vault operator init 
```

Take note of the unseal keys and Initial Root token
> [!NOTE] Tip
> If you use the web UI to Initialize the vault you can download the keys and token as a JSON file

Unseal the Vault and Login
```bash
vault operator unseal -address="https://vault.connectme365.com:8200"
vault login -address="https://vault.connectme365.com:8200"
```

Create new Key store
``` bash
vault secrets enable -address="https://vault.connectme365.com:8200" -path=kv kv
```

Create new Approle
``` bash
vault auth enable -address="https://vault.connectme365.com:8200" approle
```

Create a new kes-policy.hcl in /opt/vault/config/kes-policy.hcl
``` hcl
## /opt/vault/config/kes-policy.hcl
path "kv/data/*" {
   capabilities = [ "create", "read" ]
}
path "kv/metadata/*" {
   capabilities = [ "list", "delete" ]       
}
```


Write kes-policy to vault
```bash
vault policy write -address="https://vault.connectme365.com:8200" kes-policy /opt/vault/config/kes-policy.hcl
```

Write kes-role config
```bash
vault write -address="https://vault.connectme365.com:8200"    auth/approle/role/kes-role token_num_uses=0 secret_id_num_uses=0 period=5m

vault write -address="https://vault.connectme365.com:8200"    auth/approle/role/kes-role policies=kes-policy
```


Export and take note of Kes Role ID
``` bash
vault read -address="https://vault.connectme365.com:8200"       auth/approle/role/kes-role/role-id
```
<pre>
Key        Value
---        -----
role_id    $ROLEID$
</pre>


Export and take note of Kes Secret ID
``` bash
vault write -address="https://vault.connectme365.com:8200"   -f auth/approle/role/kes-role/secret-id
```
<pre>
Key                   Value
---                   -----
secret_id             $Secret$
secret_id_accessor    $ACCESSOR$
secret_id_num_uses    0
secret_id_ttl         0s
</pre>

