# Using Config
## Use
`docker-compose config`

## Result
Prints your resolved application config to the terminal:
This will display the out put after pulling from the .env file

![Compose Config](../../resouces/compose-config.png)