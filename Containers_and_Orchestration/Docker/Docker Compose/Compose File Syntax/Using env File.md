# Using a .env File 
You can set default values for any environment variables referenced in the Compose file, or used to configure Compose, in an environment file named .env:

 

Declare all the Environment Variables in the .env file
Example .env file

<pre>
DB_HOST=192.168.10.42:3306
DB_USER=Vikunja
DB_PASS=Kanban20
DB_NAME=vikunja
</pre>

Call the Environment Variables in the docker-compose.yml file by using ${Variable_Name}
Example of the Docker compose file

<pre>
version: '3'

services:
  api:
    image: vikunja/api
    environment:
      VIKUNJA_DATABASE_HOST: ${DB_HOST}
      VIKUNJA_DATABASE_PASSWORD: ${DB_PASS}
      VIKUNJA_DATABASE_TYPE: mysql
      VIKUNJA_DATABASE_USER: ${DB_USER}
      VIKUNJA_DATABASE_DATABASE: ${DB_NAME}
    volumes: 
      - ./files:/app/vikunja/files
    restart: unless-stopped
    
  frontend:
    image: vikunja/frontend
    restart: unless-stopped
    ports: 
      - 8071:80
</pre>

You can  verify this with the config command (docker-compose config), which prints your resolved application config to the terminal

![Docker-compose with .env](../../resouces/env_file_example.png)
 

## Priority List
Values in the shell take precedence over those specified in the .env file. If you set TAG to a different value in your shell, the substitution in image uses that instead:

When you set the same environment variable in multiple files, here’s the priority used by Compose to choose which value to use:

- Compose file
- Shell environment variables
- Environment file
- Dockerfile
- Variable is not defined